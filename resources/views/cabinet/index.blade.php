@extends('layouts.main')
@section('content')
    <style>
        .btn-notifications .btn-primary, .btn-danger {
            float: right;
        }

        .left-sidebar {
            margin: 50px 0 0 50px;
            display: flex;
            justify-content: center;
            justify-items: center;
            flex-direction: column;
            max-width: 120px;
            position: fixed;

        }

        nav {
            position: relative;
            width: 100%;
        }

        .box-sub {
            margin-top: 5px;
            border-radius: 50%;
            margin-left: 15px;
            width: 73px;
            height: 73px;
            display: flex;
            align-items: center;
        }

        .image img {
            border-radius: 50%;
        }

        @media only screen and (max-width: 1435px) {
            .left-sidebar {
                margin: 60px 0 0 140px;
                width: 100%;
                position: unset;
                max-height: 200px;
                overflow: hidden;
            }

            .image {
                display: flex;
                flex-direction: row;
                position: absolute;
                /*top: 0;*/
                height: 100px;
            }

            .left-sidebar label {
                display: none;
            }

            img {
                cursor: pointer;
            }
        }

        #result-sub {
            display: flex;
            flex-wrap: wrap;
        }

        .col-md-3 {
            margin-left: 5%;
        }
        .col-md-3 .card {
            height: 400px;
            overflow: hidden;
        }
        .pagination {
            margin-top: 20px;
        }
    </style>
    <style>
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-content {
            max-height: 600px;
            background: whitesmoke;
            overflow: scroll;
            width: 90%;
            max-width: 100%;
            margin: 0 auto;
            padding: 3em;
            font: 100%/1.4 serif;
            border: 1px solid rgba(0,0,0,0.25)
        }

        .modal-content::-webkit-scrollbar {
            width: 1em;
        }

        .modal-content::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        }

        .modal-content::-webkit-scrollbar-thumb {
            background-color: darkgrey;
            outline: 1px solid slategrey;
        }


    </style>
    <div class="bg-primary py-4 py-lg-6">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <div>
                        <h1 class="mb-0 text-white display-4">Личный кабинет</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="left-sidebar">
        <label for="" style="text-align: center"><strong>Подписки</strong></label>
        <div class="image">
            @foreach($subscriptions as $subscription)

                @if($subscription->user->avatar)
                    <img data-id="{{$subscription->id}}" class="image-sub"
                         src="{{ asset($subscription->user->avatar) }}" alt="" width="70px"
                         height="70px">
                @else
                    <img data-id="{{$subscription->id}}" class="image-sub"
                         src="{{ asset('images/logo/user.jpg') }}"
                         alt="" width="70px" height="70px">
                @endif
                <div style="display: none;text-align: center" class="option-{{$subscription->id}}">
                    <h6>{{$subscription->user->name}}</h6>
                    <a data-id="{{ $subscription->user_id }}" style="font-size: 9px;width: 100%"
                       class="btn btn-primary sub-all-curses">Просмотреть курсы</a>
                    <a data-id="{{ $subscription->user_id }}" style="font-size: 9px;width: 100%"
                       class="btn btn-danger curse-delete">Отписатся</a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="pagination" style="max-width: 160px">
        @if(isset($subscriptions))    {{ $subscriptions->links() }} @endif
    </div>



    <div class="container mt-5">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button"
                        role="tab" aria-controls="home" aria-selected="true">Закладки
                </button>
            </li>

            <li class="nav-item" role="presentation">
                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button"
                        role="tab" aria-controls="profile" aria-selected="false">Уведомления
                </button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button"
                        role="tab" aria-controls="contact" aria-selected="false">О себе
                </button>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="row mb-2">
                    @foreach($bookmarks as $product)
                        <div class="col-md-4">
                            <div class="card mt-1" id="courseAccordion">
                                {{--                    <div class="card" style="width: 18rem;">--}}
                                <img src="{{ asset($product->product->image) }}" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $product->product->title }}</h5>
                                    <p class="card-text">{{ mb_strimwidth($product->product->content,0,80,'...') }}</p>
                                    <a href="/product/show/{{ $product->product->id }}"
                                       class="btn btn-primary">Подробнее</a>
                                    <button type="button" data-id="{{ $product->product->id }}"
                                            class="btn btn-danger">Убрать из закладок
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @if($bookmarks->isEmpty())
                        <div class=" alert-danger">
                            <h1>У вас нету добавленных закладок.</h1>
                        </div>
                    @endif
                </div>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Текст</th>
                        <th scope="col">Дата</th>
                        <th scope="col">Статус</th>
                        <th scope="col">Ометить</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php $i = 1; ?>
                    @if(isset($notification))
                        <form id="form" action="">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            @foreach($notification as $value)
                                <tr>
                                    <th scope="row"> <?= $i++ ?> </th>
                                    <td>{{ $value->text }}</td>
                                    <td>{{ $value->created_at }}</td>
                                    @if($value->read == 0)
                                        <td>Не просмотрено</td>
                                    @else
                                        <td>Просмотрено</td>
                                    @endif
                                    <td><input type="checkbox" class="checkbox" value="{{ $value->id }}"></td>
                                </tr>
                            @endforeach
                        </form>
                    @endif

                    </tbody>
                    <div class="btn-notifications">
                        <bottun id="btn-delete" class="btn btn-danger">Удалить</bottun>
                        <bottun id="btn-read" class="btn btn-primary">Прочитать</bottun>

                    </div>
                </table>

            </div>

            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <form action="{{ route('about.me.store') }}" id="form-about">
                    <!-- 2 column grid layout with text inputs for the first and last names -->
                    <div class="row mb-4">
                        <div class="col">
                            <div class="form-outline">
                                <input value="@if(isset($about_me->first_name)){{ $about_me->first_name }} @endif"
                                       name="first_name" type="text" id="form6Example1"
                                       class="form-control"/>
                                <label class="form-label" for="form6Example1">Ваша имя</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input value="@if(isset($about_me->last_name)) {{ $about_me->last_name }} @endif"
                                       type="text" name="last_name" id="form6Example2"
                                       class="form-control"/>
                                <label class="form-label" for="form6Example2">Ваша фамилия</label>
                            </div>
                        </div>
                    </div>



                    <!-- Number input -->
                    <div class="form-outline mb-4">
                        <input value="@if(isset($about_me->phone)){{ $about_me->phone }}@endif" name="phone"
                               type="number" id="form6Example6"
                               class="form-control"/>
                        <label class="form-label" for="form6Example6">Номер телефона</label>
                    </div>

                    <div class="row mb-4">
                        <div class="col">
                            <div class="form-outline">
                                <select class="form-control" name="country" id="form6Example1">
                                    <option value="Азербайджан">Азербайджан</option>
                                    <option value="Украина">Украина</option>
                                    <option value="Россия">Россия</option>
                                </select>
                                <label class="form-label" for="form6Example1">Страна</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-outline">
                                <input value="@if(isset($about_me->city)){{ $about_me->city }}@endif" type="text"
                                       name="city" id="form6Example2"
                                       class="form-control"/>
                                <label class="form-label" for="form6Example2">Город</label>
                            </div>
                        </div>
                    </div>

                    <!-- Message input -->
                    <div class="form-outline mb-4">
                    <textarea name="about_me" class="form-control" id="form6Example7"
                              rows="4">@if(isset($about_me->about_me)){{ $about_me->about_me }}@endif</textarea>
                        <label class="form-label" for="form6Example7">О себе</label>
                    </div>
                    <button id="submit" type="button" class="btn btn-primary btn-block mb-4">Сохранить</button>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <div id="result-sub">

        </div>
{{--        <div id="myModal" class="modal">--}}
{{--            <!-- Modal content -->--}}
{{--            <div class="modal-content">--}}
{{--                <span class="close">&times;</span>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script>
        var modal = document.getElementById("myModal");

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        var sub_remove = document.querySelectorAll('.curse-delete');
        sub_remove.forEach(function (value) {
            value.addEventListener("click", function () {

                var id = {
                    id: this.getAttribute('data-id')
                }
                fetch('/ajax/subscription/store', {
                    method: 'post',
                    body: JSON.stringify(id),
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRF-TOKEN": token
                    }
                }).then(function (response) {
                    response.text().then(value =>
                        Swal.fire(
                            'Good job!',
                            value,
                            'success'
                        ))
                    location.reload()
                })
            })
        })
    </script>
    <script>
        $(document).ready(function () {
            $('.image-sub').on('click', function () {
                $(".option-" + this.getAttribute('data-id')).css("display", "flax").slideToggle()
            })

            $('.sub-all-curses').on('click', function () {


                var id = this.getAttribute('data-id');
                $.ajax({
                    method: "post",
                    url: "{{ '/get/user/curses' }}",
                    data: {
                        id: id,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (response) {
                        var result = '';
                        $.each(response, function (index, value) {
                            if (value['content'].length > 70){
                                var content = value['content'].slice(0,70) + '...';
                            }else {
                                var content = value['content'];
                            }

                            result = result + '<div class="col-md-3">' +
                                '<div class="card mt-1" id="courseAccordion">' +
                                '<div class="card" style="width: 18rem;">' +
                                '<img src="/' + value['image'] + '" class="card-img-top" alt="...">' +
                                '<div class="card-body">' +
                                '<h5 class="card-title">' + value['title'] + '</h5>' +
                                '<p class="card-text">' + content + '</p>' +
                                '<a href="/product/show/' + value['id'] + '" class="btn btn-primary">Подробнее</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>'+
                            '</div>'

                            $('#result-sub').empty().css("display", "flax").append(result)
                            // $('#myModal').modal('show')
                        })
                    }
                })

            });
        })
    </script>
    <script>
        var form = document.getElementById('form')
        var formData = new FormData(form);
        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        document.getElementById('submit').addEventListener("click", function () {
            var form = document.getElementById('form-about');
            var formData = new FormData(form);
            fetch('/about/me/store', {
                method: 'post',
                body: formData,
                headers: {
                    "X-CSRF-TOKEN": token
                }
            }).then(function (response) {
                response.text().then(value =>
                    Swal.fire(
                        'Good job!',
                        value,
                        'success'
                    ))
            })
        })

        document.querySelectorAll('.btn-danger').forEach(function (value) {
            var id = {
                id: value.getAttribute('data-id')
            }
            value.addEventListener("click", function () {
                fetch('/bookmark/store', {
                    method: 'post',
                    body: JSON.stringify(id),
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRF-TOKEN": token
                    }
                }).then(async function (response) {
                    location.reload()
                })
            })
        })

        buttonDelete = document.getElementById('btn-delete')
        buttonRead = document.getElementById('btn-read')

        buttonDelete.addEventListener("click", function () {
            var checkbox = document.querySelectorAll(".checkbox")
            var checked = [];
            checkbox.forEach(function (value, index) {
                if (value.checked === true) {
                    checked.push(value.getAttribute('value'))

                }
            })
            if (checked.length > 0) {
                fetch('/delete/notifications', {
                    method: "post",
                    body: JSON.stringify(checked),
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRF-TOKEN": token
                    }
                }).then(async function (response) {
                    location.reload()
                })
            }
        })


        buttonRead.addEventListener("click", function () {
            var checkbox = document.querySelectorAll(".checkbox")
            var checked = [];
            checkbox.forEach(function (value, index) {
                if (value.checked === true) {
                    checked.push(value.getAttribute('value'))

                }
            })

            if (checked.length > 0) {
                fetch('/read/notifications', {
                    method: "post",
                    body: JSON.stringify(checked),
                    headers: {
                        "Content-Type": "application/json",
                        "X-CSRF-TOKEN": token
                    }
                }).then(async function (response) {
                    location.reload()
                })
            }
        })
    </script>
@endsection
