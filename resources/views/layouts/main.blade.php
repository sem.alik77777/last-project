<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Geeks is a fully responsive and yet modern premium bootstrap template. Geek is feature-rich components and beautifully designed pages that help you create the best possible website and web application projects."/>
    <meta name="keywords"
          content="Geeks UI, bootstrap, bootstrap 5, Course, Sass, landing, Marketing, admin themes, bootstrap admin, bootstrap dashboard, ui kit, web app, multipurpose"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!-- Favicon icon-->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon/favicon.ico">


    <link href="{{ asset('js/assets/fonts/feather/feather.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/bootstrap-icons/font/bootstrap-icons.css') }}" rel="stylesheet"/>
    <link href="{{ asset('js/assets/libs/dragula/dist/dragula.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('js/assets/libs/@mdi/font/css/materialdesignicons.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('js/assets/libs/prismjs/themes/prism.css') }}" rel="stylesheet"/>
    <link href="{{ asset('js/assets/libs/dropzone/dist/dropzone.css') }}" rel="stylesheet"/>
    <link href="{{ asset('js/assets/libs/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet"/>
    <link href="{{ asset('js/assets/libs/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/@yaireo/tagify/dist/tagify.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/tiny-slider/dist/tiny-slider.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/tippy.js/dist/tippy.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/theme.min.css') }}" rel="stylesheet">
    <!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">


    <title>Add Course | Geeks - Bootstrap 5 Template</title>
</head>
<style>
    #exit-popap {
        float: right;
        cursor: pointer;
    }

    .b-popup {
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        overflow: hidden;
        position: fixed;
        top: 0px;
        z-index: 1000;
        display: none;
    }

    .b-popup .b-popup-content {
        margin: 40px auto 0px auto;
        max-width: 500px;
        max-height: 300px;
        text-align: center;
        font-size: 20px;
        display: none;
        padding: 10px;
        background-color: #c5c5c5;
        border-radius: 5px;
        box-shadow: 0px 0px 10px #000;
    }
</style>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-default">
    <div class="container-fluid px-0">
        <a class="navbar-brand" href="../index.html"
        ><img src="../assets/images/brand/logo/logo.svg" alt=""
            /></a>
        <!-- Mobile view nav wrap -->
        <ul
            class="navbar-nav navbar-right-wrap ms-auto d-lg-none d-flex nav-top-wrap"
        >
            <li>
                <a href="{{ asset('product.create') }}" class="btn btn-primary">Добавить курс</a>
            </li>
            @if(auth()->check())
                <li class="dropdown stopevent">
                    <a
                        class="btn btn-light btn-icon rounded-circle text-muted indicator indicator-primary"
                        href="#"
                        role="button"
                        data-bs-toggle="dropdown"
                    >
                        <i class="bi-bell"> </i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end shadow">
                        <div>
                            <div
                                class="border-bottom px-3 pb-3 d-flex justify-content-between align-items-center"
                            >
                                <span class="h5 mb-0">Notifications</span>
                                <a href="# " class="text-muted"
                                ><span class="align-middle"
                                    ><i class="fe fe-settings me-1"></i></span
                                    ></a>
                            </div>
                            <ul class="list-group list-group-flush notification-list-scroll">
                                <li class="list-group-item bg-light">
                                    <div class="row">
                                        <div class="col">
                                            <a href="#" class="text-body">
                                                <div class="d-flex">
                                                    <img
                                                        src="../assets/images/avatar/avatar-1.jpg"
                                                        alt=""
                                                        class="avatar-md rounded-circle"
                                                    />
                                                    <div class="ms-3">
                                                        <h5 class="fw-bold mb-1">Kristin Watson:</h5>
                                                        <p class="mb-3">
                                                            Krisitn Watsan like your comment on course Javascript
                                                            Introduction!
                                                        </p>
                                                        <span class="fs-6 text-muted">
													<span
                                                    ><span
                                                            class="fe fe-thumbs-up text-success me-1"
                                                        ></span
                                                        >2 hours ago,</span
                                                    >
													<span class="ms-1">2:19 PM</span>
												</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-auto text-center me-2">
                                            <a
                                                href="#"
                                                class="badge-dot bg-info"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"
                                                title="Mark as read"
                                            >
                                            </a>
                                            <div>
                                                <a
                                                    href="#"
                                                    data-bs-toggle="tooltip"
                                                    data-bs-placement="top"

                                                    title="Remove"
                                                >
                                                    <i class="fe fe-x text-muted"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col">
                                            <a href="#" class="text-body">
                                                <div class="d-flex">
                                                    <img
                                                        src="../assets/images/avatar/avatar-2.jpg"
                                                        alt=""
                                                        class="avatar-md rounded-circle"
                                                    />
                                                    <div class="ms-3">
                                                        <h5 class="fw-bold mb-1">Brooklyn Simmons</h5>
                                                        <p class="mb-3">
                                                            Just launched a new Courses React for Beginner.
                                                        </p>
                                                        <span class="fs-6 text-muted">
													<span
                                                    ><span
                                                            class="fe fe-thumbs-up text-success me-1"
                                                        ></span
                                                        >Oct 9,</span
                                                    >
													<span class="ms-1">1:20 PM</span>
												</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-auto text-center me-2">
                                            <a
                                                href="#"
                                                class="badge-dot bg-secondary"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"

                                                title="Mark as unread"
                                            >
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col">
                                            <a href="#" class="text-body">
                                                <div class="d-flex">
                                                    <img
                                                        src="../assets/images/avatar/avatar-3.jpg"
                                                        alt=""
                                                        class="avatar-md rounded-circle"
                                                    />
                                                    <div class="ms-3">
                                                        <h5 class="fw-bold mb-1">Jenny Wilson</h5>
                                                        <p class="mb-3">
                                                            Krisitn Watsan like your comment on course Javascript
                                                            Introduction!
                                                        </p>
                                                        <span class="fs-6 text-muted">
													<span
                                                    ><span class="fe fe-thumbs-up text-info me-1"></span
                                                        >Oct 9,</span
                                                    >
													<span class="ms-1">1:56 PM</span>
												</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-auto text-center me-2">
                                            <a
                                                href="#"
                                                class="badge-dot bg-secondary"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"

                                                title="Mark as unread"
                                            >
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col">
                                            <a href="#" class="text-body">
                                                <div class="d-flex">
                                                    <img
                                                        src="../assets/images/avatar/avatar-4.jpg"
                                                        alt=""
                                                        class="avatar-md rounded-circle"
                                                    />
                                                    <div class="ms-3">
                                                        <h5 class="fw-bold mb-1">Sina Ray</h5>
                                                        <p class="mb-3">
                                                            You earn new certificate for complete the Javascript
                                                            Beginner course.
                                                        </p>
                                                        <span class="fs-6 text-muted">
													<span
                                                    ><span class="fe fe-award text-warning me-1"></span
                                                        >Oct 9,</span
                                                    >
													<span class="ms-1">1:56 PM</span>
												</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-auto text-center me-2">
                                            <a
                                                href="#"
                                                class="badge-dot bg-secondary"
                                                data-bs-toggle="tooltip"
                                                data-bs-placement="top"

                                                title="Mark as unread"
                                            >
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="border-top px-3 pt-3 pb-0">
                                <a
                                    href="../pages/notification-history.html"
                                    class="text-link fw-semi-bold"
                                >See all Notifications</a
                                >
                            </div>
                        </div>
                    </div>
                </li>
                <li class="dropdown ms-2">
                    <a
                        class="rounded-circle"
                        href="#"
                        role="button"
                        data-bs-toggle="dropdown"
                    >
                        <div class="avatar avatar-md avatar-indicators avatar-online">
                            <img
                                alt="avatar"
                                src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                class="rounded-circle"
                            />
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end shadow">
                        <div class="dropdown-item">
                            <div class="d-flex">
                                <div class="avatar avatar-md avatar-indicators avatar-online">
                                    <img
                                        alt="avatar"
                                        src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                        class="rounded-circle"
                                    />
                                </div>
                                <div class="ms-3 lh-1">
                                    <h5 class="mb-1">{{ \Illuminate\Support\Facades\Auth::user()->name }}</h5>
                                    <p class="mb-0 text-muted">{{ \Illuminate\Support\Facades\Auth::user()->email }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <ul class="list-unstyled">
                            {{--                            <li class="dropdown-submenu">--}}
                            {{--                                <a--}}
                            {{--                                    class="dropdown-item dropdown-list-group-item dropdown-toggle"--}}
                            {{--                                    href="#"--}}
                            {{--                                >--}}
                            {{--                                    <i class="fe fe-circle me-2"></i>Status--}}
                            {{--                                </a>--}}
                            {{--                                <ul class="dropdown-menu">--}}
                            {{--                                    <li>--}}
                            {{--                                        <a class="dropdown-item" href="#">--}}
                            {{--                                            <span class="badge-dot bg-success me-2"></span>Online--}}
                            {{--                                        </a>--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <a class="dropdown-item" href="#">--}}
                            {{--                                            <span class="badge-dot bg-secondary me-2"></span>Offline--}}
                            {{--                                        </a>--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <a class="dropdown-item" href="#">--}}
                            {{--                                            <span class="badge-dot bg-warning me-2"></span>Away--}}
                            {{--                                        </a>--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <a class="dropdown-item" href="#">--}}
                            {{--                                            <span class="badge-dot bg-danger me-2"></span>Busy--}}
                            {{--                                        </a>--}}
                            {{--                                    </li>--}}
                            {{--                                </ul>--}}
                            {{--                            </li>--}}

                            <li>
                                <a class="dropdown-item" href="{{ route('personal.area') }}">
                                    <i class="fe fe-user me-2"></i>Профиль
                                </a>
                            </li>
                            {{--                            <li>--}}
                            {{--                                <a--}}
                            {{--                                    class="dropdown-item"--}}
                            {{--                                    href="../pages/student-subscriptions.html"--}}
                            {{--                                >--}}
                            {{--                                    <i class="fe fe-star me-2"></i>Subscription--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            {{--                            <li>--}}
                            {{--                                <a class="dropdown-item" href="#">--}}
                            {{--                                    <i class="fe fe-settings me-2"></i>Settings--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            {{--                        </ul>--}}
                            <div class="dropdown-divider"></div>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="dropdown-item" href="../index.html">
                                        <i class="fe fe-power me-2"></i>Выйти
                                    </a>
                                </li>
                            </ul>
                    </div>
                </li>
            @endif
        </ul>
        <!-- Button -->
        <button
            class="navbar-toggler collapsed"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbar-default"
            aria-controls="navbar-default"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <span class="icon-bar top-bar mt-0"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>
        <!-- Collapse -->
        <?php $categories = \App\Models\Category::all() ?>
        <div class="collapse navbar-collapse" id="navbar-default">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"
                       href="{{ route('product.index') }}"

                       data-bs-display="static">Главная</a>
                </li>
                @foreach($categories as $category)
                    <li class="nav-item dropdown">

                        <a
                            class="nav-link dropdown-toggle"
                            href="#"
                            id="navbarBrowse"
                            data-bs-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            data-bs-display="static"
                        >
                            {{ $category->title }}
                        </a>

                        <ul
                            class="dropdown-menu dropdown-menu-arrow"
                            aria-labelledby="navbarBrowse"
                        >
                            @foreach($category->subCategories as $sub_category)
                                <li class="dropdown-submenu dropend">
                                    <a
                                      data-value="{{ $category->title }}"  class="dropdown-item dropdown-list-group-item "
{{--                                        href="{{ route('product.index', $sub_category->id) }}"--}}
                                        href="#"
                                    >
                                        {{ $sub_category->title }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
                <li class="nav-item dropdown">
                    <a
                        class="nav-link"
                        href="#"
                        id="navbarDropdown"
                        role="button"
                        data-bs-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        <i class="fe fe-more-horizontal fs-3"></i>
                    </a>
                    <div
                        class="dropdown-menu dropdown-menu-md"
                        aria-labelledby="navbarDropdown"
                    >
                        <div class="list-group">
                            <a
                                class="list-group-item list-group-item-action border-0"
                                href="../docs/index.html"
                            >
                                <div class="d-flex align-items-center">
                                    <i class="fe fe-file-text fs-3 text-primary"></i>
                                    <div class="ms-3">
                                        <h5 class="mb-0">Documentations</h5>
                                        <p class="mb-0 fs-6">
                                            Browse the all documentation
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <a
                                class="list-group-item list-group-item-action border-0"
                                href="../docs/changelog.html"
                            >
                                <div class="d-flex align-items-center">
                                    <i class="fe fe-layers fs-3 text-primary"></i>
                                    <div class="ms-3">
                                        <h5 class="mb-0">
                                            Changelog <span class="text-primary ms-1">v2.2.2</span>
                                        </h5>
                                        <p class="mb-0 fs-6">See what's new</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
            <form class="mt-3 mt-lg-0 ms-lg-3 d-flex align-items-center">
				<span class="position-absolute ps-3 search-icon">
					<i class="fe fe-search"></i>
				</span>
                <input
                    id="search"
                    class="form-control ps-6"
                    placeholder="Search Courses"
                />
            </form>
            <a href="{{ route('product.create') }}" class="btn btn-primary ml-2">Добавить курс</a>
            <ul class="navbar-nav navbar-right-wrap ms-auto d-none d-lg-block">
                @if(auth()->check())
                    <li class="dropdown d-inline-block stopevent">
                        <a
                            class="btn btn-light btn-icon rounded-circle text-muted indicator indicator-primary"
                            href="#"
                            role="button"
                            id="dropdownNotificationSecond"
                            data-bs-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                        >
                            <i class="bi-bell"> </i>
                        </a>
                        <div
                            class="dropdown-menu dropdown-menu-end dropdown-menu-lg"
                            aria-labelledby="dropdownNotificationSecond"
                        >
                            <div>
                                <div
                                    class="border-bottom px-3 pb-3 d-flex justify-content-between align-items-center"
                                >
                                    <span class="h5 mb-0">Notifications</span>
                                    <a href="# " class="text-muted"
                                    ><span class="align-middle"
                                        ><i class="fe fe-settings me-1"></i></span
                                        ></a>
                                </div>
                                <?php $notification = \App\Models\Notification::query()->where('user_id', \Illuminate\Support\Facades\Auth::id())->where('read', 0)->get() ?>
                                <ul class="list-group list-group-flush notification-list-scroll ">
                                    @foreach($notification as $value)
                                        <li class="list-group-item bg-light">
                                            <div class="row">
                                                <div class="col">
                                                    <a class="text-body" href="#">
                                                        <div class="d-flex">
                                                            <img
                                                                src="@if(isset($value->product->image)) {{ asset($value->product->image) }} @else {{ asset($value->user->avatar) }} @endif"
                                                                alt=""
                                                                class="avatar-md rounded-circle"
                                                            />
                                                            <div class="ms-3">
                                                                <h5 class="fw-bold mb-1">{{ $value->user->name }}</h5>
                                                                <p class="mb-3">
                                                                    {{ $value->text }}
                                                                </p>
                                                                <span class="fs-6 text-muted">
														<span
                                                        ><span
                                                                class="fe fe-thumbs-up text-success me-1"
                                                            ></span
                                                            >{{ $value->created_at }}</span
                                                        >
{{--														<span class="ms-1">2:19 PM</span>--}}
													</span>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-auto text-center me-2">

                                                    <a
                                                        href="#"
                                                        class="badge-dot bg-info"
                                                        data-bs-toggle="tooltip"
                                                        data-bs-placement="top"

                                                        title="Mark as read"
                                                    >
                                                    </a>
                                                    <div>
                                                        <a
                                                            href="#"
                                                            class="bg-transparent"
                                                            data-bs-toggle="tooltip"
                                                            data-bs-placement="top"

                                                            title="Remove"
                                                        >
                                                            <i class="fe fe-x text-muted"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="border-top px-3 pt-3 pb-0">
                                    <span onclick="return confirm('Вы уверены что хотите удалить все уведомления?')"
                                          id="delete-notifications" style="cursor: pointer;color: #0a53be">Удалить все уведомления?</span>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="dropdown ms-2 d-inline-block">
                        <a
                            class="rounded-circle"
                            href="#"
                            data-bs-toggle="dropdown"
                            data-bs-display="static"
                            aria-expanded="false"
                        >
                            <div class="avatar avatar-md avatar-indicators avatar-online">
                                <img
                                    alt="avatar"
                                    src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                    class="rounded-circle"
                                />
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <div class="dropdown-item">
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-online">
                                        <img
                                            alt="avatar"
                                            src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                            class="rounded-circle"
                                        />
                                    </div>
                                    <div class="ms-3 lh-1">
                                        <h5 class="mb-1">{{ \Illuminate\Support\Facades\Auth::user()->name }}</h5>
                                        <p class="mb-0 text-muted">{{ \Illuminate\Support\Facades\Auth::user()->email }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <ul class="list-unstyled">
                                {{--                                <li class="dropdown-submenu dropstart-lg">--}}
                                {{--                                    <a--}}
                                {{--                                        class="dropdown-item dropdown-list-group-item dropdown-toggle"--}}
                                {{--                                        href="#"--}}
                                {{--                                    >--}}
                                {{--                                        <i class="fe fe-circle me-2"></i>Status--}}
                                {{--                                    </a>--}}
                                {{--                                    <ul class="dropdown-menu">--}}
                                {{--                                        <li>--}}
                                {{--                                            <a class="dropdown-item" href="#">--}}
                                {{--                                                <span class="badge-dot bg-success me-2"></span>Online--}}
                                {{--                                            </a>--}}
                                {{--                                        </li>--}}
                                {{--                                        <li>--}}
                                {{--                                            <a class="dropdown-item" href="#">--}}
                                {{--                                                <span class="badge-dot bg-secondary me-2"></span>Offline--}}
                                {{--                                            </a>--}}
                                {{--                                        </li>--}}
                                {{--                                        <li>--}}
                                {{--                                            <a class="dropdown-item" href="#">--}}
                                {{--                                                <span class="badge-dot bg-warning me-2"></span>Away--}}
                                {{--                                            </a>--}}
                                {{--                                        </li>--}}
                                {{--                                        <li>--}}
                                {{--                                            <a class="dropdown-item" href="#">--}}
                                {{--                                                <span class="badge-dot bg-danger me-2"></span>Busy--}}
                                {{--                                            </a>--}}
                                {{--                                        </li>--}}
                                {{--                                    </ul>--}}
                                {{--                                </li>--}}
                                <li>
                                    <a
                                        class="dropdown-item"
                                        href="{{ route('personal.area') }}"
                                    >
                                        <i class="fe fe-user me-2"></i>Профиль
                                    </a>
                                </li>
                                {{--                                <li>--}}
                                {{--                                    <a--}}
                                {{--                                        class="dropdown-item"--}}
                                {{--                                        href="../pages/student-subscriptions.html"--}}
                                {{--                                    >--}}
                                {{--                                        <i class="fe fe-star me-2"></i>Subscription--}}
                                {{--                                    </a>--}}
                                {{--                                </li>--}}
                                {{--                                <li>--}}
                                {{--                                    <a class="dropdown-item" href="#">--}}
                                {{--                                        <i class="fe fe-settings me-2"></i>Settings--}}
                                {{--                                    </a>--}}
                                {{--                                </li>--}}
                            </ul>
                            <div class="dropdown-divider"></div>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="dropdown-item" href="{{ route('auth.logout') }}">
                                        <i class="bi-door-closed"></i>Выйти
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @else
                    <button id="auth" class="btn btn-outline-warning" type="button">
                        Авторизуйтесь
                    </button>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')


<!-- Modal -->
<!-- Payment Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center d-flex">
                <h4 class="modal-title" id="paymentModalLabel">
                    Add New Payment Method
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div>
                    <!-- Form -->
                    <form class="row mb-4">
                        <div class="mb-3 col-12 col-md-12 mb-4">
                            <h5 class="mb-3">Credit / Debit card</h5>
                            <!-- Radio button -->
                            <div class="d-inline-flex">
                                <div class="form-check me-2">
                                    <input type="radio" id="paymentRadioOne" name="paymentRadioOne"
                                           class="form-check-input"/>
                                    <label class="form-check-label" for="paymentRadioOne"><img
                                            src="../assets/images/creditcard/americanexpress.svg" alt=""/></label>
                                </div>
                                <!-- Radio button -->
                                <div class="form-check me-2">
                                    <input type="radio" id="paymentRadioTwo" name="paymentRadioOne"
                                           class="form-check-input"/>
                                    <label class="form-check-label" for="paymentRadioTwo"><img
                                            src="../assets/images/creditcard/mastercard.svg" alt=""/></label>
                                </div>

                                <!-- Radio button -->
                                <div class="form-check">
                                    <input type="radio" id="paymentRadioFour" name="paymentRadioOne"
                                           class="form-check-input"/>
                                    <label class="form-check-label" for="paymentRadioFour"><img
                                            src="../assets/images/creditcard/visa.svg"
                                            alt=""/></label>
                                </div>
                            </div>
                        </div>
                        <!-- Name on card -->
                        <div class="mb-3 col-12 col-md-4">
                            <label for="nameoncard" class="form-label">Name on card</label>
                            <input id="nameoncard" type="text" class="form-control" name="nameoncard" placeholder="Name"
                                   required/>
                        </div>
                        <!-- Month -->
                        <div class="mb-3 col-12 col-md-4">
                            <label class="form-label">Month</label>
                            <select class="selectpicker" data-width="100%">
                                <option value="">Month</option>
                                <option value="Jan">Jan</option>
                                <option value="Feb">Feb</option>
                                <option value="Mar">Mar</option>
                                <option value="Apr">Apr</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="Aug">Aug</option>
                                <option value="Sep">Sep</option>
                                <option value="Oct">Oct</option>
                                <option value="Nov">Nov</option>
                                <option value="Dec">Dec</option>
                            </select>
                        </div>
                        <!-- Year -->
                        <div class="mb-3 col-12 col-md-4">
                            <label class="form-label">Year</label>
                            <select class="selectpicker" data-width="100%">
                                <option value="">Year</option>
                                <option value="June">2018</option>
                                <option value="July">2019</option>
                                <option value="August">2020</option>
                                <option value="Sep">2021</option>
                                <option value="Oct">2022</option>
                            </select>
                        </div>
                        <!-- Card number -->
                        <div class="mb-3 col-md-8 col-12">
                            <label for="cc-mask" class="form-label">Card Number</label>
                            <input type="text" class="form-control" id="cc-mask"
                                   data-inputmask="'mask': '9999 9999 9999 9999'" inputmode="numeric"
                                   placeholder="xxxx-xxxx-xxxx-xxxx" required/>
                        </div>
                        <!-- CVV -->
                        <div class="mb-3 col-md-4 col-12">
                            <div class="mb-3">
                                <label for="cvv" class="form-label">CVV Code
                                    <i class="fas fa-question-circle ms-1" data-bs-toggle="tooltip" data-placement="top"
                                       title=""
                                       data-original-title="A 3 - digit number, typically printed on the back of a card."></i></label>
                                <input type="password" class="form-control" name="cvv" id="cvv" placeholder="xxx"
                                       maxlength="3" inputmode="numeric" required/>
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="col-md-6 col-12">
                            <button class="btn btn-primary" type="submit">
                                Add New Card
                            </button>
                            <button class="btn btn-outline-white" type="button" data-bs-dismiss="modal">
                                Close
                            </button>
                        </div>
                    </form>
                    <span><strong>Note:</strong> that you can later remove your card at
							the account setting page.</span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addSectionModal" tabindex="-1" role="dialog" aria-labelledby="addSectionModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addSectionModalLabel">
                    Add New Section
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <input class="form-control mb-3" type="text" placeholder="Add new section"/>
                <button class="btn btn-primary" type="Button">
                    Add New Section
                </button>
                <button class="btn btn-outline-white" data-bs-dismiss="modal" aria-label="Close">
                    Close
                </button>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addLectureModal" tabindex="-1" role="dialog" aria-labelledby="addLectureModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addLectureModalLabel">
                    Add New Lecture
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <input class="form-control mb-3" type="text" placeholder="Add new lecture"/>
                <button class="btn btn-primary" type="Button">
                    Add New Lecture
                </button>
                <button class="btn btn-outline-white" data-bs-dismiss="modal" aria-label="Close">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="newCatgory" tabindex="-1" role="dialog" aria-labelledby="newCatgoryLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mb-0" id="newCatgoryLabel">
                    Create New Category
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="mb-3 mb-2">
                        <label class="form-label" for="title">Title<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" placeholder="Write a Category" id="title" required>
                        <small>Field must contain a unique value</small>
                    </div>
                    <div class="mb-3 mb-2">
                        <label class="form-label">Slug</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="slug">https://example.com</span>
                            </div>
                            <input type="text" class="form-control" id="basic-url" aria-describedby="slug"
                                   placeholder="designcourses" required>
                        </div>
                        <small>Field must contain a unique value</small>
                    </div>
                    <div class="mb-3 mb-2">
                        <label class="form-label">Parent</label>
                        <select class="selectpicker" data-width="100%">
                            <option value="">Select</option>
                            <option value="Course">Course</option>
                            <option value="Tutorial">Tutorial</option>
                            <option value="Workshop">Workshop</option>
                            <option value="Company">Company</option>
                        </select>
                    </div>
                    <div class="mb-3 mb-3">
                        <label class="form-label">Description</label>
                        <div id="editor">
                            <br>
                            <h4>One Ring to Rule Them All</h4>
                            <br>
                            <p>
                                Three Rings for the
                                <i> Elven-kingsunder</i> the sky,
                                <br> Seven for the
                                <u>Dwarf-lords</u> in halls of stone, Nine for Mortal Men,
                                <br> doomed to die, One for the Dark Lord on his dark throne.
                                <br> In the Land of Mordor where the Shadows lie.
                                <br>
                                <br>
                            </p>
                        </div>
                    </div>
                    <div class="mb-2">
                        <label class="form-label">Enabled</label>
                        <div class="form-check form-switch">
                            <input type="checkbox" class="form-check-input" id="customSwitch1" checked>
                            <label class="form-check-label" for="customSwitch1"></label>
                        </div>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary">Add New Category</button>
                        <button type="button" class="btn btn-outline-white" data-bs-dismiss="modal">
                            Close
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->


<!-- Course Modal -->
<div class="modal fade" id="courseModal" tabindex="-1" aria-labelledby="courseModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header py-4 align-items-lg-center">
                <div class="d-lg-flex">
                    <div class="mb-3 mb-lg-0">
                        <img src="../assets/images/svg/feature-icon-1.svg" alt=""
                             class=" bg-primary icon-shape icon-xxl rounded-circle">
                    </div>
                    <div class="ms-lg-4">
                        <h2 class="fw-bold mb-md-1 mb-3">Introduction to JavaScript <span class="badge bg-warning ms-2">Free</span>
                        </h2>
                        <p class="text-uppercase fs-6 fw-semi-bold mb-0"><span class="text-dark">Courses -
                  1</span> <span class="ms-3">6 Lessons</span> <span class="ms-3">1 Hour 12 Min</span></p>
                    </div>
                </div>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <h3>In this course you’ll learn:</h3>
                <p class="fs-4">Vanilla JS is a fast, lightweight, cross-platform framework for building incredible,
                    powerful JavaScript applications.</p>
                <ul class="list-group list-group-flush">
                    <!-- List group item -->
                    <li class="list-group-item ps-0">
                        <a href="#"
                           class="d-flex justify-content-between align-items-center text-inherit text-decoration-none">
                            <div class="text-truncate">
                                <span class="icon-shape bg-light text-primary icon-sm rounded-circle me-2"><i
                                        class="mdi mdi-play fs-4"></i></span>
                                <span>Introduction</span>
                            </div>
                            <div class="text-truncate">
                                <span>1m 7s</span>
                            </div>
                        </a>
                    </li>
                    <!-- List group item -->
                    <li class="list-group-item ps-0">
                        <a href="#"
                           class="d-flex justify-content-between align-items-center text-inherit text-decoration-none">
                            <div class="text-truncate">
                                <span class="icon-shape bg-light text-primary icon-sm rounded-circle me-2"><i
                                        class="mdi mdi-play fs-4"></i></span>
                                <span>Installing Development Software</span>
                            </div>
                            <div class="text-truncate">
                                <span>3m 11s</span>
                            </div>
                        </a>
                    </li>
                    <!-- List group item -->
                    <li class="list-group-item ps-0">
                        <a href="#"
                           class="d-flex justify-content-between align-items-center text-inherit text-decoration-none">
                            <div class="text-truncate">
                                <span class="icon-shape bg-light text-primary icon-sm rounded-circle me-2"><i
                                        class="mdi mdi-play fs-4"></i></span>
                                <span>Hello World Project from GitHub</span>
                            </div>
                            <div class="text-truncate">
                                <span>2m 33s</span>
                            </div>
                        </a>
                    </li>
                    <!-- List group item -->
                    <li class="list-group-item ps-0">
                        <a href="#"
                           class="d-flex justify-content-between align-items-center text-inherit text-decoration-none">
                            <div class="text-truncate">
                                <span class="icon-shape bg-light text-primary icon-sm rounded-circle me-2"><i
                                        class="mdi mdi-play fs-4"></i></span>
                                <span>Our Sample Javascript Files</span>
                            </div>
                            <div class="text-truncate">
                                <span>22m 30s</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<!-- new chat modal-->


<!-- Modal -->
<div class="modal fade" id="newchatModal" tabindex="-1" role="dialog" aria-labelledby="newchatModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered " role="document">
        <div class="modal-content ">
            <div class="modal-header align-items-center">
                <h4 class="mb-0" id="newchatModalLabel">Create New Chat</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body px-0">
                <!-- contact list -->
                <ul class="list-unstyled contacts-list mb-0">
                    <!-- chat item -->
                    <li class="py-3 px-4 chat-item contacts-item">
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="#" class="text-link contacts-link">
                                <!-- media -->
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-away">
                                        <img src="../assets/images/avatar/avatar-5.jpg" alt="" class="rounded-circle">
                                    </div>
                                    <!-- media body -->
                                    <div class=" ms-2">
                                        <h5 class="mb-0">Pete Martin</h5>
                                        <p class="mb-0 text-muted">On going description of group...
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div>
                                <small class="text-muted">2/10/2021</small>
                            </div>
                        </div>


                    </li>
                    <!-- chat item -->
                    <li class="py-3 px-4 chat-item contacts-item">

                        <div class="d-flex justify-content-between align-items-center">
                            <a href="#" class="text-link contacts-link">
                                <!-- media -->
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-offline">
                                        <img src="../assets/images/avatar/avatar-9.jpg" alt="" class="rounded-circle">
                                    </div>
                                    <!-- media body -->
                                    <div class=" ms-2">
                                        <h5 class="mb-0">Olivia Cooper</h5>
                                        <p class="mb-0 text-muted">On going description of group...
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div>
                                <small class="text-muted">2/3/2021</small>
                            </div>
                        </div>


                    </li>
                    <!-- chat item -->
                    <li class="py-3 px-4 chat-item contacts-item">

                        <div class="d-flex justify-content-between align-items-center">
                            <a href="#" class="text-link contacts-link">
                                <!-- media -->
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-busy">
                                        <img src="../assets/images/avatar/avatar-19.jpg" alt="" class="rounded-circle">
                                    </div>
                                    <!-- media body -->
                                    <div class=" ms-2">
                                        <h5 class="mb-0">Jamarcus Streich</h5>
                                        <p class="mb-0 text-muted">Start design system for UI.
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div>
                                <small class="text-muted">1/24/2021</small>
                            </div>
                        </div>


                    </li>
                    <!-- chat item -->
                    <li class="py-3 px-4 chat-item contacts-item">

                        <div class="d-flex justify-content-between align-items-center">
                            <a href="#" class="text-link contacts-link">
                                <!-- media -->
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-busy">
                                        <img src="../assets/images/avatar/avatar-12.jpg" alt="" class="rounded-circle">
                                    </div>
                                    <!-- media body -->
                                    <div class=" ms-2">
                                        <h5 class="mb-0">Lauren Wilson</h5>
                                        <p class="mb-0 text-muted">Start design system for UI...
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div>
                                <small class="text-muted">3/3/2021</small>
                            </div>
                        </div>


                    </li>
                    <!-- chat item -->
                    <li class="py-3 px-4 chat-item contacts-item">

                        <div class="d-flex justify-content-between align-items-center">
                            <a href="#" class="text-link contacts-link">
                                <!-- media -->
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-online">
                                        <img src="../assets/images/avatar/avatar-14.jpg" alt="" class="rounded-circle">
                                    </div>
                                    <!-- media body -->
                                    <div class=" ms-2">
                                        <h5 class="mb-0">User Name</h5>
                                        <p class="mb-0 text-muted">On going description of group..
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div>
                                <small class="text-muted">1/5/2021</small>
                            </div>
                        </div>


                    </li>
                    <!-- chat item -->
                    <li class="py-3 px-4 chat-item contacts-item">

                        <div class="d-flex justify-content-between align-items-center">
                            <a href="#" class="text-link contacts-link">
                                <!-- media -->
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-online">
                                        <img src="../assets/images/avatar/avatar-15.jpg" alt="" class="rounded-circle">
                                    </div>
                                    <!-- media body -->
                                    <div class=" ms-2">
                                        <h5 class="mb-0">Rosalee Roberts</h5>
                                        <p class="mb-0 text-muted">On going description of group..
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div>
                                <small class="text-muted">1/14/2021</small>
                            </div>
                        </div>


                    </li>


                </ul>
            </div>

        </div>
    </div>
</div>


<!-- add task -->


<!-- Modal -->
<div class="modal fade" id="taskModal" tabindex="-1" role="dialog"
     aria-labelledby="taskModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="taskModalLabel">Create New Task</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <form class="row">
                    <div class="mb-2 col-12">
                        <label for="taskTitle" class="form-label">Title</label>
                        <input type="text" class="form-control" id="taskTitle"
                               placeholder="Title" required>
                    </div>
                    <div class="col-6">
                        <label for="priority" class="form-label">Priority</label>
                        <select class="selectpicker" data-width="100%" id="priority">
                            <option selected>Low</option>
                            <option value="Medium">Medium</option>
                            <option value="High">High</option>

                        </select>
                    </div>
                    <div class="mb-2 col-6">
                        <label for="date" class="form-label">Due Date</label>
                        <input class="form-control flatpickr" type="text"
                               placeholder="Select Date" id="date" required>
                    </div>
                    <div class="mb-2 col-12">
                        <label for="descriptions" class="form-label">Descriptions</label>
                        <textarea class="form-control" id="descriptions"
                                  rows="3" required></textarea>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="assignTo" class="form-label">Assign To</label>
                        <select class="selectpicker" id="assignTo" data-width="100%">
                            <option selected>Codescandy</option>
                            <option value="John Deo">John Deo</option>
                            <option value="Misty">Misty</option>
                            <option value="Simon Ray">Simon Ray</option>

                        </select>
                    </div>


                    <div class="col-12 d-flex justify-content-end">
                        <button type="button" class="btn btn-outline-secondary
                            me-2" data-bs-dismiss="modal">Cancel
                        </button>
                        <button type="submit" class="btn btn-primary">Create
                            Task
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<!-- Footer -->
<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row align-items-center g-0 border-top py-2">
            <!-- Desc -->
            <div class="col-md-6 col-12 text-center text-md-start">
                <span>© 2021 Geeks. All Rights Reserved.</span>
            </div>
            <!-- Links -->
            <div class="col-12 col-md-6">
                <nav class="nav nav-footer justify-content-center justify-content-md-end">
                    <a class="nav-link active ps-0" href="#">Privacy</a>
                    <a class="nav-link" href="#">Terms </a>
                    <a class="nav-link" href="#">Feedback</a>
                    <a class="nav-link" href="#">Support</a>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="b-popup">
    <div class="b-popup-content">
        Авторизуйтесь одним из способов.
        <span id="exit-popap">Выйти</span>
        <div class="logos">
            <a href="/auth/github/redirect"><img src="{{ asset('images/logo/icons8-github-50.png') }}" alt=""></a>
            <a href="/auth/linkedin/redirect"><img src="{{ asset('images/logo/icons8-линкедин-48.png') }}" alt=""></a>
            <img src="{{ asset('images/logo/icons8-facebook-48.png') }}" alt="">
        </div>
    </div>
</div>

<!-- Scripts -->
<!-- Libs JS -->
<script src="{{asset('js/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('js/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/odometer/odometer.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/flatpickr/dist/flatpickr.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/inputmask/dist/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/apexcharts/dist/apexcharts.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/quill/dist/quill.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/file-upload-with-preview/dist/file-upload-with-preview.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/dragula/dist/dragula.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/bs-stepper/dist/js/bs-stepper.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jQuery.print/jQuery.print.js') }}"></script>
<script src="{{ asset('js/assets/libs/prismjs/prism.js') }}"></script>
<script src="{{ asset('js/assets/libs/prismjs/components/prism-scss.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/@yaireo/tagify/dist/tagify.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/tiny-slider/dist/min/tiny-slider.js') }}"></script>
<script src="{{ asset('js/assets/libs/@popperjs/core/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/tippy.js/dist/tippy-bundle.umd.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/typed.js/lib/typed.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jsvectormap/dist/js/jsvectormap.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jsvectormap/dist/maps/world.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>


<!-- clipboard -->


<!-- Theme JS -->
<script src="{{ asset('js/assets/js/theme.min.js') }}"></script>
<!-- <script src="../../assets/libs/@fortawesome/fontawesome-free/js/all.js"></script> -->


<!-- clipboard -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.12/clipboard.min.js"></script>


<!-- Theme JS -->

{{--    <link rel="stylesheet" href="/resources/demos/style.css">--}}
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script>
    document.querySelectorAll('.dropdown-list-group-item').forEach(function (item){
        item.addEventListener("click", function (){
            var inputs = document.querySelectorAll("input[type='checkbox']");
            for(var i = 0; i < inputs.length; i++) {
                inputs[i].checked = false;
            }
            document.querySelector('input[name="'+this.getAttribute('data-value')+'"]').checked = true;
            fetchSort()
        })
    })
</script>
<script>
    $(document).ready(function () {
        $.ajax({
            url: "/products/ajax",
            method: "POST",
            data: {
                _token: "{{ csrf_token() }}"
            },
            success: async function (response) {
                const source = [];
                $.each(response, function (index, value) {
                    source.push({
                        value: "/product/show/" + value['id'],
                        label: value['h_one']
                    })
                })
                autocompleteTest(source)
            }
        })
    })

    function autocompleteTest(response) {
        $(document).ready(function () {
            $("#search").autocomplete({
                source: response,
                select: function (event, ui) {
                    window.location.href = ui.item.value;
                }
            })
        })
    }
</script>
<script>
    $(document).ready(function () {
        $('#auth').click(function () {
            $('.b-popup ,.b-popup-content').show()
        })

        $('#exit-popap').click(function () {
            $('.b-popup ,.b-popup-content').hide()
        })
    })
</script>
<script>
    var span = document.getElementById('delete-notifications')

    span.addEventListener("click", function () {
        fetch('/notification/all/delete', {
            method: "post",
            headers: {
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
        }).then(async function (response) {
            location.reload()
        })
    })
</script>
</body>

</html>
