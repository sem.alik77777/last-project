<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Geeks is a fully responsive and yet modern premium bootstrap template. Geek is feature-rich components and beautifully designed pages that help you create the best possible website and web application projects.">
    <meta name="keywords"
          content="Geeks UI, bootstrap, bootstrap 5, Course, Sass, landing, Marketing, admin themes, bootstrap admin, bootstrap dashboard, ui kit, web app, multipurpose">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon icon-->
    <style data-tippy-stylesheet="">.tippy-box[data-animation=fade][data-state=hidden] {
            opacity: 0
        }
        .layer {
            overflow: scroll; /* Добавляем полосы прокрутки */
            width: 300px; /* Ширина блока */
            height: 150px; /* Высота блока */
            padding: 5px; /* Поля вокруг текста */
            border: solid 1px black; /* Параметры рамки */
        }
        [data-tippy-root] {
            max-width: calc(100vw - 10px)
        }

        .tippy-box {
            position: relative;
            background-color: #333;
            color: #fff;
            border-radius: 4px;
            font-size: 14px;
            line-height: 1.4;
            outline: 0;
            transition-property: transform, visibility, opacity
        }

        .tippy-box[data-placement^=top] > .tippy-arrow {
            bottom: 0
        }

        * {
            font-family: Areal;
        }

        #exit-popap {
            float: right;
            cursor: pointer;
        }

        .b-popup {
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            overflow: hidden;
            position: fixed;
            top: 0px;
            z-index: 1000;
            display: none;
        }

        .b-popup .b-popup-content {
            margin: 40px auto 0px auto;
            width: 500px;
            height: 300px;
            text-align: center;
            font-size: 20px;
            display: none;
            padding: 10px;
            background-color: #c5c5c5;
            border-radius: 5px;
            box-shadow: 0px 0px 10px #000;
        }

        .logos {
            margin-top: 200px;
        }

        .tippy-box[data-placement^=top] > .tippy-arrow:before {
            bottom: -7px;
            left: 0;
            border-width: 8px 8px 0;
            border-top-color: initial;
            transform-origin: center top
        }

        .tippy-box[data-placement^=bottom] > .tippy-arrow {
            top: 0
        }

        .tippy-box[data-placement^=bottom] > .tippy-arrow:before {
            top: -7px;
            left: 0;
            border-width: 0 8px 8px;
            border-bottom-color: initial;
            transform-origin: center bottom
        }

        .tippy-box[data-placement^=left] > .tippy-arrow {
            right: 0
        }

        .tippy-box[data-placement^=left] > .tippy-arrow:before {
            border-width: 8px 0 8px 8px;
            border-left-color: initial;
            right: -7px;
            transform-origin: center left
        }

        .tippy-box[data-placement^=right] > .tippy-arrow {
            left: 0
        }

        .tippy-box[data-placement^=right] > .tippy-arrow:before {
            left: -7px;
            border-width: 8px 8px 8px 0;
            border-right-color: initial;
            transform-origin: center right
        }

        .tippy-box[data-inertia][data-state=visible] {
            transition-timing-function: cubic-bezier(.54, 1.5, .38, 1.11)
        }

        .tippy-arrow {
            width: 16px;
            height: 16px;
            color: #333
        }

        .tippy-arrow:before {
            content: "";
            position: absolute;
            border-color: transparent;
            border-style: solid
        }

        .tippy-content {
            position: relative;
            padding: 5px 9px;
            z-index: 1
        }</style>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon/favicon.ico">


    <!-- Libs CSS -->

    <link href="{{ asset('js/assets/fonts/feather/feather.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/bootstrap-icons/font/bootstrap-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/assets/libs/dragula/dist/dragula.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/assets/libs/@mdi/font/css/materialdesignicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/assets/libs/prismjs/themes/prism.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/assets/libs/dropzone/dist/dropzone.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/assets/libs/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet" />
    <link href="{{ asset('js/assets/libs/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/@yaireo/tagify/dist/tagify.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/tiny-slider/dist/tiny-slider.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/tippy.js/dist/tippy.css') }}" rel="stylesheet">
    <link href="{{ asset('js/assets/libs/datatables.net-bs5/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/theme.min.css') }}" rel="stylesheet">
    <!-- Bootstrap Font Icon CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">



    <!-- Theme CSS -->
{{--    <link rel="stylesheet" href="{{ asset('css/theme.min.css') }}">--}}
    <title>Courses Filter | Geeks - Bootstrap 5 Template</title>
    <style type="text/css">.apexcharts-canvas {
            position: relative;
            user-select: none;
            /* cannot give overflow: hidden as it will crop tooltips which overflow outside chart area */
        }


        /* scrollbar is not visible by default for legend, hence forcing the visibility */
        .apexcharts-canvas ::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 6px;
        }

        .apexcharts-canvas ::-webkit-scrollbar-thumb {
            border-radius: 4px;
            background-color: rgba(0, 0, 0, .5);
            box-shadow: 0 0 1px rgba(255, 255, 255, .5);
            -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);
        }


        .apexcharts-inner {
            position: relative;
        }

        .apexcharts-text tspan {
            font-family: inherit;
        }

        .legend-mouseover-inactive {
            transition: 0.15s ease all;
            opacity: 0.20;
        }

        .apexcharts-series-collapsed {
            opacity: 0;
        }

        .apexcharts-tooltip {
            border-radius: 5px;
            box-shadow: 2px 2px 6px -4px #999;
            cursor: default;
            font-size: 14px;
            left: 62px;
            opacity: 0;
            pointer-events: none;
            position: absolute;
            top: 20px;
            display: flex;
            flex-direction: column;
            overflow: hidden;
            white-space: nowrap;
            z-index: 12;
            transition: 0.15s ease all;
        }

        .apexcharts-tooltip.apexcharts-active {
            opacity: 1;
            transition: 0.15s ease all;
        }

        .apexcharts-tooltip.apexcharts-theme-light {
            border: 1px solid #e3e3e3;
            background: rgba(255, 255, 255, 0.96);
        }

        .apexcharts-tooltip.apexcharts-theme-dark {
            color: #fff;
            background: rgba(30, 30, 30, 0.8);
        }

        .apexcharts-tooltip * {
            font-family: inherit;
        }


        .apexcharts-tooltip-title {
            padding: 6px;
            font-size: 15px;
            margin-bottom: 4px;
        }

        .apexcharts-tooltip.apexcharts-theme-light .apexcharts-tooltip-title {
            background: #ECEFF1;
            border-bottom: 1px solid #ddd;
        }

        .apexcharts-tooltip.apexcharts-theme-dark .apexcharts-tooltip-title {
            background: rgba(0, 0, 0, 0.7);
            border-bottom: 1px solid #333;
        }

        .apexcharts-tooltip-text-y-value,
        .apexcharts-tooltip-text-goals-value,
        .apexcharts-tooltip-text-z-value {
            display: inline-block;
            font-weight: 600;
            margin-left: 5px;
        }

        .apexcharts-tooltip-text-y-label:empty,
        .apexcharts-tooltip-text-y-value:empty,
        .apexcharts-tooltip-text-goals-label:empty,
        .apexcharts-tooltip-text-goals-value:empty,
        .apexcharts-tooltip-text-z-value:empty {
            display: none;
        }

        .apexcharts-tooltip-text-y-value,
        .apexcharts-tooltip-text-goals-value,
        .apexcharts-tooltip-text-z-value {
            font-weight: 600;
        }

        .apexcharts-tooltip-text-goals-label,
        .apexcharts-tooltip-text-goals-value {
            padding: 6px 0 5px;
        }

        .apexcharts-tooltip-goals-group,
        .apexcharts-tooltip-text-goals-label,
        .apexcharts-tooltip-text-goals-value {
            display: flex;
        }

        .apexcharts-tooltip-text-goals-label:not(:empty),
        .apexcharts-tooltip-text-goals-value:not(:empty) {
            margin-top: -6px;
        }

        .apexcharts-tooltip-marker {
            width: 12px;
            height: 12px;
            position: relative;
            top: 0px;
            margin-right: 10px;
            border-radius: 50%;
        }

        .apexcharts-tooltip-series-group {
            padding: 0 10px;
            display: none;
            text-align: left;
            justify-content: left;
            align-items: center;
        }

        .apexcharts-tooltip-series-group.apexcharts-active .apexcharts-tooltip-marker {
            opacity: 1;
        }

        .apexcharts-tooltip-series-group.apexcharts-active,
        .apexcharts-tooltip-series-group:last-child {
            padding-bottom: 4px;
        }

        .apexcharts-tooltip-series-group-hidden {
            opacity: 0;
            height: 0;
            line-height: 0;
            padding: 0 !important;
        }

        .apexcharts-tooltip-y-group {
            padding: 6px 0 5px;
        }

        .apexcharts-tooltip-box, .apexcharts-custom-tooltip {
            padding: 4px 8px;
        }

        .apexcharts-tooltip-boxPlot {
            display: flex;
            flex-direction: column-reverse;
        }

        .apexcharts-tooltip-box > div {
            margin: 4px 0;
        }

        .apexcharts-tooltip-box span.value {
            font-weight: bold;
        }

        .apexcharts-tooltip-rangebar {
            padding: 5px 8px;
        }

        .apexcharts-tooltip-rangebar .category {
            font-weight: 600;
            color: #777;
        }

        .apexcharts-tooltip-rangebar .series-name {
            font-weight: bold;
            display: block;
            margin-bottom: 5px;
        }

        .apexcharts-xaxistooltip {
            opacity: 0;
            padding: 9px 10px;
            pointer-events: none;
            color: #373d3f;
            font-size: 13px;
            text-align: center;
            border-radius: 2px;
            position: absolute;
            z-index: 10;
            background: #ECEFF1;
            border: 1px solid #90A4AE;
            transition: 0.15s ease all;
        }

        .apexcharts-xaxistooltip.apexcharts-theme-dark {
            background: rgba(0, 0, 0, 0.7);
            border: 1px solid rgba(0, 0, 0, 0.5);
            color: #fff;
        }

        .apexcharts-xaxistooltip:after,
        .apexcharts-xaxistooltip:before {
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .apexcharts-xaxistooltip:after {
            border-color: rgba(236, 239, 241, 0);
            border-width: 6px;
            margin-left: -6px;
        }

        .apexcharts-xaxistooltip:before {
            border-color: rgba(144, 164, 174, 0);
            border-width: 7px;
            margin-left: -7px;
        }

        .apexcharts-xaxistooltip-bottom:after,
        .apexcharts-xaxistooltip-bottom:before {
            bottom: 100%;
        }

        .apexcharts-xaxistooltip-top:after,
        .apexcharts-xaxistooltip-top:before {
            top: 100%;
        }

        .apexcharts-xaxistooltip-bottom:after {
            border-bottom-color: #ECEFF1;
        }

        .apexcharts-xaxistooltip-bottom:before {
            border-bottom-color: #90A4AE;
        }

        .apexcharts-xaxistooltip-bottom.apexcharts-theme-dark:after {
            border-bottom-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-xaxistooltip-bottom.apexcharts-theme-dark:before {
            border-bottom-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-xaxistooltip-top:after {
            border-top-color: #ECEFF1
        }

        .apexcharts-xaxistooltip-top:before {
            border-top-color: #90A4AE;
        }

        .apexcharts-xaxistooltip-top.apexcharts-theme-dark:after {
            border-top-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-xaxistooltip-top.apexcharts-theme-dark:before {
            border-top-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-xaxistooltip.apexcharts-active {
            opacity: 1;
            transition: 0.15s ease all;
        }

        .apexcharts-yaxistooltip {
            opacity: 0;
            padding: 4px 10px;
            pointer-events: none;
            color: #373d3f;
            font-size: 13px;
            text-align: center;
            border-radius: 2px;
            position: absolute;
            z-index: 10;
            background: #ECEFF1;
            border: 1px solid #90A4AE;
        }

        .apexcharts-yaxistooltip.apexcharts-theme-dark {
            background: rgba(0, 0, 0, 0.7);
            border: 1px solid rgba(0, 0, 0, 0.5);
            color: #fff;
        }

        .apexcharts-yaxistooltip:after,
        .apexcharts-yaxistooltip:before {
            top: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .apexcharts-yaxistooltip:after {
            border-color: rgba(236, 239, 241, 0);
            border-width: 6px;
            margin-top: -6px;
        }

        .apexcharts-yaxistooltip:before {
            border-color: rgba(144, 164, 174, 0);
            border-width: 7px;
            margin-top: -7px;
        }

        .apexcharts-yaxistooltip-left:after,
        .apexcharts-yaxistooltip-left:before {
            left: 100%;
        }

        .apexcharts-yaxistooltip-right:after,
        .apexcharts-yaxistooltip-right:before {
            right: 100%;
        }

        .apexcharts-yaxistooltip-left:after {
            border-left-color: #ECEFF1;
        }

        .apexcharts-yaxistooltip-left:before {
            border-left-color: #90A4AE;
        }

        .apexcharts-yaxistooltip-left.apexcharts-theme-dark:after {
            border-left-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-yaxistooltip-left.apexcharts-theme-dark:before {
            border-left-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-yaxistooltip-right:after {
            border-right-color: #ECEFF1;
        }

        .apexcharts-yaxistooltip-right:before {
            border-right-color: #90A4AE;
        }

        .apexcharts-yaxistooltip-right.apexcharts-theme-dark:after {
            border-right-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-yaxistooltip-right.apexcharts-theme-dark:before {
            border-right-color: rgba(0, 0, 0, 0.5);
        }

        .apexcharts-yaxistooltip.apexcharts-active {
            opacity: 1;
        }

        .apexcharts-yaxistooltip-hidden {
            display: none;
        }

        .apexcharts-xcrosshairs,
        .apexcharts-ycrosshairs {
            pointer-events: none;
            opacity: 0;
            transition: 0.15s ease all;
        }

        .apexcharts-xcrosshairs.apexcharts-active,
        .apexcharts-ycrosshairs.apexcharts-active {
            opacity: 1;
            transition: 0.15s ease all;
        }

        .apexcharts-ycrosshairs-hidden {
            opacity: 0;
        }

        .apexcharts-selection-rect {
            cursor: move;
        }

        .svg_select_boundingRect, .svg_select_points_rot {
            pointer-events: none;
            opacity: 0;
            visibility: hidden;
        }

        .apexcharts-selection-rect + g .svg_select_boundingRect,
        .apexcharts-selection-rect + g .svg_select_points_rot {
            opacity: 0;
            visibility: hidden;
        }

        .apexcharts-selection-rect + g .svg_select_points_l,
        .apexcharts-selection-rect + g .svg_select_points_r {
            cursor: ew-resize;
            opacity: 1;
            visibility: visible;
        }

        .svg_select_points {
            fill: #efefef;
            stroke: #333;
            rx: 2;
        }

        .apexcharts-svg.apexcharts-zoomable.hovering-zoom {
            cursor: crosshair
        }

        .apexcharts-svg.apexcharts-zoomable.hovering-pan {
            cursor: move
        }

        .apexcharts-zoom-icon,
        .apexcharts-zoomin-icon,
        .apexcharts-zoomout-icon,
        .apexcharts-reset-icon,
        .apexcharts-pan-icon,
        .apexcharts-selection-icon,
        .apexcharts-menu-icon,
        .apexcharts-toolbar-custom-icon {
            cursor: pointer;
            width: 20px;
            height: 20px;
            line-height: 24px;
            color: #6E8192;
            text-align: center;
        }

        .apexcharts-zoom-icon svg,
        .apexcharts-zoomin-icon svg,
        .apexcharts-zoomout-icon svg,
        .apexcharts-reset-icon svg,
        .apexcharts-menu-icon svg {
            fill: #6E8192;
        }

        .apexcharts-selection-icon svg {
            fill: #444;
            transform: scale(0.76)
        }

        .apexcharts-theme-dark .apexcharts-zoom-icon svg,
        .apexcharts-theme-dark .apexcharts-zoomin-icon svg,
        .apexcharts-theme-dark .apexcharts-zoomout-icon svg,
        .apexcharts-theme-dark .apexcharts-reset-icon svg,
        .apexcharts-theme-dark .apexcharts-pan-icon svg,
        .apexcharts-theme-dark .apexcharts-selection-icon svg,
        .apexcharts-theme-dark .apexcharts-menu-icon svg,
        .apexcharts-theme-dark .apexcharts-toolbar-custom-icon svg {
            fill: #f3f4f5;
        }

        .apexcharts-canvas .apexcharts-zoom-icon.apexcharts-selected svg,
        .apexcharts-canvas .apexcharts-selection-icon.apexcharts-selected svg,
        .apexcharts-canvas .apexcharts-reset-zoom-icon.apexcharts-selected svg {
            fill: #008FFB;
        }

        .apexcharts-theme-light .apexcharts-selection-icon:not(.apexcharts-selected):hover svg,
        .apexcharts-theme-light .apexcharts-zoom-icon:not(.apexcharts-selected):hover svg,
        .apexcharts-theme-light .apexcharts-zoomin-icon:hover svg,
        .apexcharts-theme-light .apexcharts-zoomout-icon:hover svg,
        .apexcharts-theme-light .apexcharts-reset-icon:hover svg,
        .apexcharts-theme-light .apexcharts-menu-icon:hover svg {
            fill: #333;
        }

        .apexcharts-selection-icon,
        .apexcharts-menu-icon {
            position: relative;
        }

        .apexcharts-reset-icon {
            margin-left: 5px;
        }

        .apexcharts-zoom-icon,
        .apexcharts-reset-icon,
        .apexcharts-menu-icon {
            transform: scale(0.85);
        }

        .apexcharts-zoomin-icon,
        .apexcharts-zoomout-icon {
            transform: scale(0.7)
        }

        .apexcharts-zoomout-icon {
            margin-right: 3px;
        }

        .apexcharts-pan-icon {
            transform: scale(0.62);
            position: relative;
            left: 1px;
            top: 0px;
        }

        .apexcharts-pan-icon svg {
            fill: #fff;
            stroke: #6E8192;
            stroke-width: 2;
        }

        .apexcharts-pan-icon.apexcharts-selected svg {
            stroke: #008FFB;
        }

        .apexcharts-pan-icon:not(.apexcharts-selected):hover svg {
            stroke: #333;
        }

        .apexcharts-toolbar {
            position: absolute;
            z-index: 11;
            max-width: 176px;
            text-align: right;
            border-radius: 3px;
            padding: 0px 6px 2px 6px;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .apexcharts-menu {
            background: #fff;
            position: absolute;
            top: 100%;
            border: 1px solid #ddd;
            border-radius: 3px;
            padding: 3px;
            right: 10px;
            opacity: 0;
            min-width: 110px;
            transition: 0.15s ease all;
            pointer-events: none;
        }

        .apexcharts-menu.apexcharts-menu-open {
            opacity: 1;
            pointer-events: all;
            transition: 0.15s ease all;
        }

        .apexcharts-menu-item {
            padding: 6px 7px;
            font-size: 12px;
            cursor: pointer;
        }

        .apexcharts-theme-light .apexcharts-menu-item:hover {
            background: #eee;
        }

        .apexcharts-theme-dark .apexcharts-menu {
            background: rgba(0, 0, 0, 0.7);
            color: #fff;
        }

        @media screen and (min-width: 768px) {
            .apexcharts-canvas:hover .apexcharts-toolbar {
                opacity: 1;
            }
        }

        .apexcharts-datalabel.apexcharts-element-hidden {
            opacity: 0;
        }

        .apexcharts-pie-label,
        .apexcharts-datalabels,
        .apexcharts-datalabel,
        .apexcharts-datalabel-label,
        .apexcharts-datalabel-value {
            cursor: default;
            pointer-events: none;
        }

        .apexcharts-pie-label-delay {
            opacity: 0;
            animation-name: opaque;
            animation-duration: 0.3s;
            animation-fill-mode: forwards;
            animation-timing-function: ease;
        }

        .apexcharts-canvas .apexcharts-element-hidden {
            opacity: 0;
        }

        .apexcharts-hide .apexcharts-series-points {
            opacity: 0;
        }

        .apexcharts-gridline,
        .apexcharts-annotation-rect,
        .apexcharts-tooltip .apexcharts-marker,
        .apexcharts-area-series .apexcharts-area,
        .apexcharts-line,
        .apexcharts-zoom-rect,
        .apexcharts-toolbar svg,
        .apexcharts-area-series .apexcharts-series-markers .apexcharts-marker.no-pointer-events,
        .apexcharts-line-series .apexcharts-series-markers .apexcharts-marker.no-pointer-events,
        .apexcharts-radar-series path,
        .apexcharts-radar-series polygon {
            pointer-events: none;
        }


        /* markers */

        .apexcharts-marker {
            transition: 0.15s ease all;
        }

        @keyframes opaque {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }


        /* Resize generated styles */

        @keyframes resizeanim {
            from {
                opacity: 0;
            }
            to {
                opacity: 0;
            }
        }

        .resize-triggers {
            animation: 1ms resizeanim;
            visibility: hidden;
            opacity: 0;
        }

        .resize-triggers,
        .resize-triggers > div,
        .contract-trigger:before {
            content: " ";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }

        .resize-triggers > div {
            background: #eee;
            overflow: auto;
        }

        .contract-trigger:before {
            width: 200%;
            height: 200%;
        }</style>
</head>

<body>
<!-- Page content -->
<?php $notification = \App\Models\Notification::query()->where('user_id', \Illuminate\Support\Facades\Auth::id())->where('read', 0)->get() ?>
<div class="layer">
    <h2>Duis te feugifacilisi</h2>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diem
        nonummy nibh euismod tincidunt ut lacreet dolore magna aliguam erat volutpat.
        Ut wisis enim ad minim veniam, quis nostrud exerci tution ullamcorper suscipit
        lobor
    </p>
</div>
<nav class="navbar navbar-expand-lg navbar-default">
    <div class="container-fluid px-0">
        <a class="navbar-brand" href="../index.html"><img src="../assets/images/brand/logo/logo.svg" alt=""></a>
        <!-- Mobile view nav wrap -->
        <ul class="navbar-nav navbar-right-wrap ms-auto d-lg-none d-flex nav-top-wrap">
            <li class="dropdown stopevent">
                <a class="btn btn-light btn-icon rounded-circle text-muted indicator indicator-primary" href="#"
                   role="button" data-bs-toggle="dropdown">
                    <i class="fe fe-bell"> </i>
                </a>
                <div class="dropdown-menu dropdown-menu-end shadow">
                    <div>
                        <div class="border-bottom px-3 pb-3 d-flex justify-content-between align-items-center">
                            <span class="h5 mb-0">Notifications</span>


                            <a href="# " class="text-muted"><span class="align-middle"><i
                                        class="fe fe-settings me-1"></i></span></a>
                        </div>
                        <div class="slimScrollDiv"
                             style="position: relative; overflow: hidden; width: auto; height: 300px;">
                            <ul class="list-group list-group-flush notification-list-scroll"
                                style="overflow: hidden; width: auto; height: 300px;">
                                @foreach($notification as $value)
                                    <li class="list-group-item bg-light">
                                        <div class="row">
                                            <div class="col">
                                                <a href="#" class="text-body">
                                                    <div class="d-flex">
                                                        <img src="{{ $value->user->avatar }}" alt=""
                                                             class="avatar-md rounded-circle">
                                                        <div class="ms-3">
                                                            <h5 class="fw-bold mb-1">Kristin Watson:</h5>
                                                            <p class="mb-3">
                                                                Krisitn Watsan like your comment on course Javascript
                                                                Introduction!
                                                            </p>
                                                            <span class="fs-6 text-muted">
													<span><span class="fe fe-thumbs-up text-success me-1"></span>2 hours ago,</span>
													<span class="ms-1">2:19 PM</span>
												</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-auto text-center me-2">
                                                <a href="#" class="badge-dot bg-info" data-bs-toggle="tooltip"
                                                   data-bs-placement="top" title=""
                                                   data-bs-original-title="Mark as read">
                                                </a>
                                                <div>
                                                    <a href="#" data-bs-toggle="tooltip" data-bs-placement="top"
                                                       title=""
                                                       data-bs-original-title="Remove">
                                                        <i class="fe fe-x text-muted"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                                {{--                                <li class="list-group-item">--}}
                                {{--                                    <div class="row">--}}
                                {{--                                        <div class="col">--}}
                                {{--                                            <a href="#" class="text-body">--}}
                                {{--                                                <div class="d-flex">--}}
                                {{--                                                    <img src="../assets/images/avatar/avatar-2.jpg" alt=""--}}
                                {{--                                                         class="avatar-md rounded-circle">--}}
                                {{--                                                    <div class="ms-3">--}}
                                {{--                                                        <h5 class="fw-bold mb-1">Brooklyn Simmons</h5>--}}
                                {{--                                                        <p class="mb-3">--}}
                                {{--                                                            Just launched a new Courses React for Beginner.--}}
                                {{--                                                        </p>--}}
                                {{--                                                        <span class="fs-6 text-muted">--}}
                                {{--													<span><span class="fe fe-thumbs-up text-success me-1"></span>Oct 9,</span>--}}
                                {{--													<span class="ms-1">1:20 PM</span>--}}
                                {{--												</span>--}}
                                {{--                                                    </div>--}}
                                {{--                                                </div>--}}
                                {{--                                            </a>--}}
                                {{--                                        </div>--}}
                                {{--                                        <div class="col-auto text-center me-2">--}}
                                {{--                                            <a href="#" class="badge-dot bg-secondary" data-bs-toggle="tooltip"--}}
                                {{--                                               data-bs-placement="top" title="" data-bs-original-title="Mark as unread">--}}
                                {{--                                            </a>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </li>--}}
                                {{--                                <li class="list-group-item">--}}
                                {{--                                    <div class="row">--}}
                                {{--                                        <div class="col">--}}
                                {{--                                            <a href="#" class="text-body">--}}
                                {{--                                                <div class="d-flex">--}}
                                {{--                                                    <img src="../assets/images/avatar/avatar-3.jpg" alt=""--}}
                                {{--                                                         class="avatar-md rounded-circle">--}}
                                {{--                                                    <div class="ms-3">--}}
                                {{--                                                        <h5 class="fw-bold mb-1">Jenny Wilson</h5>--}}
                                {{--                                                        <p class="mb-3">--}}
                                {{--                                                            Krisitn Watsan like your comment on course Javascript--}}
                                {{--                                                            Introduction!--}}
                                {{--                                                        </p>--}}
                                {{--                                                        <span class="fs-6 text-muted">--}}
                                {{--													<span><span--}}
                                {{--                                                            class="fe fe-thumbs-up text-info me-1"></span>Oct 9,</span>--}}
                                {{--													<span class="ms-1">1:56 PM</span>--}}
                                {{--												</span>--}}
                                {{--                                                    </div>--}}
                                {{--                                                </div>--}}
                                {{--                                            </a>--}}
                                {{--                                        </div>--}}
                                {{--                                        <div class="col-auto text-center me-2">--}}
                                {{--                                            <a href="#" class="badge-dot bg-secondary" data-bs-toggle="tooltip"--}}
                                {{--                                               data-bs-placement="top" title="" data-bs-original-title="Mark as unread">--}}
                                {{--                                            </a>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </li>--}}
                                {{--                                <li class="list-group-item">--}}
                                {{--                                    <div class="row">--}}
                                {{--                                        <div class="col">--}}
                                {{--                                            <a href="#" class="text-body">--}}
                                {{--                                                <div class="d-flex">--}}
                                {{--                                                    <img src="../assets/images/avatar/avatar-4.jpg" alt=""--}}
                                {{--                                                         class="avatar-md rounded-circle">--}}
                                {{--                                                    <div class="ms-3">--}}
                                {{--                                                        <h5 class="fw-bold mb-1">Sina Ray</h5>--}}
                                {{--                                                        <p class="mb-3">--}}
                                {{--                                                            You earn new certificate for complete the Javascript--}}
                                {{--                                                            Beginner course.--}}
                                {{--                                                        </p>--}}
                                {{--                                                        <span class="fs-6 text-muted">--}}
                                {{--													<span><span--}}
                                {{--                                                            class="fe fe-award text-warning me-1"></span>Oct 9,</span>--}}
                                {{--													<span class="ms-1">1:56 PM</span>--}}
                                {{--												</span>--}}
                                {{--                                                    </div>--}}
                                {{--                                                </div>--}}
                                {{--                                            </a>--}}
                                {{--                                        </div>--}}
                                {{--                                        <div class="col-auto text-center me-2">--}}
                                {{--                                            <a href="#" class="badge-dot bg-secondary" data-bs-toggle="tooltip"--}}
                                {{--                                               data-bs-placement="top" title="" data-bs-original-title="Mark as unread">--}}
                                {{--                                            </a>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </li>--}}
                            </ul>
                            <div class="slimScrollBar"
                                 style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                            <div class="slimScrollRail"
                                 style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                        </div>
                        <div class="border-top px-3 pt-3 pb-0">
                            <a href="../pages/notification-history.html" class="text-link fw-semi-bold">See all
                                Notifications</a>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown ms-2">
                <a class="rounded-circle" href="#" role="button" data-bs-toggle="dropdown">
                    <div class="avatar avatar-md avatar-indicators avatar-online">
                        <img alt="avatar" src="../assets/images/avatar/avatar-1.jpg" class="rounded-circle">
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-end shadow">
                    <div class="dropdown-item">
                        <div class="d-flex">
                            <div class="avatar avatar-md avatar-indicators avatar-online">
                                <img alt="avatar" src="../assets/images/avatar/avatar-1.jpg" class="rounded-circle">
                            </div>
                            <div class="ms-3 lh-1">
                                <h5 class="mb-1">Annette Black</h5>
                                <p class="mb-0 text-muted">annette@geeksui.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>
                    <ul class="list-unstyled">
                        <li class="dropdown-submenu">
                            <a class="dropdown-item dropdown-list-group-item dropdown-toggle" href="#">
                                <i class="fe fe-circle me-2"></i>Status
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <span class="badge-dot bg-success me-2"></span>Online
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <span class="badge-dot bg-secondary me-2"></span>Offline
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <span class="badge-dot bg-warning me-2"></span>Away
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <span class="badge-dot bg-danger me-2"></span>Busy
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a class="dropdown-item" href="../pages/profile-edit.html">
                                <i class="fe fe-user me-2"></i>Profile
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/student-subscriptions.html">
                                <i class="fe fe-star me-2"></i>Subscription
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                <i class="fe fe-settings me-2"></i>Settings
                            </a>
                        </li>
                    </ul>
                    <div class="dropdown-divider"></div>
                    <ul class="list-unstyled">
                        <li>
                            <a class="dropdown-item" href="../index.html">
                                <i class="fe fe-power me-2"></i>Sign Out
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- Button -->
        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="icon-bar top-bar mt-0"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbar-default">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link bi-chevron-compact-down" href="#" id="navbarBrowse" data-bs-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false" data-bs-display="static">
                        Browse
                    </a>
                    <ul class="dropdown-menu dropdown-menu-arrow" aria-labelledby="navbarBrowse">
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Web Development
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Bootstrap</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        React
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        GraphQl</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Gatsby</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Grunt</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Svelte</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Meteor</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        HTML5</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Angular</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Design
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Graphic Design</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Illustrator
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        UX / UI Design</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Figma Design</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Adobe XD</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Sketch</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Icon Design</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Photoshop</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="../pages/course-category.html" class="dropdown-item">
                                Mobile App
                            </a>
                        </li>
                        <li>
                            <a href="../pages/course-category.html" class="dropdown-item">
                                IT Software
                            </a>
                        </li>
                        <li>
                            <a href="../pages/course-category.html" class="dropdown-item">
                                Marketing
                            </a>
                        </li>
                        <li>
                            <a href="../pages/course-category.html" class="dropdown-item">
                                Music
                            </a>
                        </li>
                        <li>
                            <a href="../pages/course-category.html" class="dropdown-item">
                                Life Style
                            </a>
                        </li>
                        <li>
                            <a href="../pages/course-category.html" class="dropdown-item">
                                Business
                            </a>
                        </li>
                        <li>
                            <a href="../pages/course-category.html" class="dropdown-item">
                                Photography
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link bi-chevron-compact-down" href="#" id="navbarLanding" data-bs-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Landings
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarLanding">
                        <li>
                            <h4 class="dropdown-header">Landings</h4>
                        </li>
                        <li>
                            <a href="../pages/landings/landing-courses.html" class="dropdown-item">
                                Courses
                            </a>
                        </li>
                        <li>
                            <a href="../pages/landings/course-lead.html" class="dropdown-item">
                                Lead Course
                            </a>
                        </li>
                        <li>
                            <a href="../pages/landings/request-access.html" class="dropdown-item">
                                Request Access
                            </a>
                        </li>
                        <li>
                            <a href="../pages/landings/landing-sass.html" class="dropdown-item">
                                SaaS
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link bi-chevron-compact-down" href="#" id="navbarPages" data-bs-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Pages
                    </a>
                    <ul class="dropdown-menu dropdown-menu-arrow" aria-labelledby="navbarPages">
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Courses
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="../pages/course-single.html">
                                        Course Single
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-single-v2.html">
                                        Course Single v2
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-resume.html">
                                        Course Resume
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-category.html">
                                        Course Category
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-checkout.html">
                                        Course Checkout
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/course-filter-list.html">
                                        Course List/Grid
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/add-course.html">
                                        Add New Course
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Paths
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="../pages/course-path.html" class="dropdown-item">
                                        Browse Path
                                    </a>
                                </li>
                                <li>
                                    <a href="../pages/course-path-single.html" class="dropdown-item">
                                        Path Single
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Blog
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="../pages/blog.html">
                                        Listing</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/blog-single.html">
                                        Article
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/blog-category.html">
                                        Category</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/blog-sidebar.html">
                                        Sidebar</a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Career
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="../pages/career.html">
                                        Overview</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/career-list.html">
                                        Listing
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/career-single.html">
                                        Opening</a>
                                </li>

                            </ul>
                        </li>
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Specialty
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="../pages/coming-soon.html">
                                        Coming Soon
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/404-error.html">
                                        Error 404
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/maintenance-mode.html">
                                        Maintenance Mode
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/terms-condition-page.html">
                                        Terms &amp; Conditions
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <hr class="mx-3">
                        </li>


                        <li>
                            <a class="dropdown-item" href="../pages/about.html">
                                About
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">
                                Help Center <span class="badge bg-success ms-1">Pro</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/pricing.html">
                                Pricing
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/compare-plan.html">
                                Compare Plan
                            </a>
                        </li>

                        <li>
                            <a class="dropdown-item" href="../pages/contact.html">
                                Contact
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link bi-chevron-compact-down" href="#" id="navbarAccount" data-bs-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Accounts
                    </a>
                    <ul class="dropdown-menu dropdown-menu-arrow" aria-labelledby="navbarAccount">
                        <li>
                            <h4 class="dropdown-header">Accounts</h4>
                        </li>
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Instructor
                            </a>
                            <ul class="dropdown-menu">
                                <li class="text-wrap">
                                    <h5 class="dropdown-header text-dark">Instructor</h5>
                                    <p class="dropdown-text mb-0">
                                        Instructor dashboard for manage courses and earning.
                                    </p>
                                </li>
                                <li>
                                    <hr class="mx-3">
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/dashboard-instructor.html">
                                        Dashboard</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/instructor-profile.html">
                                        Profile</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/instructor-courses.html">
                                        My Courses
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/instructor-order.html">
                                        Orders</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/instructor-reviews.html">
                                        Reviews</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/instructor-students.html">
                                        Students</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/instructor-payouts.html">
                                        Payouts</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/instructor-earning.html">
                                        Earning</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Students
                            </a>
                            <ul class="dropdown-menu">
                                <li class="text-wrap">
                                    <h5 class="dropdown-header text-dark">Students</h5>
                                    <p class="dropdown-text mb-0">
                                        Students dashboard to manage your courses and subscriptions.
                                    </p>
                                </li>
                                <li>
                                    <hr class="mx-3">
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/dashboard-student.html">
                                        Dashboard</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/student-subscriptions.html">
                                        Subscriptions
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/payment-method.html">
                                        Payments</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/billing-info.html">
                                        Billing Info</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/invoice.html">
                                        Invoice</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/invoice-details.html">
                                        Invoice Details</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/dashboard-student.html">
                                        bi-bookmarkarked</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/dashboard-student.html">
                                        My Path</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu dropend">
                            <a class="dropdown-item bi-chevron-compact-right" href="#">
                                Admin
                            </a>
                            <ul class="dropdown-menu">
                                <li class="text-wrap">
                                    <h5 class="dropdown-header text-dark">Master Admin</h5>
                                    <p class="dropdown-text mb-0">
                                        Master admin dashboard to manage courses, user, site setting
                                        , and work with amazing apps.
                                    </p>
                                </li>
                                <li>
                                    <hr class="mx-3">
                                </li>
                                <li class="px-3 d-grid">
                                    <a href="../pages/dashboard/admin-dashboard.html" class="btn btn-sm btn-primary">Go
                                        to Dashboard</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <hr class="mx-3">
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/sign-in.html">
                                Sign In
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/sign-up.html">
                                Sign Up
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/forget-password.html">
                                Forgot Password
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/profile-edit.html">
                                Edit Profile
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/security.html">
                                Security
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/social-profile.html">
                                Social Profiles
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/notifications.html">
                                Notifications
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/profile-privacy.html">
                                Privacy Settings
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/delete-profile.html">
                                Delete Profile
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="../pages/linked-accounts.html">
                                Linked Accounts
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="bi-three-dots"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-md" aria-labelledby="navbarDropdown">
                        <div class="list-group">
                            <a class="list-group-item list-group-item-action border-0" href="../docs/index.html">
                                <div class="d-flex align-items-center">
                                    <i class="bi-file-text"></i>
                                    <div class="ms-3">
                                        <h5 class="mb-0">Documentations</h5>
                                        <p class="mb-0 fs-6">
                                            Browse the all documentation
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <a class="list-group-item list-group-item-action border-0" href="../docs/changelog.html">
                                <div class="d-flex align-items-center">
                                    <i class="bi-stack"></i>
                                    <div class="ms-3">
                                        <h5 class="mb-0">
                                            Changelog <span class="text-primary ms-1">v2.2.2</span>
                                        </h5>
                                        <p class="mb-0 fs-6">See what's new</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
            <form class="mt-3 mt-lg-0 ms-lg-3 d-flex align-items-center">
				<span class="position-absolute ps-3 search-icon">
					<i class="bi-search"></i>
				</span>
                <input type="search" class="form-control ps-6" placeholder="Search Courses">
            </form>

            <ul class="navbar-nav navbar-right-wrap ms-auto d-none d-lg-block">
                @if(auth()->check())
                    <li class="dropdown d-inline-block stopevent">
                        <a class="btn btn-light btn-icon rounded-circle text-muted indicator indicator-primary" href="#"
                           role="button" id="dropdownNotificationSecond" data-bs-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="bi-bell"> </i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end dropdown-menu-lg"
                             aria-labelledby="dropdownNotificationSecond">
                            <div>
                                <div class="border-bottom px-3 pb-3 d-flex justify-content-between align-items-center">
                                    <span class="h5 mb-0">Notifications</span>
                                    <a href="# " class="text-muted"><span class="align-middle"><i
                                                class="fe fe-settings me-1"></i></span></a>
                                </div>
                                <div class="slimScrollDiv"
                                     style="position: relative; overflow: hidden; width: auto; height: 300px;">
                                    <ul class="list-group list-group-flush notification-list-scroll "
                                        style="overflow: hidden; width: auto; height: 300px;">
                                        @foreach($notification as $value)
                                            <li class="list-group-item bg-light">
                                                <div class="row">
                                                    <div class="col">
                                                        <a class="text-body" href="#">
                                                            <div class="d-flex">
                                                                <img src="{{ asset($value->product->image) }}" alt=""
                                                                     class="avatar-md rounded-circle">
                                                                <div class="ms-3">
                                                                    <h5 class="fw-bold mb-1">{{ $value->user->name }}</h5>
                                                                    <p class="mb-3">
                                                                       {{ $value->text }}
                                                                    </p>
                                                                    <span class="fs-6 text-muted">
														<span><span class="fe fe-thumbs-up text-success me-1"></span>2 hours ago,</span>
														<span class="ms-1">2:19 PM</span>
													</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-auto text-center me-2">

                                                        <a href="#" class="badge-dot bg-info" data-bs-toggle="tooltip"
                                                           data-bs-placement="top" title=""
                                                           data-bs-original-title="Mark as read">
                                                        </a>
                                                        <div>
                                                            <a href="#" class="bg-transparent" data-bs-toggle="tooltip"
                                                               data-bs-placement="top" title=""
                                                               data-bs-original-title="Remove">
                                                                <i class="fe fe-x text-muted"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach

                                    </ul>
                                    <div class="slimScrollBar"
                                         style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 156.25px;"></div>
                                    <div class="slimScrollRail"
                                         style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div>
                                <div class="border-top px-3 pt-3 pb-0">
                                    <a href="../pages/notification-history.html" class="text-link fw-semi-bold">See all
                                        Notifications</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="dropdown ms-2 d-inline-block">
                        <a class="rounded-circle" href="#" data-bs-toggle="dropdown" data-bs-display="static"
                           aria-expanded="false">
                            <div class="avatar avatar-md avatar-indicators avatar-online">
                                <img alt="avatar" src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                     class="rounded-circle">
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <div class="dropdown-item">
                                <div class="d-flex">
                                    <div class="avatar avatar-md avatar-indicators avatar-online">
                                        <img alt="avatar" src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                             class="rounded-circle">
                                    </div>
                                    <div class="ms-3 lh-1">
                                        <h5 class="mb-1">{{ \Illuminate\Support\Facades\Auth::user()->name }}</h5>
                                        <p class="mb-0 text-muted">{{ \Illuminate\Support\Facades\Auth::user()->email }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-divider"></div>
                            <ul class="list-unstyled">
                                <li class="dropdown-submenu dropstart-lg">
                                    <a class="dropdown-item bi-chevron-compact-left" href="#">
                                        Status
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge-dot bg-success me-2"></span>Online
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge-dot bg-secondary me-2"></span>Offline
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge-dot bg-warning me-2"></span>Away
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="#">
                                                <span class="badge-dot bg-danger me-2"></span>Busy
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('personal.area') }}">
                                        <i class="bi-person"></i>Profile
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="../pages/student-subscriptions.html">
                                        <i class="bi-star"></i>Subscription
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="#">
                                        <i class="bi-gear"></i>Settings
                                    </a>
                                </li>
                            </ul>
                            <div class="dropdown-divider"></div>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="dropdown-item" href="{{ route('auth.logout') }}">
                                        <i class="bi-door-closed"></i>Sign Out
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @else
                    <button id="auth" class="btn btn-outline-warning" type="button">
                        Авторизуйтесь
                    </button>
                @endif
            </ul>
        </div>
    </div>
</nav>

<div class="b-popup">
    <div class="b-popup-content">
        Авторизуйтесь одним из способов.
        <span id="exit-popap">Выйти</span>
        <div class="logos">
            <a href="/auth/github/redirect"><img src="{{ asset('images/logo/icons8-github-50.png') }}" alt=""></a>
            <a href="/auth/linkedin/redirect"><img src="{{ asset('images/logo/icons8-линкедин-48.png') }}" alt=""></a>
            <img src="{{ asset('images/logo/icons8-facebook-48.png') }}" alt="">
        </div>
    </div>
</div>

@yield('content')

<!-- Footer -->
<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row align-items-center g-0 border-top py-2">
            <!-- Desc -->
            <div class="col-md-6 col-12 text-center text-md-start">
                <span>© 2021 Geeks. All Rights Reserved.</span>
            </div>
            <!-- Links -->
            <div class="col-12 col-md-6">
                <nav class="nav nav-footer justify-content-center justify-content-md-end">
                    <a class="nav-link active ps-0" href="#">Privacy</a>
                    <a class="nav-link" href="#">Terms </a>
                    <a class="nav-link" href="#">Feedback</a>
                    <a class="nav-link" href="#">Support</a>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Scripts -->
<!-- Libs JS -->
{{--<script src="{{asset('css/libs/jquery-slimscroll.js')}}"></script>--}}
{{--<script src="{{asset('css/libs/clipboard.js')}}"></script>--}}
{{--<script src="{{asset('css/libs/theme.min.js')}}"></script>--}}
{{--<script src="{{asset('css/libs/jquery-dist.js')}}"></script>--}}
{{--<script src="{{asset('css/libs/bootstrap-bundle.js')}}"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
{{--<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>--}}

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<script src="{{ asset('js/review-rating.js') }}"></script>
<script src="{{asset('js/assets/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{ asset('js/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/odometer/odometer.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/flatpickr/dist/flatpickr.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/inputmask/dist/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/apexcharts/dist/apexcharts.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/quill/dist/quill.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/file-upload-with-preview/dist/file-upload-with-preview.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/dragula/dist/dragula.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/bs-stepper/dist/js/bs-stepper.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/dropzone/dist/min/dropzone.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jQuery.print/jQuery.print.js') }}"></script>
<script src="{{ asset('js/assets/libs/prismjs/prism.js') }}"></script>
<script src="{{ asset('js/assets/libs/prismjs/components/prism-scss.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/@yaireo/tagify/dist/tagify.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/tiny-slider/dist/min/tiny-slider.js') }}"></script>
<script src="{{ asset('js/assets/libs/@popperjs/core/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/tippy.js/dist/tippy-bundle.umd.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/typed.js/lib/typed.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jsvectormap/dist/js/jsvectormap.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/jsvectormap/dist/maps/world.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net-bs5/js/dataTables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/assets/libs/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js') }}"></script>


<!-- clipboard -->



<!-- Theme JS -->
{{--<script src="{{ asset('js/assets/js/theme.min.js') }}"></script>--}}


<svg id="SvgjsSvg1001" width="2" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1"
     xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.dev"
     style="overflow: hidden; top: -100%; left: -100%; position: absolute; opacity: 0;">
    <defs id="SvgjsDefs1002"></defs>
    <polyline id="SvgjsPolyline1003" points="0,0"></polyline>
    <path id="SvgjsPath1004" d="M0 0 "></path>
</svg>


<script>
    $(document).ready(function () {
        $('#auth').click(function () {
            $('.b-popup ,.b-popup-content').show()
        })

        $('#exit-popap').click(function () {
            $('.b-popup ,.b-popup-content').hide()
        })
    })
</script>
</body>
</html>
