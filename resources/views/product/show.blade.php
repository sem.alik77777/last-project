@extends('layouts.main')
@section('content')
    <!-- Libs CSS -->
    <!-- Video section -->
    <div class="p-lg-5 py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 mb-5">
                    <div class="rounded-3 position-relative w-100 d-block overflow-hidden p-0" style="height: 600px;">
                        <img src="{{ asset($product->image) }}" width="100%" alt="">
                        {{--                    <iframe class="position-absolute top-0 end-0 start-0 end-0 bottom-0 h-100 w-100" src="https://www.youtube.com/embed/PkZNo7MFNFg"></iframe>--}}
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="row">
                <div class="col-xl-8 col-lg-12 col-md-12 col-12 mb-4 mb-xl-0">
                    <!-- Card -->
                    <div class="card mb-5">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <h1 class="fw-semi-bold mb-2">
                                    {{ $product->h_one }}
                                </h1>
                                <a href="#" data-bs-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="Add to bi-bookmarkarks" data-bs-original-title="">
                                    @if(isset($bookmark))
                                        @if(empty($bookmark))
                                            <i data-id="{{ $product->id }}" class="bi-bookmark fs-3"></i>
                                        @else
                                            <i data-id="{{ $product->id }}" class="bi-bookmark-check-fill fs-3"></i>
                                        @endif
                                    @endif
                                </a>
                            </div>
                            <div class="d-flex mb-5">
                <span>
{{--                  <i class="bi-star-fill me-n1 text-warning"></i>--}}
                    {{--                  <i class="bi-star-fill me-n1 text-warning"></i>--}}
                    {{--                  <i class="bi-star-fill me-n1 text-warning"></i>--}}
                    {{--                  <i class="bi-star-fill me-n1 text-warning"></i>--}}
                    {{--                  <i class="bi-star-fill-half-full text-warning"></i>--}}
                    <i class="bi-eye"></i>
                  <span data-id="{{ $product->id }}" id="views" class="fw-medium">{{ $product->views }}</span>
                </span>

                                <span class="ms-4 d-none d-md-block">

                                    {{--                  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
                                    {{--                    <rect x="3" y="8" width="2" height="6" rx="1" fill="#754FFE"></rect>--}}
                                    {{--                    <rect x="7" y="5" width="2" height="9" rx="1" fill="#754FFE"></rect>--}}
                                    {{--                    <rect x="11" y="2" width="2" height="12" rx="1" fill="#DBD8E9"></rect>--}}
                                    {{--                  </svg>--}}
                                    {{--                  <span>--}}
                                    {{--                    Intermediate--}}
                                    {{--                  </span>--}}
                </span>
                                <span class="ms-4 d-none d-md-block">
                  <i class="mdi mdi-account-multiple-outline"></i>
                  <span>Цена за курс {{ $product->price }} &#8381;</span>
                </span>
                            </div>
                            @if(!empty($product->user->avatar))
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex align-items-center">
                                        @if(!empty($product->user->avatar) )
                                        <img src="{{ $product->user->avatar }}" class="rounded-circle avatar-md" alt="">
                                            @else
                                            <img src="{{ asset('images/logo/user.jpg') }}" class="rounded-circle avatar-md" alt="">
                                        @endif
                                        <div class="ms-2 lh-1">
                                            <h4 class="mb-1">{{ $product->user->name }}</h4>
                                            {{--                                        <p class="fs-6 mb-0">@kathrynjones</p>--}}
                                        </div>
                                    </div>
                                    <div>
                                        @if(\Illuminate\Support\Facades\Auth::check())
                                            @if($product->subscription)
                                                <button data-id="{{ $product->user->id }}" type="button"
                                                        id="subscription"
                                                        class="btn btn-outline-white btn-sm">Подписаться
                                                </button>
                                            @else
                                                <button data-id="{{ $product->user->id }}" type="button"
                                                        id="subscription"
                                                        class="btn btn-outline-danger btn-sm">Отписаться
                                                </button>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-lt-tab" id="tab" role="tablist">
                            <!-- Nav item -->
                            <li class="nav-item">
                                <a class="nav-link active" id="description-tab" data-bs-toggle="pill"
                                   href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
                            </li>
                            <!-- Nav item -->
                        {{--                            <li class="nav-item">--}}
                        {{--                                <a class="nav-link" id="review-tab" data-bs-toggle="pill" href="#review" role="tab"--}}
                        {{--                                   aria-controls="review" aria-selected="false">Reviews</a>--}}
                        {{--                            </li>--}}
                        <!-- Nav item -->
                        {{--                            <li class="nav-item">--}}
                        {{--                                <a class="nav-link" id="transcript-tab" data-bs-toggle="pill" href="#transcript"--}}
                        {{--                                   role="tab" aria-controls="transcript" aria-selected="false">Transcript</a>--}}
                        {{--                            </li>--}}
                        <!-- Nav item -->
                            {{--                            <li class="nav-item">--}}
                            {{--                                <a class="nav-link" id="faq-tab" data-bs-toggle="pill" href="#faq" role="tab"--}}
                            {{--                                   aria-controls="faq" aria-selected="false">FAQ</a>--}}
                            {{--                            </li>--}}
                        </ul>
                    </div>
                    <!-- Card -->
                    <div class="card rounded-3">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="tab-content" id="tabContent">
                                <!-- Tab pane -->
                                <div class="tab-pane fade active show" id="description" role="tabpanel"
                                     aria-labelledby="description-tab">
                                    <div class="mb-4 d-flex">

                                        <div class="col-md-9">
                                            <h3 class="mb-2">О курсе</h3>
                                            <p>
                                                {{ $product->content }}
                                            </p>

                                        </div>
                                        <div class="col-md-3">
                                            <a href="{{ asset('images/screen.png') }}">
                                                <img src="{{ asset('images/screen.png') }}"
                                                     style="height: 300px; width: 150px" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <h4 class="mb-3">Описание</h4>
                                    <div class="row mb-3">
                                        {{ $product->content_two }}
                                    </div>
                                    {{--                                        <div class="col-12 col-md-6">--}}
                                    {{--                                            <!-- List group -->--}}
                                    {{--                                            <ul class="list-unstyled">--}}
                                    {{--                                                <li class="d-flex align-item-center mb-2">--}}
                                    {{--                                                    <i class="far fa-check-circle text-success me-2 lh-lg"></i>--}}
                                    {{--                                                    <span>--}}
                                    {{--                            Recognize the importance of understanding your objectives when addressing an audience.--}}
                                    {{--                          </span>--}}
                                    {{--                                                </li>--}}
                                    {{--                                                <li class="d-flex align-item-center mb-2">--}}
                                    {{--                                                    <i class="far fa-check-circle text-success me-2 lh-lg"></i>--}}
                                    {{--                                                    <span>--}}
                                    {{--                            Identify the fundaments of composing a successful close.--}}
                                    {{--                          </span>--}}
                                    {{--                                                </li>--}}
                                    {{--                                                <li class="d-flex align-item-center mb-2">--}}
                                    {{--                                                    <i class="far fa-check-circle text-success me-2 lh-lg"></i>--}}
                                    {{--                                                    <span>--}}
                                    {{--                            Explore how to connect with your audience through crafting compelling stories.--}}
                                    {{--                          </span>--}}
                                    {{--                                                </li>--}}
                                    {{--                                            </ul>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-12 col-md-6">--}}
                                    {{--                                            <!-- List group -->--}}
                                    {{--                                            <ul class="list-unstyled">--}}
                                    {{--                                                <li class="d-flex align-item-center mb-2">--}}
                                    {{--                                                    <i class="far fa-check-circle text-success me-2 lh-lg"></i>--}}
                                    {{--                                                    <span>--}}
                                    {{--                            Examine ways to connect with your audience by personalizing your content.--}}
                                    {{--                          </span>--}}
                                    {{--                                                </li>--}}
                                    {{--                                                <li class="d-flex align-item-center mb-2">--}}
                                    {{--                                                    <i class="far fa-check-circle text-success me-2 lh-lg"></i>--}}
                                    {{--                                                    <span>--}}
                                    {{--                            Break down the best ways to exude executive presence.--}}
                                    {{--                          </span>--}}
                                    {{--                                                </li>--}}
                                    {{--                                                <li class="d-flex align-item-center mb-2">--}}
                                    {{--                                                    <i class="far fa-check-circle text-success me-2 lh-lg"></i>--}}
                                    {{--                                                    <span>--}}
                                    {{--                            Explore how to communicate the unknown in an impromptu communication.--}}
                                    {{--                          </span>--}}
                                    {{--                                                </li>--}}
                                    {{--                                            </ul>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <p>Maecenas viverra condimentum nulla molestie condimentum. Nunc ex libero, feugiat--}}
                                    {{--                                        quis lectus vel,--}}
                                    {{--                                        ornare euismod ligula. Aenean sit amet arcu nulla.</p>--}}
                                    {{--                                    <p>--}}
                                    {{--                                        Duis facilisis ex a urna blandit ultricies. Nullam sagittis ligula non eros--}}
                                    {{--                                        semper, nec mattis odio--}}
                                    {{--                                        ullamcorper. Phasellus feugiat sit amet leo eget consectetur.--}}
                                    {{--                                    </p>--}}

                                    {{--                                    <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">--}}
                                    <div class="mb-3">
                                        <!-- Content -->
                                        <h3 class="mb-4">How students rated this courses</h3>
                                        <div class="row align-items-center">
                                            <div class="col-auto text-center">
                                                <h3 class="display-2 fw-bold">{{ $rating['avg'] }}</h3>
                                                <i class="bi-star-fill me-n1 text-warning"></i>
                                                <i class="bi-star-fill me-n1 text-warning"></i>
                                                <i class="bi-star-fill me-n1 text-warning"></i>
                                                <i class="bi-star-fill me-n1 text-warning"></i>
                                                <i class="bi-star-fill me-n1-half text-warning"></i>
                                                <p class="mb-0 fs-6">(Based on @if(empty($rating['count']))
                                                        0 @else {{ $rating['count'] }} @endif reviews)</p>
                                            </div>
                                            <!-- Progress bar -->
                                            <div class="col pt-3 order-3 order-md-2">
                                                <div class="progress mb-3" style="height: 6px;">
                                                    <div class="progress-bar bg-warning" role="progressbar"
                                                         style="width: {{ $grade[1] . '%' }};" aria-valuenow="90"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"></div>
                                                </div>
                                                <div class="progress mb-3" style="height: 6px;">
                                                    <div class="progress-bar bg-warning" role="progressbar"
                                                         style="width: {{ $grade[2] . '%' }};" aria-valuenow="90"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"></div>
                                                </div>
                                                <div class="progress mb-3" style="height: 6px;">
                                                    <div class="progress-bar bg-warning" role="progressbar"
                                                         style="width: {{ $grade[3] . '%' }};" aria-valuenow="90"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"></div>
                                                </div>
                                                <div class="progress mb-3" style="height: 6px;">
                                                    <div class="progress-bar bg-warning" role="progressbar"
                                                         style="width: {{ $grade[4] . '%' }};" aria-valuenow="90"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"></div>
                                                </div>
                                                <div class="progress mb-3" style="height: 6px;">
                                                    <div class="progress-bar bg-warning" role="progressbar"
                                                         style="width: {{ $grade[5] . '%' }};" aria-valuenow="90"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"></div>
                                                </div>
                                            </div>

                                            <!-- Rating -->
                                            <div class="col-md-auto col-6 order-2 order-md-3">
                                                <div>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <span class="ms-1">{{$grade[1]}}%</span>
                                                </div>
                                                <div>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <span class="ms-1">{{$grade[2]}}%</span>
                                                </div>
                                                <div>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <span class="ms-1">{{$grade[3]}}%</span>
                                                </div>
                                                <div>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <span class="ms-1">{{$grade[4]}}%</span>
                                                </div>
                                                <div>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                                    <span class="ms-1">{{$grade[5]}}%</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <hr class="my-5">
                                    <div class="mb-3">
                                        <!-- Review -->
                                        <div class="d-lg-flex align-items-center justify-content-between mb-5">
                                            <div class="mb-3 mb-lg-0">
                                                <h3 class="mb-0">Reviews</h3>
                                            </div>
                                            <div>
                                                <!-- Search -->
                                                {{--                                                <form class="form-inline">--}}
                                                {{--                                                    <div class="d-flex align-items-center me-2">--}}
                                                {{--                            <span class="position-absolute ps-3">--}}
                                                {{--                              <i class="fe fe-search"></i>--}}
                                                {{--                            </span>--}}
                                                {{--                                                        <input type="search" class="form-control ps-6"--}}
                                                {{--                                                               placeholder="Search Review">--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </form>--}}
                                            </div>
                                        </div>


                                        @if(!empty($reviews))
                                            @foreach($reviews as $review)
                                                <div class="d-flex mt-4">
                                                    <!-- Media -->
                                                    @if(isset($review->user->avatar))
                                                        <img src="{{ $review->user->avatar }}" alt=""
                                                             class="rounded-circle avatar-lg">
                                                    @else
                                                        <img src="{{ asset('images/logo/user.jpg')}}" alt=""
                                                             class="rounded-circle avatar-lg">
                                                @endif
                                                <!-- Content -->
                                                    <div class=" ms-3">
                                                        <h4 class="mb-1">
                                                            @if(isset($review->user->name)) {{ $review->user->name }} @else
                                                                Не авторизованный пользователь @endif
                                                            <span
                                                                class="ms-1 fs-6 text-muted">{{ $review->created_at }}</span>
                                                        </h4>
                                                        <p>
                                                            {{ $review->review }}
                                                        </p>
                                                        @if($review->ip == request()->ip() || $review->user_id == auth()->id())
                                                            <div class="d-lg-flex">
                                                                {{--                                                            <p class="mb-0">Was this review helpful?</p>--}}
                                                                <a href="{{ route('review.delete',$review->id ) }}" class="btn btn-xs btn-danger ms-lg-3 delete-review">Удалить</a>
                                                                {{--                                                            <a href="#" class="btn btn-xs btn-outline-white ms-1">No</a>--}}
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <form id="review-form">
                                        @if(empty($rating['avg']) && auth()->check())
                                            <label for="rating">Оценить курс</label>
                                            <select name="rating" class="form-control mb-2 w-25" id="rating">
                                                <option>Не выбран</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        @endif
                                        <label for="review">Отзыв</label>
                                        <textarea name="review" id="review" class="form-control"></textarea>
                                        {{--                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <button id="review-save" type="button" class="btn btn-primary mt-2">Оставить
                                            отзыв
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-xl-4 col-lg-12 col-md-12 col-12">
                    @foreach($similarCourses as $course)
                        <div class="card mt-1" id="courseAccordion">
                            {{--                    <div class="card" style="width: 18rem;">--}}
                            <img src="{{ asset($course->image) }}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{ $course->title }}</h5>
                                <p class="card-text">{{ $course->content }}</p>
                                <a href="/product/show/{{ $course->id }}" class="btn btn-primary">Подробнее</a>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        document.getElementById('subscription').addEventListener("click", function () {
            var id = {
                id: this.getAttribute('data-id')
            }
            fetch('/ajax/subscription/store', {
                method: 'post',
                body: JSON.stringify(id),
                headers: {
                    "Content-Type": "application/json",
                    "X-CSRF-TOKEN": token
                }
            }).then(function (response) {
                response.text().then(value =>
                    Swal.fire(
                        'Good job!',
                        value,
                        'success'
                    ))
                location.reload()
            })

        })
    </script>
    <script src="{{ asset('js/review-rating.js') }}"></script>
    <script src="{{ asset('js/bookmark.js') }}"></script>
    <script src="{{ asset('js/view.js') }}"></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.12/clipboard.min.js"></script>--}}



@endsection
