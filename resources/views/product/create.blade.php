@extends('layouts.main')
<link rel="stylesheet" href="{{ asset('css/form-style.css') }}">

@section('content')
    <!-- Thanks to Pieter B. for helping out with the logistics -->
    {{--    <link rel="stylesheet" href="{{ asset('css/crop.css') }}">--}}

    <style>
        #btn-added-tags, #btn-delete-tags, #btn-added-teachers, #btn-delete-teachers {
            cursor: pointer;
        }

        img {
            max-width: 100%; /* This rule is very important, please do not ignore this! */
        }

        #result {
            width: 200px;
            height: 200px;
        }

        .finish-content {
            border: 2px white solid;
            background: #d7d2cd;
            font-family: Source Sans Pro, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
        }

        .cropper-container {
            max-width: 100%;
            max-height: 400px;
        }
    </style>
    <div class="container">
        <div class="row">

            <form id="contact" action="#" style="height: 820px">
                <div>
                    <h3>Шаг 1</h3>
                    <section>

                        <label for="">Название курса*</label>
                        <input class="required"
                               type="text"
                               placeholder="до 255 символов" name="title"
                               data-purpose="edit-course-title"
                               id="main[title]" value="">

                        <label>Заголовок*</label>
                        <input class="prompt srch_explore required"
                               type="text"
                               placeholder="Course h one here" name="h_one"
                               data-purpose="edit-course-title"
                               maxlength="60"
                               id="h_one" value="">

                        <label>Описание Курса*</label><textarea rows="3" name="description_one" id="" class="required"
                                                                placeholder="Item description here..."></textarea>

                        <label for="author">Автор/Школа*</label><input class="form-control required" type="text"
                                                                       placeholder="Course author or school name here"
                                                                       name="author_school" id="author" value="">


                        <label>URL*</label>
                        <input type="url" class="form-control required" value="{{ old('url') }}" name="url">

                        <label for="duration_of_the_course_int mt-5">Продолжительность курса*</label>
                        <div class="d-flex">
                            <input id="duration_of_the_course_int" type="number" class="form-control w-25 required"
                                   name="duration_of_the_course_int">
                            <select name="duration_of_the_course_type" id="" class="form-control w-25">
                                <option value="часы">Часы</option>
                                <option value="дни">Дни</option>
                                <option value="месяцы">Месяцы</option>
                                <option value="урок(ов)">Уроки</option>
                            </select>
                        </div>
                        <div class="d-flex mt-2">
                        <div class="mb-2 w-30">
                            <label for="employment_assistance">Помощь в трудоустройстве</label><br>
                            <input type="checkbox" class="form-check" name="employment_assistance">
                        </div>

                        <div class="mb-2 w-30">
                            <label for="internship">Стажировка</label><br>
                            <input type="checkbox" class="form-check" name="internship">
                        </div>
                        </div>
                    </section>
                    <h3>Шаг 2</h3>
                    <section>
                        <div class="col-lg-12 col-md-12">
                            <div class="ui search focus mt-30 lbel25">
                                <label>Тег*</label>
                                <div id="tags" class=" d-flex flex-wrap box-tags">
                                    <input name="tags[]" type="text"
                                           class="form-control tags required">
                                </div>
                                <div class="button-edit">
                                    <img id="btn-added-tags"
                                         src="https://img.icons8.com/color/48/000000/add--v1.png"
                                         width="35" height="35"/>
                                    <img id="btn-delete-tags"
                                         src="https://img.icons8.com/flat-round/64/000000/delete-sign.png"
                                         width="30" height="30"/>
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-12 col-md-12">
                            <div class="ui search focus mt-30 lbel25">
                                <label>Учителя*</label>
                                <div id="teachers"
                                     class=" d-flex flex-wrap box-teachers">
                                    <input name="teachers[]" type="text"
                                           class="form-control teachers required"
                                    >
                                </div>
                                <div class="button-edit">
                                    <img id="btn-added-teachers"
                                         src="https://img.icons8.com/color/48/000000/add--v1.png"
                                         width="35" height="35"/>
                                    <img id="btn-delete-teachers"
                                         src="https://img.icons8.com/flat-round/64/000000/delete-sign.png"
                                         width="30" height="30"/>
                                </div>
                            </div>
                        </div>
                        <label class="label25">Зарплата "от" после прохождения курса*</label>
                        <input value="{{ old('salary_due') }}" name="salary_due" type="number"
                               class="form-control required"
                               id="salary_due">
                        <label class="label">Зарплата "до" после прохождения курса*</label>
                        <input value="{{ old('salary_from') }}" name="salary_from" type="number"
                               class="form-control required"
                               id="salary_from">
                        <label class="label25">Цена за курс*</label><input class="prompt srch_explore " type="text"
                                                                           placeholder="0" name="price" id="" value="">
                        <label for="time" class="form-label">Длительност/В часах</label> <input
                            value="{{ old('duration_of_the_course') }}" class="form-control"
                            name="duration_of_the_course" type="number">
                    </section>
                    <h3>Шаг 3</h3>
                    <section>

                        <label>Категория*</label>
                        <select name="category_id" class="form-control">
                            @foreach($categories as $category)
                                <option
                                    value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>

                        <label>Под Категория*</label>
                        <select name="sub_category_id" class="form-control">
                            @foreach($sub_categories as $category)
                                <option
                                    value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>


                        <div class="col-lg-6 col-md-12">
                            <div class="mt-30 lbel25">
                                <label>Онлайн/Оффлайн*</label>
                                <div
                                    tabindex="0"><select name="online_offline"
                                                         class="form-control">
                                        <option value="">Выберите</option>
                                        <option value="1">Онлайн</option>
                                        <option value="2">Оффлайн</option>
                                        <option value="3">Онлайн-Оффлайн</option>
                                    </select><i class="dropdown icon"></i>
                                </div>
                            </div>
                        </div>


                        <div class="d-flex mt-2">
                            <!-- Below are a series of inputs which allow file selection and interaction with the cropper api -->
                            <input class="required" type="file" name="image-upload" id="fileInput" accept="image/*"/>
                            {{--                            <input type="button" id="btnCrop" class="btn btn-primary" value="Обрезать"/>--}}
                            {{--                            <input type="button" id="btnRestore" class="btn btn-warning" value="Сбросить"/>--}}
                        </div>
                        <div id="canvas-size mt-2">
                            <canvas id="canvas" style="max-width: 400px; max-height: 400px">
                                Your browser does not support the HTML5 canvas element.
                            </canvas>
                        </div>

                        <div id="result">
                        </div>
                    </section>
                    <h3>Шаг 4</h3>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <section>
                        <div class="finish-content card-accent-dark">
                            <p>Проверте правильность написания данных иначе после проверки администротором будет
                                запрещено в публикации</p>
                            <p>1.Удостовертесь в правильном написании url.</p>
                            <p>2.Удостовертесь в правильном категории.</p>
                            <p>3.Удостовертесь в правильном под категории.</p>
                            <p>4.Отслеживайте уведомления в катором будет результат принятия решения администратора по
                                публикации вашего курса.</p>
                        </div>
                        @if(config('services.recaptcha.key'))
                            <div style="margin: 0 auto " class="g-recaptcha"
                                 data-sitekey="{{config('services.recaptcha.key')}}">
                            </div>
                        @endif
                    </section>
                </div>
            </form>
        </div>
    </div>


    <div class="errors"></div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.3/cropper.js"></script>--}}
    <script>
        var form = $("#contact");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                var form = document.getElementById('contact')
                var formData = new FormData(form)
                $.ajax({
                    url: "ajax/store",
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {

                        console.log(response)
                        if (response === 'success') {
                            Swal.fire(
                                'Good job!',
                                'Курс добавлен и будет опубликован после проверки модератора!',
                                'success'
                            )
                        } else {
                            var html = ''
                            $.each(response['errors'], function (index, value) {
                                html = html + value
                            })
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: html,
                                footer: '<a href="">Why do I have this issue?</a>'
                            })
                            $('.errors').empty()
                            $.each(response['errors'], function (index, value) {
                                $('.errors').append('<p style="color: red; text-align: center">' + value + '</p>')
                            })

                        }
                    }
                })
            }
        });
    </script>

    <script>
        var canvas = $("#canvas"),
            context = canvas.get(0).getContext("2d"),
            $result = $('#result');

        $('#fileInput').on('change', function () {
            if (this.files && this.files[0]) {
                if (this.files[0].type.match(/^image\//)) {
                    var reader = new FileReader();
                    reader.onload = function (evt) {
                        var img = new Image();
                        img.onload = function () {
                            context.canvas.height = img.height;
                            context.canvas.width = img.width;
                            context.drawImage(img, 0, 0);
                            var cropper = canvas.cropper({
                                aspectRatio: 16 / 9
                            });
                            $('#btnCrop').click(function () {
                                // Get a string base 64 data url
                                $result.empty()
                                var croppedImageDataURL = canvas.cropper('getCroppedCanvas').toDataURL("image/png");
                                $('#file').val(croppedImageDataURL)
                                $result.append($('<img>').attr('src', croppedImageDataURL));
                            });
                            $('#btnRestore').click(function () {
                                canvas.cropper('reset');
                                $result.empty();
                            });
                        };
                        img.src = evt.target.result;
                    };
                    reader.readAsDataURL(this.files[0]);
                } else {
                    alert("Invalid file type! Please select an image file.");
                }
            } else {
                alert('No file(s) selected.');
            }
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#btn-added-tags').click(function () {
                $('.box-tags').append('<input name="tags[]" type="text" class="form-control mt-1 tags" id="tags">')
            });
            $('#btn-delete-tags').click(function () {
                var int = $('.box-tags').find('.tags').length + 1;
                var int2 = $('.box-tags').find('.tags').length - 1;
                if (int > 1) {
                    $('.tags').each(function (index, value) {

                        if (index === int2) {
                            $(value).detach()
                        }
                    })
                }

            });
            $('#btn-added-teachers').click(function () {
                $('.box-teachers').append('<input name="teachers[]" type="text" class="form-control mt-1 teachers" id="teachers">')
            });
            $('#btn-delete-teachers').click(function () {
                var int = $('.box-teachers').find('.teachers').length + 1;
                var int2 = $('.box-teachers').find('.teachers').length - 1;
                if (int > 1) {
                    $('.teachers').each(function (index, value) {

                        if (index === int2) {
                            $(value).detach()
                        }
                    })
                }

            });
        })
    </script>
    {{--    <script src="{{ asset('js/validate-create.js') }}"></script>--}}
@endsection











