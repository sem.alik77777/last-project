<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
    <title>Document</title>
</head>
<body>
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    .modal-lg{
        max-width: 1000px !important;
    }
</style>

<header class="site-header sticky-top py-1">
    <nav class="container d-flex flex-column flex-md-row justify-content-between">
        <a class="py-2" href="#" aria-label="Product">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor"
                 stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="d-block mx-auto" role="img"
                 viewBox="0 0 24 24"><title>Product</title>
                <circle cx="12" cy="12" r="10"></circle>
                <path
                    d="M14.31 8l5.74 9.94M9.69 8h11.48M7.38 12l5.74-9.94M9.69 16L3.95 6.06M14.31 16H2.83m13.79-4l-5.74 9.94"></path>
            </svg>
        </a>
        @foreach($categories as $category)
            <a class="py-2 d-none d-md-inline-block" href="#">{{ $category->title }}</a>
        @endforeach
    </nav>
</header>


<div class="container d-flex justify-content-center bg-light flex-wrap">
    <div class="w-100 text-center">
        <h1>Добавить Курс</h1></div>

    <form class="w-50" method="post" action="{{ route('product.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="categories" class="form-label">Категория</label>
            <select name="category_id" class="form-control" id="categories">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{$category->title}}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="sub_categories" class="form-label">Под Категория</label>
            <select name="sub_category_id" class="form-control" id="sub_categories">
                @foreach($sub_categories as $category)
                    <option value="{{ $category->id }}">{{$category->title}}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="title" class="form-label">Название</label>
            <input value="{{ old('title') }}" name="title" type="text" class="form-control" id="title">
        </div>

        <div class="mb-3">
            <label for="h_one" class="form-label">Заголовок</label>
            <input value="{{ old('h_one') }}" name="h_one" type="text" class="form-control" id="h_one">
        </div>

        <div class="form-floating">
            <textarea name="content" class="form-control" placeholder="Leave a comment here"
                      id="description"></textarea>
            <label for="description">Описание 1</label>
        </div>
        <div class="form-floating">
            <textarea name="content_two" class="form-control" placeholder="Leave a comment here" id="text"></textarea>
            <label for="text">Описание 2</label>
        </div>

        <div class="mb-3">
            <label for="image-upload" class="form-label">Фото</label>
            <input type="file" name="image-upload" id="image-upload" class="image">
        </div>

        <input type="hidden" name="file" id="file">

        <div class="mb-3 w-75 d-flex flex-wrap ">
            <label for="tags" class="form-label w-100">Тег</label>
            <div class="w-100 d-flex flex-wrap box-tags">
                <input name="tags[]" type="text" class="form-control" id="tags">
            </div>
            <button type="button" class="btn btn-warning">+</button>
            <button type="button" class="btn btn-danger">-</button>

        </div>

        <div class="mb-3">
            <label for="author" class="form-label">Автор/Школа</label>
            <input value="{{ old('author_school') }}" name="author_school" type="text" class="form-control" id="author">
        </div>

        <div class="mb-3">
            <label for="teachers" class="form-label">Учителя</label>
            <input name="teachers[]" type="text" class="form-control" id="teachers">
        </div>

        <div class="mb-3">
            <label for="for_reading" class="form-label">На Чтение </label>
            <input value="{{ old('for_reading') }}" name="for_reading" type="number" class="form-control"
                   id="for_reading">
        </div>

        <div class="mb-3">
            <label for="time" class="form-label">Длительност/В часах</label>
            <input value="{{ old('duration_of_the_course') }}" name="duration_of_the_course" type="text" class="form-control" id="time">
        </div>

        <div class="mb-3">
            <label for="price" class="form-label">Цена за курс</label>
            <input value="{{ old('price') }}" name="price" type="number" class="form-control" id="price">
        </div>


        <div class="mb-3">
            <label for="credit" class="form-label">Рассрочка/Кредит</label>
            <select name="credit" class="form-control" id="credit">
                <option value="">Не имеет</option>
                <option value="1">Рассрочка</option>
                <option value="2">Кредит</option>
            </select>
        </div>

        <div class="start-end-curse d-flex">
            <div class="mb-3 w-50">
                <label for="start_curse" class="form-label">Начало курса</label>
                <input value="{{ old('start_curse') }}" name="start_curse" type="date" class="form-control" id="start_curse">
            </div>

            <div class="mb-3 w-50">
                <label for="end_curse" class="form-label">Конец курса</label>
                <input value="{{ old('end_curse') }}" name="end_curse" type="date" class="form-control" id="end_curse">
            </div>
        </div>

        <div class="due-from-salary d-flex">
            <div class="mb-3 w-50">
                <label for="salary_due" class="form-label">Зарплата "от" после прохождения курса</label>
                <input value="{{ old('salary_due') }}" name="salary_due" type="number" class="form-control" id="salary_due">
            </div>

            <div class="mb-3 w-50">
                <label for="salary_from" class="form-label">Зарплата "до" после прохождения курса</label>
                <input value="{{ old('salary_from') }}" name="salary_from" type="number" class="form-control" id="salary_from">
            </div>
        </div>

        <div class="mb-3">
            <label for="online_offline" class="form-label">Онлайн/Оффлайн курс</label>
            <select name="online_offline" class="form-control" id="online_offline">
                <option value="1">Онлайн</option>
                <option value="0">Оффлайн</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="url" class="form-label">Ссылка на курс</label>
            <input value="{{ old('url') }}" name="url" type="text" class="form-control" id="url">
        </div>

        @if(config('services.recaptcha.key'))
            <div class="g-recaptcha"
                 data-sitekey="{{config('services.recaptcha.key')}}">
            </div>
        @endif

        <script
            src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous"></script>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    @if(session('success'))
        <div class="alert alert-success h-25" style="margin-left: 10px">
            <ul>
                <li>
                    {{session('success')}}
                </li>
            </ul>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger h-50" style="margin-left: 10px">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Laravel Cropper Js - Crop Image Before Upload - Tutsmake.com</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.btn-warning').click(function () {
            $('.box-tags').append('<input name="tags[]" type="text" class="form-control tags" id="tags">')
        });
        $('.btn-danger').click(function () {
            var int = $('.box-tags').find('.tags').length + 1;
            var int2 = $('.box-tags').find('.tags').length - 1;

            if (int > 1) {

                $('.tags').each(function (index, value) {
                    console.log(index, int2)
                    if (index === int2) {
                        $(value).detach()
                    }
                })
            }

        });
    })
</script>

</body>
</html>

<script>
    var $modal = $('#modal');
    var image = document.getElementById('image');
    var cropper;
    $("body").on("change", ".image", function(e){
        var files = e.target.files;
        var done = function (url) {
            image.src = url;
            $modal.modal('show');
        };
        var reader;
        var file;
        var url;
        if (files && files.length > 0) {
            file = files[0];
            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });
    $modal.on('shown.bs.modal', function () {
        cropper = new Cropper(image, {
            aspectRatio: 1,
            viewMode: 3,
            preview: '.preview'
        });
    }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
    });
    $("#crop").click(function(){
        canvas = cropper.getCroppedCanvas({
            width: 160,
            height: 160,
        });

        canvas.toBlob(function(blob) {
            url = URL.createObjectURL(blob);
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                $modal.modal('hide');

              var image =  document.getElementById('file').value = base64data

            }
        });
    })
</script>



