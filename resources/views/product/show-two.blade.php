<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/css/star-rating.min.css" media="all"
          rel="stylesheet" type="text/css"/>

    <!-- with v4.1.0 Krajee SVG theme is used as default (and must be loaded as below) - include any of the other theme CSS files as mentioned below (and change the theme property of the plugin) -->
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.css"
          media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/show.css') }}">
    <title>Document</title>
</head>
<style>
    .answer {
        color: #0a53be;
        cursor: pointer;
        margin-left: 10px;
    }

    #delete {
        color: red;
        cursor: pointer;
        margin-left: 10px;
    }
    #similar-courses-link {
        color: cornflowerblue;
    }
</style>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                IT-ID
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent"
                 style="display: flex; justify-content: end">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        {{--                        @if (Route::has('login'))--}}
                        {{--                            <li class="nav-item">--}}
                        {{--                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                        {{--                            </li>--}}
                        {{--                        @endif--}}

                        {{--                        @if (Route::has('register'))--}}
                        {{--                            <li class="nav-item">--}}
                        {{--                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                        {{--                            </li>--}}
                        {{--                        @endif--}}
                        <li class="nav-item">
                            <button type="button" class="btn btn-outline-primary btn-auth">Авторизоваться</button>
                        </li>

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <a href="{{ route('personal.area') }}" class="dropdown-item">Личный кабинет</a>
                                <form id="logout-form" action="{{ route('auth.logout') }}" method="post" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <?php $avatar = \Illuminate\Support\Facades\Auth::user()->avatar ?>
                        <img src="{{ asset($avatar) }}" alt="" width="30" height="30">
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

</div>
<div class="container">
    <div class="row">

        <div class="col-md-8"><h1>{{ $product->h_one }}</h1></div>

        <div class="col-md-8 mt-2">
            <div class="card">
                <div class="card-body">
                    <div class="card-img-actions">
                        <img src="{{asset($product->image)}}" class="card-img img-fluid" width="350" height="350"
                             alt="">
                        <div class="curs-description">
                            <strong>Автор:</strong>
                        </div>

                        <form action="" id="form">
                            <input id="id" name="product_id" type="hidden" value="{{$product->id}}">
                        </form>
                        <div class="card-body bg-light text-center">
                            <div class="mb-2">
                                <h6 class="font-weight-semibold mb-2">
                                    <a href="#" class="text-default mb-2" data-abc="true"></a></h6>
                                <a href="#" class="text-muted" data-abc="true">Laptops & Notebooks</a></div>
                            <h3 class="mb-0 font-weight-semibold"></h3>
                            <div>
                                <i class="fa fa-star star"></i>
                                <i class="fa fa-star star"></i>
                                <i class="fa fa-star star"></i>
                                <i class="fa fa-star star"></i>
                            </div>
                            <div class="text-muted mb-3">@if(is_null($product->views))
                                    0 @else{{ $product->views }} @endif reviews <br>
                                <span> Рейтинг @if(empty($rating)) 0 @else {{$rating}} @endif</span>
                            </div>

                            <button type="button" class="btn bg-cart">
                                <i class="fa fa-cart-plus mr-2"></i> Add to cart
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <ul>
                @foreach($similarCourses as $course)
                    <li><a id="similar-courses-link" href="{{ route('product.show', $course->id) }}">{{ $course->title }}</a></li>
                    <hr>
                @endforeach
            </ul>
        </div>
    </div>

    <section class="section-medium section-arrow--bottom-center section-arrow-primary-color bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-white text-center">
                    <h2 class="section-title ">Если у вас остались вопросы пишите и делитесь советами</h2>
                    <p class="section-sub-title">
                        Оставляйте отзыва и делитесь впечатлениями о курсе.
                        <br>Помогайте новым участникам и получайте ценные призы!
                    </p>
                </div>
            </div>
        </div>
    </section>


    <div class="container">
        <div id="reviews" class="review-section">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h4 class="m-0">37 Reviews</h4>
                <select class="custom-select custom-select-sm border-0 shadow-sm ml-2 select2-hidden-accessible"
                        data-select2-id="1" tabindex="-1" aria-hidden="true">
                    <option data-select2-id="3">Most Relevant</option>
                    <option>Most Recent</option>
                </select>
                <span class="select2 select2-container select2-container--default" dir="ltr" data-select2-id="2"
                      style="width: 188px;">
                <span class="selection">
                    <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true"
                          aria-expanded="false" tabindex="0" aria-labelledby="select2-qd66-container">
                        <span class="select2-selection__rendered" id="select2-qd66-container" role="textbox"
                              aria-readonly="true" title="Most Relevant">Most Relevant</span>
                        <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>
                    </span>
                </span>
                <span class="dropdown-wrapper" aria-hidden="true"></span>
            </span>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="stars-counters">
                        <tbody>
                        <tr class="">
                            <td>
                                <span>
                                    <button
                                        class="fit-button fit-button-color-blue fit-button-fill-ghost fit-button-size-medium stars-filter">5 Stars</button>
                                </span>
                            </td>
                            <td class="progress-bar-container">
                                <div class="fit-progressbar fit-progressbar-bar star-progress-bar">
                                    <div class="fit-progressbar-background">
                                        <span class="progress-fill" style="width: 97.2973%;"></span>
                                    </div>
                                </div>
                            </td>
                            <td class="star-num">(36)</td>
                        </tr>
                        <tr class="">
                            <td>
                                <span>
                                    <button
                                        class="fit-button fit-button-color-blue fit-button-fill-ghost fit-button-size-medium stars-filter">5 Stars</button>
                                </span>
                            </td>
                            <td class="progress-bar-container">
                                <div class="fit-progressbar fit-progressbar-bar star-progress-bar">
                                    <div class="fit-progressbar-background">
                                        <span class="progress-fill" style="width: 2.2973%;"></span>
                                    </div>
                                </div>
                            </td>
                            <td class="star-num">(2)</td>
                        </tr>
                        <tr class="">
                            <td>
                                <span>
                                    <button
                                        class="fit-button fit-button-color-blue fit-button-fill-ghost fit-button-size-medium stars-filter">5 Stars</button>
                                </span>
                            </td>
                            <td class="progress-bar-container">
                                <div class="fit-progressbar fit-progressbar-bar star-progress-bar">
                                    <div class="fit-progressbar-background">
                                        <span class="progress-fill" style="width: 0;"></span>
                                    </div>
                                </div>
                            </td>
                            <td class="star-num">(0)</td>
                        </tr>
                        <tr class="">
                            <td>
                                <span>
                                    <button
                                        class="fit-button fit-button-color-blue fit-button-fill-ghost fit-button-size-medium stars-filter">5 Stars</button>
                                </span>
                            </td>
                            <td class="progress-bar-container">
                                <div class="fit-progressbar fit-progressbar-bar star-progress-bar">
                                    <div class="fit-progressbar-background">
                                        <span class="progress-fill" style="width: 0;"></span>
                                    </div>
                                </div>
                            </td>
                            <td class="star-num">(0)</td>
                        </tr>
                        <tr class="">
                            <td>
                                <span>
                                    <button
                                        class="fit-button fit-button-color-blue fit-button-fill-ghost fit-button-size-medium stars-filter">5 Stars</button>
                                </span>
                            </td>
                            <td class="progress-bar-container">
                                <div class="fit-progressbar fit-progressbar-bar star-progress-bar">
                                    <div class="fit-progressbar-background">
                                        <span class="progress-fill" style="width: 0;"></span>
                                    </div>
                                </div>
                            </td>
                            <td class="star-num">(0)</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <div class="ranking">
                        <h6 class="text-display-7">Rating Breakdown</h6>
                        <ul>
                            <li>
                                Seller communication level<span>5<span
                                        class="review-star rate-10 show-one"></span></span>
                            </li>
                            <li>
                                Recommend to a friend<span>5<span class="review-star rate-10 show-one"></span></span>
                            </li>
                            <li>
                                Service as described<span>4.9<span class="review-star rate-10 show-one"></span></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            @if(!empty($reviews))

                @foreach($reviews as $value)
                    <div id="review-list" class="review-list">
                        <ul>
                            <li>
                                <div class="d-flex">

                                    <div class="left">
                        <span>
                            <img src="{{ $value->user->avatar }}"
                                 class="profile-pict-img img-fluid" alt=""/>
                        </span>
                                    </div>
                                    <div class="right">
                                        <h4>
                                            {{ $value->user->name }}

                                            <span class="gig-rating text-body-2">
{{--                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="15" height="15">--}}
                                                {{--                                    <path--}}
                                                {{--                                        fill="currentColor"--}}
                                                {{--                                        d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"--}}
                                                {{--                                    ></path>--}}
                                                {{--                                </svg> 5.0--}}

                            </span>
                                            @auth() <span class="answer"
                                                          onclick="answer({{ $value->id }})">ответить</span>@endauth
                                            @if($value->user_id == \Illuminate\Support\Facades\Auth::id())<a
                                                href="{{ route('review.delete',$value->id) }}"><span
                                                    id="delete">удалить</span></a>@endif
                                        </h4>
                                        {{--                            <div class="country d-flex align-items-center">--}}
                                        {{--                            <span>--}}
                                        {{--                                <img class="country-flag img-fluid"--}}
                                        {{--                                     src="https://bootdey.com/img/Content/avatar/avatar6.png"/>--}}
                                        {{--                            </span>--}}
                                        {{--                                <div class="country-name font-accent">India</div>--}}
                                        {{--                            </div>--}}
                                        <div class="review-description">
                                            <p>
                                                {{ $value->review }}
                                            </p>
                                        </div>
                                        <span
                                            class="publish py-3 d-inline-block w-100">Published {{ $value->created_at }}</span>
                                        {{--                                    <div class="helpful-thumbs">--}}
                                        {{--                                        <div class="helpful-thumb text-body-2">--}}
                                        {{--                                <span class="fit-icon thumbs-icon">--}}
                                        {{--                                    <svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">--}}
                                        {{--                                        <path--}}
                                        {{--                                            d="M13.5804 7.81165C13.8519 7.45962 14 7 14 6.43858C14 5.40843 13.123 4.45422 12.0114 4.45422H10.0932C10.3316 3.97931 10.6591 3.39024 10.6591 2.54516C10.6591 0.948063 10.022 0 8.39207 0C7.57189 0 7.26753 1.03682 7.11159 1.83827C7.01843 2.31708 6.93041 2.76938 6.65973 3.04005C6.01524 3.68457 5.03125 5.25 4.44013 5.56787C4.38028 5.59308 4.3038 5.61293 4.22051 5.62866C4.06265 5.39995 3.79889 5.25 3.5 5.25H0.875C0.391754 5.25 0 5.64175 0 6.125V13.125C0 13.6082 0.391754 14 0.875 14H3.5C3.98325 14 4.375 13.6082 4.375 13.125V12.886C5.26354 12.886 7.12816 14.0002 9.22728 13.9996C9.37781 13.9997 10.2568 14.0004 10.3487 13.9996C11.9697 14 12.8713 13.0183 12.8188 11.5443C13.2325 11.0596 13.4351 10.3593 13.3172 9.70944C13.6578 9.17552 13.7308 8.42237 13.5804 7.81165ZM0.875 13.125V6.125H3.5V13.125H0.875ZM12.4692 7.5565C12.9062 7.875 12.9062 9.1875 12.3159 9.48875C12.6856 10.1111 12.3529 10.9439 11.9053 11.1839C12.1321 12.6206 11.3869 13.1146 10.3409 13.1246C10.2504 13.1255 9.32247 13.1246 9.22731 13.1246C7.23316 13.1246 5.54296 12.011 4.37503 12.011V6.44287C5.40611 6.44287 6.35212 4.58516 7.27847 3.65879C8.11368 2.82357 7.83527 1.43153 8.3921 0.874727C9.78414 0.874727 9.78414 1.84589 9.78414 2.54518C9.78414 3.69879 8.94893 4.21561 8.94893 5.32924H12.0114C12.6329 5.32924 13.1223 5.88607 13.125 6.44287C13.1277 6.99967 12.9062 7.4375 12.4692 7.5565ZM2.84375 11.8125C2.84375 12.1749 2.54994 12.4688 2.1875 12.4688C1.82506 12.4688 1.53125 12.1749 1.53125 11.8125C1.53125 11.4501 1.82506 11.1562 2.1875 11.1562C2.54994 11.1562 2.84375 11.4501 2.84375 11.8125Z"--}}
                                        {{--                                        ></path>--}}
                                        {{--                                    </svg>--}}
                                        {{--                                </span>--}}
                                        {{--                                            <span class="thumb-title">Helpful</span>--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <div class="helpful-thumb text-body-2 ml-3">--}}
                                        {{--                                <span class="fit-icon thumbs-icon">--}}
                                        {{--                                    <svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">--}}
                                        {{--                                        <path--}}
                                        {{--                                            d="M0.419563 6.18835C0.148122 6.54038 6.11959e-07 7 5.62878e-07 7.56142C2.81294e-05 8.59157 0.876996 9.54578 1.98863 9.54578L3.90679 9.54578C3.66836 10.0207 3.34091 10.6098 3.34091 11.4548C3.34089 13.0519 3.97802 14 5.60793 14C6.42811 14 6.73247 12.9632 6.88841 12.1617C6.98157 11.6829 7.06959 11.2306 7.34027 10.9599C7.98476 10.3154 8.96875 8.75 9.55987 8.43213C9.61972 8.40692 9.6962 8.38707 9.77949 8.37134C9.93735 8.60005 10.2011 8.75 10.5 8.75L13.125 8.75C13.6082 8.75 14 8.35825 14 7.875L14 0.875C14 0.391754 13.6082 -3.42482e-08 13.125 -7.64949e-08L10.5 -3.0598e-07C10.0168 -3.48226e-07 9.625 0.391754 9.625 0.875L9.625 1.11398C8.73647 1.11398 6.87184 -0.000191358 4.77272 0.00038257C4.62219 0.000300541 3.74322 -0.000438633 3.65127 0.000382472C2.03027 -1.04643e-06 1.12867 0.981667 1.18117 2.45566C0.76754 2.94038 0.564868 3.64065 0.682829 4.29056C0.342234 4.82448 0.269227 5.57763 0.419563 6.18835ZM13.125 0.875L13.125 7.875L10.5 7.875L10.5 0.875L13.125 0.875ZM1.53079 6.4435C1.09375 6.125 1.09375 4.8125 1.6841 4.51125C1.31436 3.88891 1.64713 3.05613 2.09467 2.81605C1.86791 1.37941 2.61313 0.885417 3.65906 0.875355C3.74962 0.874535 4.67753 0.875355 4.77269 0.875355C6.76684 0.875355 8.45704 1.98898 9.62497 1.98898L9.62497 7.55713C8.5939 7.55713 7.64788 9.41484 6.72153 10.3412C5.88632 11.1764 6.16473 12.5685 5.6079 13.1253C4.21586 13.1253 4.21586 12.1541 4.21586 11.4548C4.21586 10.3012 5.05107 9.78439 5.05107 8.67076L1.9886 8.67076C1.36708 8.67076 0.877707 8.11393 0.874973 7.55713C0.872266 7.00033 1.09375 6.5625 1.53079 6.4435ZM11.1563 2.1875C11.1563 1.82506 11.4501 1.53125 11.8125 1.53125C12.1749 1.53125 12.4688 1.82506 12.4688 2.1875C12.4688 2.54994 12.1749 2.84375 11.8125 2.84375C11.4501 2.84375 11.1563 2.54994 11.1563 2.1875Z"--}}
                                        {{--                                        ></path>--}}
                                        {{--                                    </svg>--}}
                                        {{--                                </span>--}}
                                        {{--                                            <span class="thumb-title">Not Helpful</span>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </div>--}}

                                        @if(isset($value->reviewChild))
                                            @foreach($value->reviewChild as $review)
                                                <div class="response-item mt-4 d-flex">
                                                    <div class="left">
                                <span>
                                    <img src="{{ $review->user->avatar }}"
                                         class="profile-pict-img img-fluid" alt=""/>
                                </span>
                                                    </div>
                                                    <div class="right">
                                                        <h4>
                                                            {{ $review->user->name }}
                                                            <span class="gig-rating text-body-2">
{{--                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="15"--}}
                                                                {{--                                             height="15">--}}
                                                                {{--                                            <path--}}
                                                                {{--                                                fill="currentColor"--}}
                                                                {{--                                                d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"--}}
                                                                {{--                                            ></path>--}}
                                                                {{--                                        </svg> 5.0 --}}
                                                                @if($review->user_id == \Illuminate\Support\Facades\Auth::id())
                                                                    <a href="{{ route('review.child.delete',$review->id) }}"><span
                                                                            id="delete">удалить</span></a>@endif
                                    </span>
                                                        </h4>
                                                        {{--                                                    <div class="country d-flex align-items-center">--}}
                                                        {{--                                    <span>--}}
                                                        {{--                                        <img class="country-flag img-fluid"--}}
                                                        {{--                                             src="{{ $review->user->avatar }}"/>--}}
                                                        {{--                                    </span>--}}
                                                        {{--                                                        <div class="country-name font-accent">India</div>--}}
                                                        {{--                                                    </div>--}}
                                                        <div class="review-description">
                                                            <p>
                                                                {{ $review->text }}
                                                            </p>
                                                        </div>
                                                        <span
                                                            class="publish py-3 d-inline-block w-100">{{ $review->created_at }}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <form action="{{ route('review.child.store') }}" method="get"
                                                  id="form-answer-{{$value->id}}" class="w-25" style="display: none">
                                                <input type="hidden" name="review_id" value="{{ $value->id }}">
                                                <textarea name="text" class="form-control"></textarea>
                                                <button class="btn btn-outline-warning">Отправить</button>
                                            </form>
                                        @endif

                                        {{--                            <div class="response-item mt-4 d-flex">--}}
                                        {{--                                <div class="left">--}}
                                        {{--                                <span>--}}
                                        {{--                                    <img src="https://bootdey.com/img/Content/avatar/avatar2.png"--}}
                                        {{--                                         class="profile-pict-img img-fluid" alt=""/>--}}
                                        {{--                                </span>--}}
                                        {{--                                </div>--}}
                                        {{--                                <div class="right">--}}
                                        {{--                                    <h4>--}}
                                        {{--                                        Gurdeep Osahan--}}
                                        {{--                                        <span class="gig-rating text-body-2">--}}
                                        {{--                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" width="15"--}}
                                        {{--                                             height="15">--}}
                                        {{--                                            <path--}}
                                        {{--                                                fill="currentColor"--}}
                                        {{--                                                d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"--}}
                                        {{--                                            ></path>--}}
                                        {{--                                        </svg>--}}
                                        {{--                                        5.0--}}
                                        {{--                                    </span>--}}
                                        {{--                                    </h4>--}}
                                        {{--                                    <div class="country d-flex align-items-center">--}}
                                        {{--                                    <span>--}}
                                        {{--                                        <img class="country-flag img-fluid"--}}
                                        {{--                                             src="https://bootdey.com/img/Content/avatar/avatar3.png"/>--}}
                                        {{--                                    </span>--}}
                                        {{--                                        <div class="country-name font-accent">India</div>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <div class="review-description">--}}
                                        {{--                                        <p>--}}
                                        {{--                                            The process was smooth, after providing the required info, Pragyesh sent me--}}
                                        {{--                                            an outstanding packet of wireframes. Thank you a lot!--}}
                                        {{--                                        </p>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <span class="publish py-3 d-inline-block w-100">Published 4 weeks ago</span>--}}
                                        {{--                                </div>--}}
                                        {{--                            </div>--}}
                                    </div>

                                </div>
                            </li>
                        </ul>

                    </div>


                @endforeach
            @endif
            @if(isset($reviews))
                {{ $reviews->links() }}
            @endif

        </div>
        @if(\Illuminate\Support\Facades\Auth::check())
            <form action="" id="review-form">
                <div class="col-md-12 d-flex justify-content-center flex-wrap"
                     style="background: #0d6efd; color: wheat">
                    @if($rating)
                        <input id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5"
                               data-step="1">

                    @endif
                    <h2 class="w-100 text-center">Отзыв</h2>
                    <textarea name="review" class="form-control w-75" placeholder="200 символов"></textarea>
                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <button type="button" id="review-save" class="btn btn-danger h-50 w-50 mt-3">Отправить отзыв
                    </button>
                </div>
            </form>
        @else
            <div class="col-md-12 d-flex justify-content-center flex-wrap"
                 style="background: #0d6efd; color: wheat">
                <h2 class="w-100 text-center">Авторизуйтесь что-бы оставить отзыв</h2>
                <button class="btn btn-danger btn-auth">Авторизоваться</button>
            </div>
        @endif


        <div class="col-md-2"></div>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/view.js') }}"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <!-- important mandatory libraries -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        @if($rating)
            <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/star-rating.min.js"
                    type="text/javascript"></script>

            <!-- with v4.1.0 Krajee SVG theme is used as default (and must be loaded as below) - include any of the other theme JS files as mentioned below (and change the theme property of the plugin) -->
            <script
                src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.js"></script>

            <!-- optionally if you need translation for your language then include locale file as mentioned below (replace LANG.js with your own locale file) -->
            <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/locales/LANG.js"></script>
        @endif
        <script src="{{ asset('js/review-rating.js') }}"></script>
        <script src="{{ asset('js/show/answer.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('.btn-auth').click(function () {
                    Swal.fire(
                        'Good job!',
                        '<a href="/auth/github/redirect"><button class="btn btn-warning">GITHUB</button></a> <a href="/auth/linkedin/redirect"><button class="btn btn-primary">LINKEDIN</button></a>',
                        'success'
                    )
                })
            })
        </script>
</body>
</html>
