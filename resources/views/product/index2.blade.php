<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <title>Document</title>
</head>

<style>
    * {
        padding: 0;
        margin: 0;
    }

    .ui-autocomplete {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        list-style: none;
        font-size: 14px;
        text-align: left;
        background-color: #ffffff;
        border: 1px solid #cccccc;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
        box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
        background-clip: padding-box;
    }

    .ui-autocomplete > li > div {
        display: block;
        padding: 3px 20px;
        clear: both;
        font-weight: normal;
        line-height: 1.42857143;
        color: #333333;
        white-space: nowrap;
    }

    .ui-state-hover,
    .ui-state-active,
    .ui-state-focus {
        text-decoration: none;
        color: #262626;
        background-color: #f5f5f5;
        cursor: pointer;
    }

    .ui-helper-hidden-accessible {
        border: 0;
        clip: rect(0 0 0 0);
        height: 1px;
        margin: -1px;
        overflow: hidden;
        padding: 0;
        position: absolute;
        width: 1px;
    }
</style>

<body>

<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                IT-ID
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent"
                 style="display: flex; justify-content: end">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        {{--                        @if (Route::has('login'))--}}
                        {{--                            <li class="nav-item">--}}
                        {{--                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                        {{--                            </li>--}}
                        {{--                        @endif--}}

                        {{--                        @if (Route::has('register'))--}}
                        {{--                            <li class="nav-item">--}}
                        {{--                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                        {{--                            </li>--}}
                        {{--                        @endif--}}
                        <li class="nav-item">
                           <button type="button" class="btn btn-outline-primary btn-auth">Авторизоваться</button>
                        </li>

                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <a href="{{ route('personal.area') }}" class="dropdown-item">Личный кабинет</a>
                                <form id="logout-form" action="{{ route('auth.logout') }}" method="post" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <?php $avatar = \Illuminate\Support\Facades\Auth::user()->avatar ?>
                        <img src="{{ asset($avatar) }}" alt="" width="30" height="30">
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

</div>

<div class="box" id="showScroll">

</div>

<div class="col-md-12 mt-4">
    <form class="d-flex w-25" style="float: right">
        <input class="form-control me-2" id="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="button">Search</button>
    </form>
</div>
<div id="container" class="container d-flex justify-content-center mt-50 mb-50">
    <div class="sort mt-5" style="margin-right: 50px">
        <form id="sort-form" action="{{ route('product.ajax.response') }}" method="post">
            @csrf
            <div class="form-check form-switch">
                <input class="form-check-input" name="duration" type="checkbox" onclick="checkCheckbox('duration')"
                       id="Duration">
                <label class="form-check-label" for="flexSwitchCheckChecked">Duration</label>
            </div>

            <div class="form-check form-switch">
                <input class="form-check-input" name="price" type="checkbox" id="Price"
                       onclick="checkCheckbox('price')">
                <label class="form-check-label" for="flexSwitchCheckChecked">Price</label>
            </div>
            <input type="text" value="9" name="tmp" id="hidden" hidden>
            <input type="text" value="0" name="skip" id="skip" hidden>

            <label for="salary_due">Зарплата от:</label>
            <input type="number" name="salary_due" id="salary_due" class="form-control">
            <label for="salary_from">Зарплата до:</label>
            <input type="number" name="salary_from" id="salary_from" class="form-control">
            <label for="online_offline">Тип обучения</label>
            <select name="online_offline" class="form-control" id="online_offline">
                <option value="">Выбрать</option>
                <option value="1">Онлайн</option>
                <option value="0">Оффлайн</option>
            </select>
            <button type="button" id="sort-button" class="btn btn-primary mt-2">SORT</button>
        </form>
    </div>
    <div class="row" id="content">
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/scroll.js') }}"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(document).ready(function () {
        $('.btn-auth').click(function (){
            Swal.fire(
                'Good job!',
                '<a href="/auth/github/redirect"><button class="btn btn-warning">GITHUB</button></a> <a href="/auth/linkedin/redirect"><button class="btn btn-primary">LINKEDIN</button></a>',
                'success'
            )
        })
        $.ajax({
            url: 'products/ajax',
            method: "POST",
            data: {
                _token: "{{ csrf_token() }}"
            },
            success: function (response) {
                $("#search").autocomplete({
                    source: response
                });
            }
        })

        $('.btn-outline-success').click(function () {
            var content = $("#content")
            content.empty()

            $.ajax({
                url: 'product/search/ajax',
                method: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    text: $(this).parent().parent().find('#search').val(),
                },
                success: function (response) {
                    $.each(response, function (i, value) {

                        content.append("<div class=\"col-md-4 mt-2\"><div class=\"card\"><div class=\"card-body\"><div class=\"card-img-actions\"> " +
                            "<img src=\"" + value['image'] + "\" class=\"card-img img-fluid\" width=\"96\" height=\"350\" alt=\"\"> " +
                            "<div class=\"card-body bg-light text-center\">" +
                            "<div class=\"mb-2\"> <h6 class=\"font-weight-semibold mb-2\"> <a href=\"#\" class=\"text-default mb-2\" data-abc=\"true\">" + value['title'] + "</a> </h6> <a href=\"product/show/" + value['id'] + "\" class=\"text-muted\" data-abc=\"true\">Laptops & Notebooks</a> </div>" +
                            " <h3 class=\"mb-0 font-weight-semibold\">" + value['price'] + "</h3>" +
                            "<div> <i class=\"fa fa-star star\"></i> <i class=\"fa fa-star star\"></i> <i class=\"fa fa-star star\"></i> <i class=\"fa fa-star star\"></i> </div>" +
                            "<div class=\"text-muted mb-3\">" + value['views'] + " reviews</div> <button type=\"button\" class=\"btn bg-cart\"><i class=\"fa fa-cart-plus mr-2\"></i> Add to cart</button>" +
                            "</div></div></div></div></div>"
                        )
                    })
                }
            })
        })
    })
</script>

</body>
</html>
