@extends('layouts.main')
@section('content')
    <div class="bg-primary py-4 py-lg-6">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                    <div>
                        <h1 class="mb-0 text-white display-4">Filter Page</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page header -->
    <!-- Content -->
    <div class="py-6" id="py-6">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 mb-4">
                    <div class="row d-lg-flex justify-content-between align-items-center">
                        <div class="col-md-6 col-lg-8 col-xl-9 ">
                            {{--                            <h4 class="mb-3 mb-lg-0">Displaying 9 out of 68 courses</h4>--}}
                        </div>
                        <div class="d-inline-flex col-md-6 col-lg-4 col-xl-3 ">
                        {{--                            <div class="me-2">--}}
                        {{--                                <!-- Nav -->--}}
                        {{--                                <div class="nav btn-group flex-nowrap" role="tablist">--}}
                        {{--                                    <button class="btn btn-outline-white active" data-bs-toggle="tab"--}}
                        {{--                                            data-bs-target="#tabPaneGrid" role="tab" aria-controls="tabPaneGrid"--}}
                        {{--                                            aria-selected="true">--}}
                        {{--                                        <span class="bi-grid"></span>--}}
                        {{--                                    </button>--}}
                        {{--                                    <button class="btn btn-outline-white" data-bs-toggle="tab"--}}
                        {{--                                            data-bs-target="#tabPaneList" role="tab" aria-controls="tabPaneList"--}}
                        {{--                                            aria-selected="false">--}}
                        {{--                                        <span class="bi-list-stars"></span>--}}
                        {{--                                    </button>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        <!-- List  -->
                            <form action="" id="sort">
                                <div class="dropdown bootstrap-select" style="width: 100%;"><select class="selectpicker"
                                                                                                    name="sort"
                                                                                                    data-width="100%">
                                        <option value="">Сортировать по</option>
                                        <option value="price_due">Цена от меньшего к большему</option>
                                        <option value="price_from">Цена от большему к меньшего</option>
                                        <option value="date">Дата публикации</option>
                                        <option value="popular">Популярность</option>
                                        <option value="rating">Рейтинг</option>
                                        <option value="employment_assistance">Помощь в трудоустройстве</option>
                                        <option value="internship">Стажировка</option>
                                    </select>
                                    {{--                                <button type="button" tabindex="-1" class="btn dropdown-toggle bs-placeholder btn-light"--}}
                                    {{--                                        data-bs-toggle="dropdown" role="combobox" aria-owns="bs-select-1"--}}
                                    {{--                                        aria-haspopup="listbox" aria-expanded="false" title="Sort by">--}}
                                    {{--                                    <div class="filter-option">--}}
                                    {{--                                        <div class="filter-option-inner">--}}
                                    {{--                                            <div class="filter-option-inner-inner">Sort by</div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    </button>
                                    <div class="dropdown-menu ">
                                        <div class="inner show" role="listbox" id="bs-select-1" tabindex="-1">
                                            <ul class="dropdown-menu inner show" role="presentation"></ul>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-12 mb-4 mb-lg-0">

                    <!-- Card -->
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">
                            <h4 class="mb-0">Filter</h4>
                        </div>
                        <!-- Card body -->
                        <div class="card-body">
                            <span class="dropdown-header px-0 mb-2"> Category</span>
                            <!-- Checkbox -->
                            @foreach($categories as $category)
                                <div class="form-check mb-1">

                                    <input type="checkbox" name="{{ $category->title }}" class="form-check-input"
                                           id="reactCheck">
                                    <label class="form-check-label" for="reactCheck">{{ $category->title }}</label>
                                </div>
                            @endforeach
                        </div>
                        <!-- Card body -->
                        <div class="card-body border-top">
                            <span class="dropdown-header px-0 mb-2"> Ratings</span>
                            <!-- Custom control -->
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" class="form-check-input" id="starRadio1" name="customRadio"
                                       value=""
                                       checked="">
                                <label class="form-check-label" for="starRadio1">
                                    Все
                                </label>
                            </div>
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" class="form-check-input" id="starRadio1" value="5"
                                       name="customRadio">
                                <label class="form-check-label" for="starRadio1">
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <span class="fs-6">5 &amp; UP</span>
                                </label>
                            </div>
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" class="form-check-input" value="4.5" id="starRadio1"
                                       name="customRadio">
                                <label class="form-check-label" for="starRadio1">
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-half me-n1 text-warning"></i>
                                    <span class="fs-6">4.5 &amp; UP</span>
                                </label>
                            </div>
                            <!-- Custom control -->
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" class="form-check-input" value="4" id="starRadio2"
                                       name="customRadio"
                                >
                                <label class="form-check-label" for="starRadio2"> <i
                                        class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    {{--                                        <i class="bi-star-half me-n1 text-warning"></i>--}}
                                    <span class="fs-6">4.0 &amp; UP</span></label>
                            </div>
                            <!-- Custom control -->
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" class="form-check-input" value="3.5" id="starRadio3"
                                       name="customRadio">
                                <label class="form-check-label" for="starRadio3"> <i
                                        class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    {{--                                        <i class="bi-star-fill me-n1 text-warning"></i>--}}
                                    <i class="bi-star-half me-n1 text-warning"></i>
                                    <span class="fs-6">3.5 &amp; UP</span></label>
                            </div>
                            <!-- Custom control -->
                            <div class="custom-control custom-radio mb-1">
                                <input type="radio" class="form-check-input" value="3" id="starRadio4"
                                       name="customRadio">
                                <label class="form-check-label" for="starRadio4"> <i
                                        class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    <i class="bi-star-fill me-n1 text-warning"></i>
                                    {{--                                        <i class="bi-star-fill me-n1 text-warning"></i>--}}
                                    {{--                                        <i class="bi-star-half me-n1 text-warning"></i>--}}
                                    <span class="fs-6">3.0 &amp; UP</span></label>
                            </div>
                        </div>
                        <!-- Card body -->
                        {{--                        <div class="card-body border-top">--}}
                        {{--                            <span class="dropdown-header px-0 mb-2"> Skill Level</span>--}}
                        {{--                            <!-- Checkbox -->--}}
                        {{--                            <div class="form-check mb-1">--}}
                        {{--                                <input name="all_level" type="checkbox" class="form-check-input" id="allTwoCheck">--}}
                        {{--                                <label class="form-check-label" for="allTwoCheck">Все</label>--}}
                        {{--                            </div>--}}
                        {{--                            <!-- Checkbox -->--}}
                        {{--                            <div class="form-check mb-1">--}}
                        {{--                                <input name="newbie" type="checkbox" class="form-check-input" id="beginnerTwoCheck"--}}
                        {{--                                       checked="">--}}
                        {{--                                <label class="form-check-label" for="beginnerTwoCheck">Новичек</label>--}}
                        {{--                            </div>--}}
                        {{--                            <!-- Checkbox -->--}}
                        {{--                            <div class="form-check mb-1">--}}
                        {{--                                <input type="checkbox" name="amateur" class="form-check-input" id="intermediateCheck">--}}
                        {{--                                <label class="form-check-label" for="intermediateCheck">Опытный</label>--}}
                        {{--                            </div>--}}
                        {{--                            <!-- Checkbox -->--}}
                        {{--                            <div class="form-check mb-1">--}}
                        {{--                                <input type="checkbox" name="pro" class="form-check-input" id="AdvancedTwoCheck">--}}
                        {{--                                <label class="form-check-label" for="AdvancedTwoCheck">Профи</label>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <button id="btn-sort" type="button" class="btn btn-warning">Сортировать</button>
                    </div>
                    </form>
                </div>
                <!-- Tab content -->
                <div class="col-xl-9 col-lg-9 col-md-8 col-12" id="scroll-container">
                    @foreach($products as $product)
                        <div class="card mb-4 card-hover">
                            <div class="row g-0">
                                <a class="col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded"
                                   style="background-image: url( {{ asset($product['image'])  }} );"
                                   href="#">
                                    <img src="{{ asset($product['image']) }}" alt="..."
                                         class="img-fluid d-lg-none invisible">
                                </a>
                                <div class="col-lg-9 col-md-12 col-12" style="line-height: 25px">
                                    <!-- Card body -->
                                    <div class="card-body">
                                        <h3 class="mb-2 text-truncate-line-2 "><a
                                                href="{{ route('product.show', $product['id']) }}"
                                                class="text-inherit">{{ $product['h_one'] }}</a>
                                        </h3>
                                        <h4 style="float: right"> {{ $product['price'] }} ₽</h4>
                                        <!-- List inline -->
                                        <ul class="mb-5 list-inline">
                                            <li class="list-inline-item"><i
                                                    class="bi-alarm"></i>{{ $product['duration_of_the_course'] }}
                                            </li>
                                            <li class="list-inline-item">{{ mb_strimwidth($product['content'],0, 200, '...') }}</li>
                                            <li class="list-inline-item"> <span>

                                                    @if($product['rating'] == null)
                                                        @for ($i = 0; $i < 5; $i++)
                                                            <i class="bi-star me-n1 text-warning"></i>
                                                        @endfor
                                                    @endif

                                                    @for($i = 0; $i < $product['rating']; $i++)
                                                        <?php          $value = $product['rating'] - $i; ?>
                                                        @if($value < 1)
                                                            <i class="bi-star-half me-n1 text-warning"></i>
                                                            <?php break; ?>
                                                        @endif
                                                        <i class="bi-star-fill me-n1 text-warning"></i>
                                                    @endfor


                          </span>

                                                <span class="text-warning">{{ $product['rating'] }}</span>
                                                <span class="fs-6 text-muted">({{ $product['views']}})</span></li>
                                            @if($product['employment_assistance'])
                                                <i class="bi-check fs-4">Помощь в трудоустройстве</i>
                                            @endif
                                            @if($product['internship'])
                                                <i class="bi-check fs-4">Стажировка</i>
                                            @endif
                                        </ul>

                                        <!-- Row -->
                                        @if(auth()->check())
                                            <div id="test" class="row align-items-center g-0">
                                                <div class="col-auto">
                                                    @if($product['avatar'])
                                                        <img src="{{ asset($product['avatar']) }}"
                                                             class="rounded-circle avatar-xs" alt="">
                                                    @endif
                                                </div>
                                                <div class="col ms-2">
                                                    <span>{{ $product['user_name'] }}</span>
                                                </div>
                                                <div class="col-auto block-test">
                                                    <a href="#" class="text-muted bi-bookmarkark fs-3">
                                                        @if($product['bookmark'])
                                                            <i data-id="{{ $product['id'] }}"
                                                               class="bi-bookmark-check-fill"></i>
                                                        @else
                                                            <i data-id="{{ $product['id'] }}"
                                                               class="bi-bookmark fs-3"></i>
                                                        @endif
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        <div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <!-- Tab pane -->
                <div class="tab-pane fade pb-4" id="tabPaneList" role="tabpanel" aria-labelledby="tabPaneList">
                    <!-- Card -->
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/bookmark.js') }}"></script>
    @if(!isset($sub_categories))
        <script src="{{ asset('js/scroll.js') }}"></script>
    @endif
    <script src="{{ asset('js/sort.js') }}"></script>


@endsection
