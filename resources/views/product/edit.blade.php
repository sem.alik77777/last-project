<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <title>Document</title>
</head>
<body>


<header class="site-header sticky-top py-1">
    <nav class="container d-flex flex-column flex-md-row justify-content-between">
        <a class="py-2" href="#" aria-label="Product">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor"
                 stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="d-block mx-auto" role="img"
                 viewBox="0 0 24 24"><title>Product</title>
                <circle cx="12" cy="12" r="10"></circle>
                <path
                    d="M14.31 8l5.74 9.94M9.69 8h11.48M7.38 12l5.74-9.94M9.69 16L3.95 6.06M14.31 16H2.83m13.79-4l-5.74 9.94"></path>
            </svg>
        </a>
        {{--        @foreach($categories as $category)--}}
        {{--            <a class="py-2 d-none d-md-inline-block" href="#">{{ $category->title }}</a>--}}
        {{--        @endforeach--}}
    </nav>
</header>


<div class="container d-flex justify-content-center bg-light flex-wrap">
    <div class="w-100 text-center">
        <h1>Добавить Курс</h1></div>

    <form class="w-50" method="post" action="{{ route('product.update', $product->id) }}" enctype="multipart/form-data">
        @csrf
        {{--        <div class="mb-3">--}}
        {{--            <label for="categories" class="form-label">Категория</label>--}}
        {{--            <select name="category_id" class="form-control" id="categories">--}}
        {{--                <option value="{{  }}"></option>--}}
        {{--                @foreach($categories as $category)--}}
        {{--                    <option value="{{ $category->id }}">{{$category->title}}</option>--}}
        {{--                @endforeach--}}
        {{--            </select>--}}
        {{--        </div>--}}
        {{--        <div class="mb-3">--}}
        {{--            <label for="sub_categories" class="form-label">Под Категория</label>--}}
        {{--            <select name="sub_category_id" class="form-control" id="sub_categories">--}}
        {{--                @foreach($sub_categories as $category)--}}
        {{--                    <option value="{{ $category->id }}">{{$category->title}}</option>--}}
        {{--                @endforeach--}}
        {{--            </select>--}}
        {{--        </div>--}}

        <div class="mb-3">
            <input value="{{ $product->category_id }}" name="category_id" type="hidden" class="form-control">
        </div>
        <div class="mb-3">
            <input value="{{ $product->sub_category_id }}" name="sub_category_id" type="hidden" class="form-control">
        </div>

        <div class="mb-3">
            <label for="title" class="form-label">Название</label>
            <input value="{{ $product->title }}" name="title" type="text" class="form-control" id="title">
        </div>

        <div class="mb-3">
            <label for="h_one" class="form-label">Заголовок</label>
            <input value="{{ $product->h_one }}" name="h_one" type="text" class="form-control" id="h_one">
        </div>

        <div class="form-floating">
            <textarea name="description" class="form-control" placeholder="Leave a comment here"
                      id="description">{{ $product->description }}</textarea>
            <label for="description">Описание 1</label>
        </div>
        <div class="form-floating mt-3">
            <textarea name="description_two" class="form-control" placeholder="Leave a comment here"
                      id="text">{{ $product->description_two }}</textarea>
            <label for="text">Описание 2</label>
        </div>

        {{--        <div class="mb-3">--}}
        {{--            <label for="file" class="form-label">Фото</label>--}}
        {{--            <input name="image" type="file" class="form-control" id="file">--}}
        {{--        </div>--}}

        <div class="mb-3 w-75 d-flex flex-wrap ">
            <label for="tags" class="form-label w-100">Тег</label>
            @foreach($product->tags as $value)
                <div class="w-100 d-flex flex-wrap box-tags">
                    <input name="tags[]" type="text" value="{{ $value }}" class="form-control" id="tags">
                </div>
            @endforeach
            <button type="button" class="btn btn-warning">+</button>
            <button type="button" class="btn btn-danger">-</button>
        </div>

        <div class="mb-3">
            <label for="author_school" class="form-label">Автор/Школа</label>
            <input value="{{ $product->author_school }}" name="author_school" type="text" class="form-control"
                   id="author_school">
        </div>

        <div class="mb-3">
            <label for="teachers" class="form-label">Учителя</label>
            @foreach($product->teachers as $value)
                <input name="teachers[]" value="{{ $value }}" type="text" class="form-control" id="teachers">
            @endforeach
        </div>

        <div class="mb-3">
            <label for="for_reading" class="form-label">На Чтение </label>
            <input value="{{ $product->for_reading }}" name="for_reading" type="number" class="form-control"
                   id="for_reading">
        </div>

        <div class="mb-3">
            <label for="duration_of_the_course" class="form-label">Длительност/В часах</label>
            <input value="{{ $product->duration_of_the_course }}" name="duration_of_the_course" type="text"
                   class="form-control" id="duration_of_the_course">
        </div>

        <div class="mb-3">
            <label for="price" class="form-label">Цена за курс</label>
            <input value="{{ $product->price }}" name="price" type="number" class="form-control" id="price">
        </div>


        <div class="mb-3">
            <label for="credit" class="form-label">Рассрочка/Кредит</label>
            <select name="credit" class="form-control" id="credit">
                @if($product->credit == 0)
                    <option value="" selected>Не имеет</option>
                @else
                    <option value="">Не имеет</option>
                @endif
                @if($product->credit == 1)
                    <option value="1" selected>Рассрочка</option>
                @else
                    <option value="1">Рассрочка</option>
                @endif
                @if($product->credit == 2)
                    <option value="2" selected>Кредит</option>
                @else
                    <option value="2">Кредит</option>
                @endif

            </select>
        </div>

        <div class="start-end-curse d-flex">
            <div class="mb-3 w-50">
                <label for="start_curse" class="form-label">Начало курса</label>
                <input value="{{ $product->start_curse }}" name="start_curse" type="date" class="form-control"
                       id="start_curse">
            </div>

            <div class="mb-3 w-50">
                <label for="end_curse" class="form-label">Конец курса</label>
                <input value="{{ $product->end_curse }}" name="end_curse" type="date" class="form-control"
                       id="end_curse">
            </div>
        </div>

        <div class="due-from-salary d-flex">
            <div class="mb-3 w-50">
                <label for="salary_due" class="form-label">Зарплата "от" после прохождения курса</label>
                <input value="{{ $product->salary_due }}" name="salary_due" type="number" class="form-control"
                       id="salary_due">
            </div>

            <div class="mb-3 w-50">
                <label for="salary_from" class="form-label">Зарплата "до" после прохождения курса</label>
                <input value="{{ $product->salary_from }}" name="salary_from" type="number" class="form-control"
                       id="salary_from">
            </div>
        </div>

        <div class="mb-3">
            <label for="online_offline" class="form-label">Онлайн/Оффлайн курс</label>
            <select name="online_offline" class="form-control" id="online_offline">
                @if($product->online_offline == 1)
                    <option value="1" selected>Онлайн</option>
                @else
                    <option value="1">Онлайн</option>
                @endif
                @if($product->online_offline == 0)
                    <option value="0" selected>Оффлайн</option>
                @else
                    <option value="0">Оффлайн</option>
                @endif
            </select>
        </div>

        <script
            src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous"></script>

        <div class="form-group mt-4 mb-4">
            <div class="captcha">
                <span>{!! captcha_img() !!}</span>
                <button type="button" class="btn btn-danger" class="reload" id="reload">
                    ↻
                </button>
            </div>
        </div>
        <div class="form-group mb-4">
            <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    @if ($errors->any())
        <div class="alert alert-danger h-50" style="margin-left: 10px">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
    $(document).ready(function () {
        var session = "<?= $session ?>"
            if (session === 'save'){
                Swal.fire(
                    'Хорошая работа!',
                    'Вы обновили курс!',
                    'success'
                )
            }
        $('.btn-warning').click(function () {
            $('.box-tags').append('<input name="tags[]" type="text" class="form-control tags" id="tags">')
        });
        $('.btn-danger').click(function () {
            var int = $('.box-tags').find('.tags').length + 1;
            var int2 = $('.box-tags').find('.tags').length - 1;

            if (int > 1) {

                $('.tags').each(function (index, value) {
                    console.log(index, int2)
                    if (index === int2) {
                        $(value).detach()
                    }
                })
            }

        });
    })
</script>

</body>
</html>


<script type="text/javascript">
    $('#reload').click(function () {
        $.ajax({
            type: 'GET',
            url: '/reload/captcha',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
{{--<html>--}}
{{--<head>--}}
{{--    <title>reCAPTCHA demo: Simple page</title>--}}
{{--    <script src="https://www.google.com/recaptcha/api.js" async defer></script>--}}
{{--</head>--}}
{{--<body>--}}
{{--<form action="?" method="POST">--}}
{{--    <div class="g-recaptcha" data-sitekey="6LdMCaEcAAAAANVWnkmys0irTzESAjWNgqn3cgiR"></div>--}}
{{--    <br/>--}}
{{--    <input type="submit" value="Submit">--}}
{{--</form>--}}
{{--</body>--}}
{{--</html>--}}

{{--    <!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
{{--    <title>Laravel 8 Form Captcha Validation</title>--}}
{{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}
{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--}}
{{--</head>--}}
{{--<body>--}}
{{--<div class="container mt-4">--}}
{{--    @if(session('status'))--}}
{{--        <div class="alert alert-success">--}}
{{--            {{ session('status') }}--}}
{{--        </div>--}}
{{--    @endif--}}
{{--    <div class="card">--}}
{{--        <div class="card-header text-center font-weight-bold">--}}
{{--            <h2>Laravel 8 Add Captcha in Form For Validation</h2>--}}
{{--        </div>--}}
{{--        <div class="card-body">--}}
{{--            <form name="captcha-contact-us" id="captcha-contact-us" method="post" action="{{url('captcha-validation')}}">--}}
{{--                @csrf--}}
{{--                <div class="form-group">--}}
{{--                    <label for="exampleInputEmail1">Name</label>--}}
{{--                    <input type="text" id="name" name="name" class="@error('name') is-invalid @enderror form-control">--}}
{{--                    @error('name')--}}
{{--                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label for="exampleInputEmail1">Email</label>--}}
{{--                    <input type="email" id="email" name="email" class="@error('email') is-invalid @enderror form-control">--}}
{{--                    @error('email')--}}
{{--                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--                <div class="form-group">--}}
{{--                    <label for="exampleInputEmail1">Message</label>--}}
{{--                    <textarea name="message" class="@error('message') is-invalid @enderror form-control"></textarea>--}}
{{--                    @error('message')--}}
{{--                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>--}}
{{--                    @enderror--}}
{{--                </div>--}}
{{--                <div class="form-group mt-4 mb-4">--}}
{{--                    <div class="captcha">--}}
{{--                        <span>{!! captcha_img() !!}</span>--}}
{{--                        <button type="button" class="btn btn-danger" class="reload" id="reload">--}}
{{--                            ↻--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="form-group mb-4">--}}
{{--                    <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">--}}
{{--                </div>--}}
{{--                <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

{{--</body>--}}
{{--</html>--}}
