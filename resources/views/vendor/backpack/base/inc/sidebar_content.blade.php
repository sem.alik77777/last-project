<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item"><a class="nav-link" href="/admin/categories">Category</a></li>
<li class="nav-item"><a class="nav-link" href="/admin/sub-categories">Sub Category</a></li>
<li class="nav-item"><a class="nav-link" href="/admin/products">Product</a></li>
