<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('sub_category_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->decimal('price')->nullable()->index();
            $table->tinyInteger('credit')->nullable()->index();
            $table->string('title');
            $table->text('content');
            $table->string('image');
            $table->json('tags')->nullable();
            $table->string('author_school')->index();
            $table->json('teachers')->nullable();
            $table->string('duration_of_the_course');
            $table->date('start_curse')->nullable();
            $table->date('end_curse')->nullable();
            $table->text('url');
            $table->string('user_cookie');
            $table->string('ip');
            $table->integer('salary_due')->nullable()->index();
            $table->integer('salary_from')->nullable()->index();
            $table->boolean('online_offline')->default(0)->index();
            $table->boolean('status')->default(0);
            $table->string('h_one');
            $table->integer('views')->default(0);
            $table->boolean('employment_assistance')->default(false);
            $table->boolean('internship')->default(false);
            $table->timestamps();

            $table->foreign('sub_category_id')
                ->on('sub_categories')
                ->references('id')
                ->onDelete('cascade');
            $table->foreign('category_id')
                ->on('categories')
                ->references('id')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
