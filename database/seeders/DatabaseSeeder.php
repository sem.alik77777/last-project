<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class DatabaseSeeder extends Seeder
{
    use WithFaker;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $title = [
            'React. Интенсивный курс для программистов',
            'Курс Java разработчик',
            'МОЯ ПЕРВАЯ ИГРА НА UNITY',
            'Разработка без кода в Bubble.io (NoCode)',
            'Deep Learning и нейронные сети — Практический курс по глубоким нейронным сетям на Python'
        ];
        $author = ['Елена'];
        $for_reading = [4, 7, 4, 6, 5];
        $price = [9800, 12000, 23000, 14000, 36900];
        $url = [
            'https://js.dmitrylavrik.ru/react/?utm=site-courses',
            'https://prog.kiev.ua/java-online',
            'https://motaynaus.ru/hypercasual',
            'https://welovenocode.com/course',
            'https://skillfactory.ru/nejronnye-seti-deep-learning'
        ];
        $author_school = ['Дмитрий Лаврик', 'Дмитрий Лаврик', 'Дмитрий Лаврик', 'Дмитрий Лаврик', 'Андрей Зимовнов'];
        $content = [
            'Изучить философию и дух компонентного подхода современных javascript-фреймворков, научиться использовать React для решения практических задач.',
            'Индивидуальное обучение со свободным графиком. С гарантированной стажировкой в проекте.',
            'Освой Unity и язык C#. Познай кайф от GameDev. Выпусти игру через 3 месяца.',
            'А мы с вами. Обычные люди, которые понимают, что они хотят и могут описать словами функционал приложения или сайта. Также как вам больше не нужен телефонист, чтобы позвонить, почтальон, чтобы отправить письмо, или дизайнер, чтобы собрать простой лендинг на Тильде. Теперь революция добралась и до разработчиков.',
            'Научитесь самостоятельно создавать мобильные приложения и веб-сервисы.Не используя код. За неделю.'
        ];
        $content_two = ['Vue проще, а может быть, даже просто лучше. Но React популярнее, вакансий и заказов на фрилансе ощутимо больше. Кроме того, сейчас многие опасаются Vue из-за неоднозначных новшеств, запланированных в версии 3.',
            'Java — язык программирования общего назначения. На Java пишут бизнес логику веб приложений и мобильные приложения под операционную систему Android. С помощью Java созданы такие проекты как Minecraft и Privat24, а сам язык используют в Google, Facebook, eBay и многих других известных компаниях.',
            'Проходите курс, когда удобно вам. Сразу после оплаты у вас будет доступ ко всем лекциям. В первую лекцию мы добавили ссылки на чаты студентов — заходите в них, чтобы пообщаться с коллегами и получить консультации ментора.',
            'А мы с вами. Обычные люди, которые понимают, что они хотят и могут описать словами функционал приложения или сайта. Также как вам больше не нужен телефонист, чтобы позвонить, почтальон, чтобы отправить письмо, или дизайнер, чтобы собрать простой лендинг на Тильде. Теперь революция добралась и до разработчиков.',
            'Машинное обучение — одна из самых быстрорастущих областей знаний! Инвестиции в машинное обучение вырастут в 5 раз в течение ближайших 3 лет. И Deep Learning — это передний край данной индустрии.'
        ];
        $h_one = [
            'React. Интенсивный курс для программистов',
            'Курс Java разработчик',
            'МОЯ ПЕРВАЯ ИГРА НА UNITY',
            'Разработка без кода в Bubble.io (NoCode)',
            'Deep Learning и нейронные сети — Практический курс по глубоким нейронным сетям на Python'];
        $teacher = ['Дмитрий Лаврик', 'Дмитрий Лаврик', 'Дмитрий Лаврик'];
        $duration_of_the_course = [8, 12, 14, 7, 10];

        $imagePath = [
            'react.png',
            'java-razrabotchik.jpg',
            'game-dev-na-unity.png',
            'razrabotka-bez-koda-v-bubble.io-nocode.jpg',
            'deep-learning-na-python.jpg'
        ];

        User::factory(30)->create();
        Category::factory()->count(3)
            ->has(SubCategory::factory()->count(3))
            ->create();

        for ($i = 0; $i < count($title); $i++) {
            $image = Image::make('public/images/' . $imagePath[$i]);
            $image->save('public/images/' . md5($imagePath[$i]) . '.jpg');

            DB::table('products')->insert([
                'category_id' => rand(1, 3),
                'sub_category_id' => rand(1, 3),
                'title' => $title[$i],
                'image' => 'images/' . $image->filename . '.jpg',
                'price' => $price[$i],
                'url' => $url[$i],
                'author_school' => $author_school[$i],
                'content' => $content[$i],
                'h_one' => $h_one[$i],
                'duration_of_the_course' => $duration_of_the_course[$i],
                'salary_due' => rand(1000, 10000),
                'salary_from' => rand(11000, 20000),
                'start_curse' => '02.10.2021',
                'end_curse' => '02.10.2021',
                'user_cookie' => Str::random(16),
                'ip' => '127.0.0.1'
            ]);
        }
    }
}
