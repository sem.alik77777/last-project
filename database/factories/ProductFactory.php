<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => rand(1, 3),
            'sub_category_id' => rand(1, 3),
            'user_id' => rand(1,30),
            'title' => $this->faker->text(100),
            'content' => $this->faker->text(200),
            'image' => 'storage/images/'.$this->faker->image('public/storage/images/', 640, 480, null, false),
            'h_one' => $this->faker->text(200),
            'tags' => ['twitter'],
            'author_school' => $this->faker->name,
            'teachers' => ['Дмитрий Коваленько'],
            'duration_of_the_course' => rand(1, 20),
            'views' => rand(1, 1000),
            'price' => rand(11, 444),
            'url' => $this->faker->url,
            'status' => 1,
            'salary_due' => rand(1000, 10000),
            'salary_from' => rand(11000, 20000),
            'start_curse' => $this->faker->date,
            'end_curse' => $this->faker->date,
            'user_cookie' => Str::random(16),
            'ip' => '127.0.0.1'
        ];
    }
}
