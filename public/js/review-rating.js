var button = document.getElementById('review-save')

button.addEventListener("click", function () {
    const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    var form = document.getElementById('review-form');
    var formData = new FormData(form);


    setTimeout(function () {
    fetch('/review/save/ajax', {
        method: 'post',
        body: formData,
        headers: {
            "X-CSRF-TOKEN": token
        }
    }).then(async function (response) {
        // console.log(response.json())
        location.reload()
    })
    }, 1000)
})

