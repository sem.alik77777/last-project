var btnSort = document.getElementById('btn-sort')

btnSort.addEventListener("click", function () {
    fetchSort()
})


document.querySelector('.selectpicker').addEventListener("change", function () {
    fetchSort()
})

function fetchSort() {
    var form = document.getElementById('sort')
    var formData = new FormData(form)
    let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    var url = '/product/sort'
    var block = document.getElementById('scroll-container');


    fetch(url, {
        method: 'POST',
        body: formData,
        headers: {
            "X-CSRF-TOKEN": token
        }

    }).then(async function (response) {
        const tmp = response.json().then(function (value){
            return  value.length;
        })


        tmp.then(function (value){
            if (value > 1){
                block.innerHTML = "";
                tmp.json().then(value =>
                    value.forEach(function callback(currentValue, index, product) {
                        block.innerHTML = block.innerHTML + "<div class=\"card mb-4 card-hover\">\n" +
                            "                            <div class=\"row g-0\">\n" +
                            "                                <a class=\"col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded\" style=\"background-image: url(http://localhost:1111/" + product[index]['image'] + ");\" href=\"#\">\n" +
                            "                                    <img src=\"http://localhost:1111/images/006e62370474efe16ef8408efa7d0eb2.jpg\" alt=\"...\" class=\"img-fluid d-lg-none invisible\">\n" +
                            "                                </a>\n" +
                            "                                <div class=\"col-lg-9 col-md-12 col-12\">\n" +
                            "                                    <!-- Card body -->\n" +
                            "                                    <div class=\"card-body\">\n" +
                            "                                        <h3 class=\"mb-2 text-truncate-line-2 \"><a href=\"product/show/" + product[index]['id'] + "\" class=\"text-inherit\">" + product[index]['h_one'] + "</a>\n" +
                            "                                        </h3>\n" +
                            "<h4 style=\"float: right\"> " + product[index]['price'] + " ₽</h4>" +
                            "                                        <!-- List inline -->\n" +
                            "                                        <ul class=\"mb-5 list-inline\">\n" +
                            "                                            <li class=\"list-inline-item\"><i class=\"bi-alarm\"></i>" + product[index]['duration_of_the_course'] + " минут на обучение\n" +
                            "                                            </li>\n" +
                            "                                            <li class=\"list-inline-item\">\n" +
                            "                                            </li>\n" +
                            "                                            <li class=\"list-inline-item\"> <span>\n" +
                            "                                             " + product[index]['stars'] + "   " +
                            "                          </span>\n" +
                            "                                                <span class=\"text-warning\">" + product[index]['rating'] + "</span>\n" +
                            "                                                <span class=\"fs-6 text-muted\">(" + product[index]['views'] + ")</span></li>\n" +
                            "                                        </ul>\n" +
                            "                                                 " + product[index]['content'] + " " +
                            "                                        <!-- Row -->\n" +
                            "                                        <div class=\"row align-items-center g-0\">\n" +
                            "                                            <div class=\"col-auto\">\n" +
                            " " + product[index]['avatar'] + " " +
                            "                                            </div>\n" +
                            "                                            <div class=\"col ms-2\">\n" +
                            "                                                <span>" + product[index]['name'] + "</span>\n" +
                            "                                            </div>\n" +
                            "                                            <div class=\"col-auto\">\n" +
                            "                                                <a href=\"#\" class=\"text-muted bi-bookmarkark\">\n" +
                            "                                                    <i class=\"bi-bookmark  fs-3\"></i>\n" +
                            "                                                </a>\n" +
                            "                                            </div>\n" +
                            "                                                 " + product[index]['internship'] + " " +
                            "                                                 " + product[index]['employment_assistance'] + " " +
                            "                                        </div>\n" +
                            "                                        <div>\n" +
                            "                                        </div>\n" +
                            "                                    </div>\n" +
                            "                                </div>\n" +
                            "                            </div>\n" +
                            "                        </div>"
                    })
                )
                    .catch(function () {
                        block.innerHTML = '<div class=""></div> <i class="bi-search fa-2x">Ничего не найдено по данным критериям</i>';
                    })
            }else {
                block.innerHTML = '<div class=""></div> <i class="bi-search fa-2x">Ничего не найдено по данным критериям</i>';
            }
        })

    })
}
