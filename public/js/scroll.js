var tmp = 9;

window.addEventListener('scroll', event => {
    if (scrollY + innerHeight === document.body.scrollHeight) {
        console.log(tmp)
        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        var url = 'products/ajax/response'
        var data = {
            _token: token,
            skip: tmp,
            tmp: 9
        }

        var block = document.getElementById('scroll-container');

        fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                // "Accept": "application/json, text-plain, */*",
                // "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-TOKEN": token
            },


        }).then(async function (response) {
            response.json().then(value =>
                value.map(product =>
                    block.innerHTML = block.innerHTML + "<div class=\"card mb-4 card-hover\">\n" +
                        "                            <div class=\"row g-0\">\n" +
                        "                                <a class=\"col-12 col-md-12 col-xl-3 col-lg-3 bg-cover img-left-rounded\" style=\"background-image: url(http://localhost:1111/" + product['image'] + ");\" href=\"#\">\n" +
                        "                                    <img src=\"http://localhost:1111/images/006e62370474efe16ef8408efa7d0eb2.jpg\" alt=\"...\" class=\"img-fluid d-lg-none invisible\">\n" +
                        "                                </a>\n" +
                        "                                <div class=\"col-lg-9 col-md-12 col-12\">\n" +
                        "                                    <!-- Card body -->\n" +
                        "                                    <div class=\"card-body\">\n" +
                        "                                        <h3 class=\"mb-2 text-truncate-line-2 \"><a href=\"/product/show/" + product['id'] + "\" class=\"text-inherit\">" + product['h_one'] + "</a>\n" +
                        "                                        </h3>\n" +
                        "<h4 style=\"float: right\"> " + product[index]['price'] + " ₽</h4>"+
                        "                                        <!-- List inline -->\n" +
                        "                                        <ul class=\"mb-5 list-inline\">\n" +
                        "                                           <li class=\"list-inline-item\"><i class=\"bi-alarm\"></i>" + product['duration_of_the_course'] +" минут на обучение\n" +
                        "                                            </li>\n" +
                        "                                            <li class=\"list-inline-item\">\n" +
                        "                                  " + product['skill'] + "   " +
                        "                                            </li>\n" +
                        "                                            <li class=\"list-inline-item\"> <span>\n" +
                        "                                                     " + product['stars'] + "   " +
                        "                          </span>\n" +
                        "                                                <span class=\"text-warning\">" + product['rating'] + "</span>\n" +
                        "                                                <span class=\"fs-6 text-muted\">("+ product['views'] +")</span></li>\n" +
                        "                                        </ul>\n" +
                        "                                        <!-- Row -->\n" +
                        "                                        <div class=\"row align-items-center g-0\">\n" +
                        "                                            <div class=\"col-auto\">\n" +
                        "                                                " + product['avatar'] + "\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"col ms-2\">\n" +
                        "                                                <span>" + product['user_name'] + "</span>\n" +
                        "                                            </div>\n" +
                        "                                            <div class=\"col-auto\">\n" +
                        "                                                <a href=\"#\" class=\"text-muted bi-bookmarkark\">\n" +
                        "                                                    " + product['bookmark'] + "\n" +
                        "                                                </a>\n" +
                        "                                            </div>\n" +
                        "                                        </div>\n" +
                        "                                        <div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                            </div>\n" +
                        "                        </div>"
                ))

        })

        tmp = tmp + 9;
    }
})

