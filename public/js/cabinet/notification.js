var button = document.getElementById('btn-notification');
var notification = document.getElementById('count-notification')
var notificationList = document.getElementById('notification-list')

notification.addEventListener("click", function (){
if (notificationList.style.display === 'none'){
    notificationList.style.display = 'block';
}else {
    notificationList.style.display = 'none';
}

})
button.addEventListener("click", function () {
    var form = document.getElementById('form-notification');
    var formData = new FormData(form);
    let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    fetch('/notification/read/ajax', {
        method: "post",
        headers: {
            "X-CSRF-TOKEN": token
        },
        body: formData
    }).then(function (response){
            location.reload()
    })
})
