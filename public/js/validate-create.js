var buttonNext = document.getElementById('next')
var c = 0;
var step_one = 0;

buttonNext.addEventListener("click", function () {
    var inputs = document.querySelectorAll('input, textarea');
    for (i = 0; i < inputs.length; i++) {
        if (inputs[i].getAttribute('name') === 'title') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('title-div').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('title-div').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'h_one') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('h_one-div').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('h_one-div').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'description_one') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('description_one').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('description_one').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'description_two') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('description_two').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('description_two').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'author_school') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('author_school').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('author_school').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'for_reading') {
            if (inputs[i].value === '') {
                document.getElementById('for_reading').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('for_reading').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'tags[]') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('tags').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('tags').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'teachers[]') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('teachers').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('teachers').style.border = "green 2px solid";
            }
        }
        if (inputs[i].getAttribute('name') === 'url') {
            if (inputs[i].value.length <= 3) {
                document.getElementById('url').style.border = "red 2px solid";
                c++;
            } else {
                document.getElementById('url').style.border = "green 2px solid";
            }
        }
    }

    if (c === 0) {
        document.getElementById('tab_step1').className = 'step-tab-panel step-tab-info'
        document.getElementById('tab_step2').className = 'step-tab-panel step-tab-gallery active'
       var step2 = document.getElementById('step-tab-2')
        step2.style.display = 'block'
        step_one = 1;
    }else {
        document.getElementById('tab_step1').className = 'step-tab-panel step-tab-info active'
        document.getElementById('tab_step2').className = 'step-tab-panel step-tab-gallery'
    }
    c = 0
})
