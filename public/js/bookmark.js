document.addEventListener("click", function (event) {
    if (event.target.classList.contains('bi-bookmark') || event.target.classList.contains('bi-bookmark-check-fill')) {
        let token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        var data = {
            id: event.target.getAttribute('data-id')
        }
        fetch('/bookmark/store', {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": token
            },
        }).then(async function (response){
            // console.log(response.text())
            Promise.resolve(response.text()).then(function (value){
                if (value === 'delete'){
                    event.target.className = 'bi-bookmark fs-3'
                }else {
                    event.target.className = 'bi-bookmark-check-fill fs-3'
                }
            })
        })
    }
})


