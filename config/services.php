<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */
    'recaptcha' => [
        'key' => env ( 'GOOGLE_RECAPTCHA_KEY' ),
        'secret' => env ( 'GOOGLE_RECAPTCHA_SECRET' ),
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'github' => [
        'client_id' => '50708b16a8c8c51fb881',
        'client_secret' => '8ee9d7486d6a3214935ecc75505e3249990be7af',
        'redirect' => '/auth/github/callback',
    ],
    'linkedin' => [
        'client_id' => '8637g8rkk1x64b',
        'client_secret' => 'QoVNjWQlkHtQ9xDF',
        'redirect' => '/auth/linkedin/callback',
    ],
    'facebook' => [
        'client_id' => '543177066748269',
        'client_secret' => 'c60834fdb6b5d4183e3f49c8687f1ec0',
        'redirect' => '/auth/facebook/callback',
    ],
];
