<?php

use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Authorization\AuthorizationController;
use App\Http\Controllers\BookmarkController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PersonalAreaController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReviewAndRatingController;
use App\Http\Controllers\SubscriptionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('/main', [MainController::class, 'index']);

Route::post('products/ajax', [ProductController::class, 'productsAjax']);
Route::post('product/ajax/store', [ProductController::class, 'ajaxStore']);
Route::post('product/search/ajax', [ProductController::class, 'productsSearchAjax']);
Route::post('products/ajax/response', [ProductController::class, 'ajaxResponse'])->name('product.ajax.response');
Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
Route::post('product/store', [ProductController::class, 'store'])->name('product.store');
Route::get('product/show/{id}', [ProductController::class, 'show'])->name('product.show');
Route::get('product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
Route::post('product/update/{id}', [ProductController::class, 'update'])->name('product.update');
Route::post('product/view', [ProductController::class, 'viewProduct']);
Route::post('product/sort', [ProductController::class, 'sort']);
Route::get('products/{category?}', [ProductController::class, 'index'])->name('product.index');

Auth::routes();

Route::get('/personal/area', [PersonalAreaController::class, 'index'])->name('personal.area');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/auth/logout', [LogoutController::class, 'logout'])->name('auth.logout');
Route::get('/auth/github/redirect', [AuthorizationController::class, 'githubRedirect']);
Route::get('auth/github/callback', [AuthorizationController::class, 'githubCallback']);

Route::get('/auth/linkedin/redirect', [AuthorizationController::class, 'linkedinRedirect']);
Route::get('/auth/linkedin/callback', [AuthorizationController::class, 'linkedinCallback']);

Route::get('/auth/facebook/redirect', [AuthorizationController::class, 'facebookRedirect']);
Route::get('/auth/facebook/callback', [AuthorizationController::class, 'facebookCallback']);


Route::post('/review/save/ajax', [ReviewAndRatingController::class, 'storeAjax']);
Route::get('/review/child/store', [ReviewAndRatingController::class, 'storeReviewChildren'])->name('review.child.store');
Route::post('/review/paginate', [ReviewAndRatingController::class, 'reviewPaginate'])->name('review.paginate');
Route::get('/review/delete/{id}', [ReviewAndRatingController::class, 'reviewDelete'])->name('review.delete');
Route::get('/review/child/delete/{id}', [ReviewAndRatingController::class, 'reviewChildDelete'])->name('review.child.delete');
Route::get('/reload/captcha', [ProductController::class, 'reloadCaptcha']);

Route::post('/bookmark/store', [BookmarkController::class, 'store']);
Route::post('/notification/read/ajax', [PersonalAreaController::class, 'notificationReadAjax']);
Route::post('/notification/all/delete', [PersonalAreaController::class, 'removeAllNotification']);
Route::post('delete/notifications', [PersonalAreaController::class, 'deleteNotifications']);
Route::post('read/notifications', [PersonalAreaController::class, 'readNotifications']);
Route::post('about/me/store', [PersonalAreaController::class, 'aboutMeStore'])->name('about.me.store');

Route::post('ajax/subscription/store', [SubscriptionController::class, 'ajaxSubscriptionStore']);
Route::post('get/user/curses', [PersonalAreaController::class, 'getAllCursesForUser']);

Route::get('test', [ProductController::class,'apiHHWork']);
