<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class Product extends Model
{
    use HasFactory, CrudTrait;

    protected $fillable = [
        'category_id',
        'sub_category_id',
        'title',
        'content',
        'price',
        'credit',
        'image',
        'tags',
        'author_school',
        'teachers',
        'duration_of_the_course',
        'start_curse',
        'end_curse',
        'url',
        'salary_due',
        'salary_from',
        'online_offline',
        'status',
        'h_one',
        'views',
        'user_cookie',
        'ip'
    ];

    protected $casts = [
        'tags' => 'array',
        'teachers' => 'array'
    ];

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function subCategory(): BelongsTo
    {
        return $this->belongsTo(SubCategory::class);
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            $img = explode('/', $obj->image);
            Storage::disk('upload')->delete($img[1] . '/' . $img[2]);
        });
    }

    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        // or use your own disk, defined in config/filesystems.php
        $disk = 'upload';
        // destination path relative to the disk above
        $destination_path = "images";

        // if the image was erased
        if ($value == null) {
            // delete the image from disk
            Storage::disk($disk)->delete($this->{$attribute_name});

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (Str::startsWith($value, 'data:image')) {
            // 0. Make the image
            $image = Image::make($value)->encode('jpg', 90);

            // 1. Generate a filename.
            $filename = md5($value . time()) . '.jpg';

            // 2. Store the image on disk.
            Storage::disk($disk)->put($destination_path . '/' . $filename, $image->stream());

            // 3. Delete the previous image, if there was one.
            Storage::disk($disk)->delete($this->{$attribute_name});

            // 4. Save the public path to the database
            // but first, remove "public/" from the path, since we're pointing to it
            // from the root folder; that way, what gets saved in the db
            // is the public URL (everything that comes after the domain name)
            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = 'storage/images' . '/' . $filename;
        } else {
            return $this->attributes[$attribute_name] = $value;
        }
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function update(array $attributes = [], array $options = [])
    {
        if ($this->status == 0) {
            if (request()->get('status') == 1) {
                if ($this->user_id != null) {
                $notification = new Notification();
                $notification->text = 'Ваш курс одобрен администратором.' .' "'. $this->title .'"';
                $notification->user_id = $this->user_id;
                $notification->product_id = $this->id;
                $notification->save();
                }
            }
        }else{
            $notification = Notification::query()
                ->where('product_id',$this->id)->first();
            if (!empty($notification)){
                $notification->delete();
            }
        }
        $redirect_location = parent::update(request()->input());
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function bookmark(): HasOne
    {
        return $this->hasOne(Bookmark::class);
    }

}
