<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AboutMe extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone',
        'user_id',
        'first_name',
        'last_name',
        'date_of_birth',
        'country',
        'city',
        'about_me',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
