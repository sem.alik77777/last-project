<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CookieMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (empty($_COOKIE['it-id'])){
            setcookie('it-id', md5(Str::random(10)), strtotime("+1 year"));
            return $next($request);
        }
        return $next($request);
    }
}
