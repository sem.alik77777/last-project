<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'category_id' => 'required|numeric',
            'sub_category_id' => 'required|numeric',
            'title' => 'required|string|min:5|max:255',
            'h_one' => 'required|string|min:5|max:255',
            'content' => 'required|string|min:10|max:5000',
            'image-upload' => 'required|mimes:jpeg,png,jpg',
            'author_school' => 'required|min:3|string',
            'duration_of_the_course_int' => 'required|numeric',
            'price' => 'required|numeric',
            'url' => 'required',
            'start_curse' => 'date',
            'end_curse' => 'date',
            'salary_due' => 'required|numeric',
            'salary_from' => 'required|numeric',
            'online_offline' => 'required|numeric',
            'views' => 'numeric',
            'g-recaptcha-response' => 'required|recaptcha'
        ];
    }
}
