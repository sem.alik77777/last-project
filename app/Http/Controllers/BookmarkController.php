<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use App\Models\Notification;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookmarkController extends Controller
{
    public function store()
    {
        $examination = Bookmark::query()->where('user_id', Auth::id())->where('product_id', \request()->get('id'))->get();
        if ($examination->isEmpty()){
            $bookmark = new Bookmark();
            $bookmark->product_id = \request()->get('id');
            $bookmark->user_id = Auth::id();
            $bookmark->save();

            $notification = new Notification();
            $notification->text = 'Вы успешно добавили в свои закладки курс -' . Product::query()->find(\request()->get('id'))->title;
            $notification->user_id = Auth::id();
            $notification->product_id = \request()->get('id');
            $notification->save();
        }else{
            Bookmark::query()->find($examination->pluck('id')[0])->delete();
            $notification = new Notification();
            $notification->text = 'Вы успешно удалили курс из закладок -' . Product::query()->find(\request()->get('id'))->title;
            $notification->user_id = Auth::id();
            $notification->product_id = \request()->get('id');
            $notification->save();

            return 'delete';
        }
        return 'success';
    }
}
