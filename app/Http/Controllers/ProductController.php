<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Bookmark;
use App\Models\Category;
use App\Models\Notification;
use App\Models\Product;
use App\Models\Review;
use App\Models\SubCategory;
use App\Models\Subscription;
use App\Models\User;
use App\Service\ProductSkillTegService;
use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;


class ProductController extends Controller
{
    private $serviceTeg;

    public function __construct(ProductSkillTegService $service)
    {
        $this->serviceTeg = $service;
    }

    public function index($id = null)
    {
        if (!empty($id)) {
            $products = Product::query()->where('sub_category_id', $id)->get();
            $categories = Category::all();
            $sub_categories = true;

            $products->map(function ($product, $index) use ($products) {
                $bookmark = $product->bookmark;
                $user = $product->user;

                if (empty($user)) {
                    $products[$index]['user_name'] = false;
                    $products[$index]['avatar'] = false;
                } else {
                    $products[$index]['user_name'] = $user->name;
                    $products[$index]['avatar'] = $user->avatar;
                }
                if (empty($bookmark)) {
                    $products[$index]['bookmark'] = false;
                } else {
                    $products[$index]['bookmark'] = true;
                }
            });

            foreach ($products as $key => $product) {
                $rating = DB::table('ratings')->where('product_id', $product->id)->get()->avg('rating');
                $products[$key]['stars'] = $this->serviceTeg->renderStarTag($products[$key]['rating']);
                if ($rating == null) {
                    $products[$key]['rating'] = 0;
                } else {
                    $products[$key]['rating'] = $rating;
                }
            }

            return \view('product.index',
                [
                    'products' => $products,
                    'categories' => $categories,
                    'sub_categories' => $sub_categories
                ]);
        }

        $products = Product::query()->with(['bookmark', 'user'])->take(6)->get();

        $products->map(function ($product, $index) use ($products) {
            $bookmark = $product->bookmark;
            $user = $product->user;

            if (empty($user)) {
                $products[$index]['user_name'] = false;
                $products[$index]['avatar'] = false;
            } else {
                $products[$index]['user_name'] = $user->name;
                $products[$index]['avatar'] = $user->avatar;
            }
            if (empty($bookmark)) {
                $products[$index]['bookmark'] = false;
            } else {
                $products[$index]['bookmark'] = true;
            }
        });

        foreach ($products as $key => $product) {
            $rating = DB::table('ratings')->where('product_id', $product->id)->get()->avg('rating');
            $products[$key]['stars'] = $this->serviceTeg->renderStarTag($rating);
            if ($rating == null) {
                $products[$key]['rating'] = 0;
            } else {
                $products[$key]['rating'] = $rating;
            }
        }
        $categories = Category::all();

        return \view('product.index',
            [
                'products' => $products->toArray(),
                'categories' => $categories
            ]);
    }

    public function ajaxResponse()
    {
        $products = Product::query()->get();
        $count = $products->count();
        if (\request()->get('skip') <= $count) {
            $products = array_slice($products->toArray(), \request()->get('skip'), \request()->get('tmp'));

            foreach ($products as $key => $product) {
                $rating = (DB::table('ratings')
                    ->where('product_id', $product['id'])
                    ->get()->avg('rating')) ? DB::table('ratings')
                    ->where('product_id', $product['id'])->get()->avg('rating') : 0;
//                $products[$key]['skill'] = $this->serviceTeg->renderTag($product);
                $products[$key]['rating'] = $rating;
                $products[$key]['stars'] = $this->serviceTeg->renderStarTag($products[$key]['rating']);
                if ($products[$key]['user_id'] !== null) {
                    $products[$key]['user_name'] = (User::query()->find($product['user_id'])->name);
                    $products[$key]['avatar'] = '<img src="' . User::query()->find($product['user_id'])->avatar . '" class="rounded-circle avatar-xs" alt="">';
                } else {
                    $products[$key]['user_name'] = '';
                    $products[$key]['avatar'] = '';
                }
                if (Auth::check()) {
                    if (Bookmark::query()->where('product_id', $product['id'])->where('user_id', Auth::id())->get()->isNotEmpty()) {
                        $products[$key]['bookmark'] = '<i data-id="' . $product['id'] . '" class="bi-bookmark-check-fill fs-3"></i>';
                    } else {
                        $products[$key]['bookmark'] = '<i data-id="' . $product['id'] . '" class="bi-bookmark fs-3"></i>';
                    }

                }
            }
            return response()->json($products);
        }
    }

    public function sort()
    {
        $request = request()->input();

        if (request()->get('customRadio') == '') {
            unset($request['customRadio']);
        }
        if (request()->get('sort') == null) {
            unset($request['sort']);
        }

        if (empty($request)) {
            $products = Product::query()->get();
        }


        if (!empty($request)) {
            $input = array_keys($request);
            $array = [];
            foreach ($input as $item) {
                $array[] = str_replace('_', ' ', $item);
            }

        }
        if (!empty($array)){
            $categories = Category::query()->whereIn('title', $array)->get();
            if ($categories->isNotEmpty()) {
                $products = Category::query()->whereIn('id', $categories->pluck('id'))->with('products')->get()->pluck('products')->flatten()->toArray();
                $products = $this->ratingFilter($products, request()->get('customRadio'));
                $products = $this->serviceTeg->productFormation($products);
            } else {
                if (!isset($products)) {
                    $products = $this->ratingFilter(Product::query()->get()->toArray(), request()->get('customRadio'));
                    $products = $this->serviceTeg->productFormation($products);
                }
            }
        }

        if (!empty($request['sort'])) {
            if ($request['sort'] == 'price_due') {
                usort($products, function ($a, $b) {
                    return ($a['price'] - $b['price']);
                });
                $products = $this->serviceTeg->productFormation($products);
            }
        }

        if (!empty($request['sort'])) {
            if ($request['sort'] == 'price_from') {
                usort($products, function ($a, $b) {
                    return ($b['price'] - $a['price']);
                });
                $products = $this->serviceTeg->productFormation($products);
            }
        }

        if (!empty($request['sort'])) {
            if ($request['sort'] == 'date') {
                foreach ($products as $product) {
                    $arrayIds[] = $product['id'];
                }
                $products = Product::query()->whereIn('id', $arrayIds)->get()->sortBy([['created_at', 'desc']]);
                $products = $this->serviceTeg->productFormation($products);
            }
        }
        if (!empty($request['sort'])) {
            if ($request['sort'] == 'popular'){
                $products = collect($products)->sortBy([['views', 'desc']])->toArray();
                $products = $this->serviceTeg->productFormation($products);
            }
        }
        if (!empty($request['sort'])) {
            if ($request['sort'] == 'rating'){
                $products = $this->serviceTeg->productFormation($products);
                $products = collect($products)->sortBy([['rating', 'desc']]);
            }
        }

        if (!empty($request['sort'])) {
            if ($request['sort'] == 'employment_assistance'){
                $products = collect($products);
                $products = Product::query()->whereIn('id', $products->pluck('id'))->where('employment_assistance', true)->get();
                $products = $this->serviceTeg->productFormation($products);
            }
        }
        if (!empty($request['sort'])) {
            if ($request['sort'] == 'internship'){
                $products = collect($products);
                $products = Product::query()->whereIn('id', $products->pluck('id'))->where('internship', true)->get();
                $products = $this->serviceTeg->productFormation($products);

            }
        }

        return $products;
    }

    public function apiHHWork()
    {
        $client = new Client();
        $response = $client->get('https://api.hh.ru/salary_statistics/dictionaries/professional_areas');
        $result = collect(json_decode(file_get_contents('work.txt'), true));
        $result = $result->where('name', '=', 'Разработка ПО');
        $id = $result->map(function ($work) {
            foreach ($work['specializations'] as $specialization) {
                $res = stristr($specialization['name'], 'PHP');
                if ($res) {
                    return $specialization['id'];
                }
            }
        });

        dd($id);
//        file_put_contents('work.txt',$response->getBody()->getContents());


    }


    public
    function ratingFilter($products, $arg = null)
    {
        foreach ($products as $key => $product) {
            $rating = (DB::table('ratings')
                ->where('product_id', $product['id'])
                ->get()->avg('rating')) ? DB::table('ratings')
                ->where('product_id', $product['id'])->get()->avg('rating') : 0;
            $products[$key]['rating'] = $rating;
        }

        if (!is_null($arg)) {
            $result = [];
            $fromArg = $arg + 0.5;
            foreach ($products as $product) {
                if ($product['rating'] >= $arg && $product['rating'] < $fromArg) {
                    $result[] = $product;
                }
            }
            return $result;
        }
        return $products;
    }

    public
    function create(): View
    {
        $categories = Category::all();
        $sub_categories = SubCategory::all();
        return \view('product.create',
            [
                'categories' => $categories,
                'sub_categories' => $sub_categories
            ]);
    }


    public function ajaxStore()
    {
        try {
            $img = Image::make(request()->file('image-upload')->getRealPath());
            $pathImage = 'storage/images/' . md5(Str::random(8)) . '.png';
            $img->save($pathImage);
        } catch (\Exception $exception) {
            return [
                'errors' => 'Укажите путь к изображению заного.'
            ];
        }


//        $validator = Validator::make(request()->all(), ProductStoreRequest::rules());
//
//        if ($validator->fails()) {
//            return response()->json([
//                'success' => false,
//                'errors' => $validator->getMessageBag()->toArray()
//            ]);
//        }
        $urlValidation = $this->urlEdit(request()->get('url'));

        $urlValidation = explode('?', $urlValidation);

        $productUrlValidation = Product::query()->where('url', $urlValidation[0])->get();

        if ($productUrlValidation->isNotEmpty()) {
            return \response()->json(['errors' => ['Такой курс уже существует.']]);
        }
        if (count($urlValidation) > 1) {
            $productUrlValidation = Product::query()->where('url', $urlValidation[0] . '?' . $urlValidation[1])->get();
            if ($productUrlValidation->isNotEmpty()) {
                return [
                    'errors' => 'Такой курс уже существует.'
                ];
            }
        }

        $url = explode('?', request()->get('url'));
        $result = Http::get($url[0])->body();
        preg_match('/' . request()->get('h_one') . '/', $result, $matches);

        if (!empty($matches)) {
            $url = $this->urlEdit(request()->get('url'));
        }

        if (empty($matches)) {
            $result = Http::get(implode('?', $url))->body();
            preg_match('/' . request()->get('h_one') . '/', $result, $matches);
            if (!empty($matches)) {
                $url = $this->urlEdit(request()->get('url'));
            }
        }

        if (empty($matches)) {
            return ['errors' => ['Заголовок курса должен совподать с заголовком курса которой находиться по url который вы добовляете!']];
        }

        $product = new Product();
        $product->category_id = request()->get('category_id');
        $product->sub_category_id = request()->get('sub_category_id');
        if (Auth::id()) {
            $product->user_id = Auth::id();
        }

        try {
            $product->title = request()->get('title');
            $product->content = request()->get('description_one');
            $product->h_one = request()->get('h_one');
            $product->image = $pathImage;
            $product->credit = request()->get('credit');
            $product->author_school = request()->get('author_school');
            $product->views = request()->get('views');
            $product->duration_of_the_course = request()->get('duration_of_the_course_int') . ' ' . request()->get('duration_of_the_course_type');
            $product->price = request()->get('price');
            $product->start_curse = request()->get('start_curse');
            $product->end_curse = request()->get('end_curse');
            $product->salary_due = request()->get('salary_due');
            $product->salary_from = request()->get('salary_from');
            $product->online_offline = request()->get('online_offline');
            $product->online_offline = request()->get('online_offline');
            $product->teachers = request()->get('teachers');
            $product->url = $url;
            $product->ip = request()->ip();
            if (!is_null(request()->get('employment_assistance'))) {
                $product->employment_assistance = true;
            }
            if (!is_null(request()->get('internship'))) {
                $product->internship = true;
            }
            $product->views = 1;
            $product->user_cookie = $_COOKIE['it-id'];
            $product->save();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

        return 'success';
    }

    public
    function store(ProductStoreRequest $request)
    {

        $urlValidation = $this->urlEdit($request->get('url'));

        $urlValidation = explode('?', $urlValidation);
        $productUrlValidation = Product::query()->where('url', $urlValidation[0])->get();
        if ($productUrlValidation->isNotEmpty()) {
            return redirect()->back()->withInput()->withErrors([
                'errors' => 'Такой курс уже существует.'
            ]);
        }
        if (count($urlValidation) > 1) {
            $productUrlValidation = Product::query()->where('url', $urlValidation[0] . '?' . $urlValidation[1])->get();
            if ($productUrlValidation->isNotEmpty()) {
                return redirect()->back()->withInput()->withErrors([
                    'errors' => 'Такой курс уже существует.'
                ]);
            }
        }

        $url = explode('?', $request->get('url'));
        $result = Http::get($url[0]);
        preg_match('/' . $request->get('h_one') . '/', $result, $matches);

        if (!empty($matches)) {
            $url = $this->urlEdit($request->get('url'));
        }

        if (empty($matches)) {
            $result = Http::get(implode('?', $url));
            preg_match('/' . $request->get('h_one') . '/', $result, $matches);
            if (!empty($matches)) {
                $url = $this->urlEdit($request->get('url'));
            }
        }

        if (empty($matches)) {
            return redirect()->back()->withInput()
                ->withErrors([
                    'errors' => 'Заголовок курса должен совподать с заголовком курса которой находиться по url который вы добовляете!'
                ]);
        }

        $product = new Product();
        $product->category_id = $request->get('category_id');
        $product->sub_category_id = $request->get('sub_category_id');
        if (Auth::id()) {
            $product->user_id = Auth::id();
        }

        $img = Image::make($request->get('file'));
        $pathImage = 'storage/images/' . md5(Str::random(8)) . '.png';
        $img->save($pathImage);

        $product->title = $request->get('title');
        $product->content = $request->get('content');
        $product->content_two = $request->get('content_two');
        $product->h_one = $request->get('h_one');
        $product->image = $pathImage;
        $product->credit = $request->get('credit');
        $product->author_school = $request->get('author_school');
        $product->for_reading = $request->get('for_reading');
        $product->views = $request->get('views');
        $product->duration_of_the_course = $request->get('duration_of_the_course');
        $product->price = $request->get('price');
        $product->start_curse = $request->get('start_curse');
        $product->end_curse = $request->get('end_curse');
        $product->salary_due = $request->get('salary_due');
        $product->salary_from = $request->get('salary_from');
        $product->online_offline = $request->get('online_offline');
        $product->online_offline = $request->get('online_offline');
        $product->teachers = $request->get('teachers');
        $product->url = $url;
        $product->ip = $request->ip();
        $product->views = 1;
        $product->user_cookie = $_COOKIE['it-id'];
        $product->save();

        return redirect()->back()->with(['success' => 'Отправлено на проверку модератору.']);
    }

    public
    function urlEdit(string $url): string
    {
        $result = preg_replace('#^(http://)#', '', $url);
        $result = preg_replace('#^(https://)#', '', $result);
        $result = preg_replace('#^(www.)#', '', $result);

        return $result;
    }

    public
    function show($id)
    {
        $product = Product::find($id);
        $reviews = Review::query()->where('product_id', $id)->get()->reverse();
        $similarCourses = SubCategory::query()->find($product->sub_category_id)->products->random(1);
        $rating = DB::table('ratings')->where('product_id', $id)->get();
        $rating = $rating->groupBy('rating');

        $resultRating = $rating->map(function ($value, $index) {
            return $array[$index] = $value->count();
        });

        if ($resultRating->isNotEmpty()) {
            $i = 100 / $resultRating->sum();
        }

        $grade = [];

        if ($resultRating->isEmpty()) {
            $grade[1] = 0;
            $grade[2] = 0;
            $grade[3] = 0;
            $grade[4] = 0;
            $grade[5] = 0;
        }

        foreach ($resultRating as $key => $item) {
            if (!array_key_exists(1, $resultRating->toArray())) {
                $grade[1] = 0;
            }
            if (!array_key_exists(2, $resultRating->toArray())) {
                $grade[2] = 0;
            }
            if (!array_key_exists(3, $resultRating->toArray())) {
                $grade[3] = 0;
            }
            if (!array_key_exists(4, $resultRating->toArray())) {
                $grade[4] = 0;
            }
            if (!array_key_exists(5, $resultRating->toArray())) {
                $grade[5] = 0;
            }
            $grade[$key] = floor($item * $i);
        }

        if (Auth::check()) {
            $rating['avg'] = DB::table('ratings')
                ->where('product_id', $id)
                ->get()->avg('rating');
            $rating['avg'] = round($rating['avg'], PHP_ROUND_HALF_UP);
            $rating['count'] = DB::table('ratings')->where('product_id', $id)->get()->count();

            $bookmark = Bookmark::query()->where('product_id', $id)->where('user_id', Auth::id())->get();
            $product->subscription = Subscription::query()->where('user_id', $product->user_id)->where('subscription_id', Auth::id())->get()->isEmpty();

            if (!empty($rating)) {
                $rating['avg'] = 0;
                return \view('product.show', [
                    'grade' => $grade,
                    'product' => $product,
                    'reviews' => $reviews,
                    'rating' => $rating,
                    'similarCourses' => $similarCourses,
                    'bookmark' => $bookmark->isNotEmpty()
                ]);
            }
        }
        $rating['avg'] = DB::table('ratings')
            ->where('product_id', $id)
            ->get()->avg('rating');
        $rating['avg'] = round($rating['avg'], PHP_ROUND_HALF_UP);

        return \view('product.show', ['grade' => $grade, 'product' => $product, 'rating' => $rating, 'reviews' => $reviews, 'similarCourses' => $similarCourses]);
    }

    public
    function edit($id)
    {
        $product = Product::find($id);
        if (session('success') == 'Updated') {
            $session = 'save';
        } else {
            $session = null;
        }
        return \view('product.edit', ['product' => $product, 'session' => $session]);
    }

    public
    function update(ProductUpdateRequest $request, $id)
    {
        $productUpdate = Product::query()->find($id)->update([$request->input()]);
        if ($productUpdate) {
            return redirect()->back()->with(['success' => 'Updated']);
        }
    }


    public
    function productsAjax()
    {
        $products = Product::query()->get()->toArray();

        return response()->json($products);
    }

    public
    function productsSearchAjax()
    {
        $request = request()->get('text');
        $result = DB::select("SELECT * FROM products WHERE h_one LIKE '$request%'");
        return response()->json($result);

//        $result = Product::query()->where('h_one', request()->get('text'))->get()->toArray();
    }

    public
    function viewProduct()
    {
        $product = Product::find((int)request()->get('product_id'));
        $product->views = $product->views + 1;
        $product->save();

        return response()->json($product);
    }
}
