<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionController extends Controller
{
    public function ajaxSubscriptionStore()
    {
        $user_id = Auth::id();
        if (\request()->get('id')){
            $tmp = Subscription::query()->where('user_id',\request()->id)->where('subscription_id', $user_id)->get();
            if ($tmp->isEmpty()){
                $subscription = new Subscription();
                $subscription->user_id = \request()->id;
                $subscription->subscription_id = $user_id;
                $subscription->save();
            }else{
                Subscription::query()->where('user_id', \request()->id)->where('subscription_id', $user_id)->delete();
                return 'Вы отписались от ' . User::find(\request()->get('id'))->name . '.';
            }
        }

        return 'Вы подписались на ' . User::find(\request()->get('id'))->name . '.';
    }
}
