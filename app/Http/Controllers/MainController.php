<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MainController extends Controller
{
    public function index(): View
    {
        $categories = Category::all();
        return \view('main.index',
        [
            'categories' => $categories
        ]);
    }
}
