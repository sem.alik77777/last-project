<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\Request;

class CrudCategoryController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\Category");
        $this->crud->setRoute("admin/categories");
        $this->crud->setEntityNameStrings('Category', 'Categories');
    }

    public function setupListOperation()
    {
        $this->crud->setColumns([
            'title'
        ]);
    }

    public function setupCreateOperation()
    {
        $this->crud->addFields([
            [
                'label' => 'Назвние категории',
                'type' => 'text',
                'name' => 'title'
            ],
            [
                'label' => 'Заголовок',
                'type' => 'text',
                'name' => 'h_one'
            ]
        ]);
    }

    public function setupUpdateOperation()
    {
        $this->crud->addFields([
            [
                'label' => 'Назвние категории',
                'type' => 'text',
                'name' => 'title'
            ],
            [
                'label' => 'Заголовок',
                'type' => 'text',
                'name' => 'h_one'
            ]
        ]);
    }
}
