<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Illuminate\Http\Request;

class CrudProductController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\Product");
        $this->crud->setRoute("admin/products");
        $this->crud->setEntityNameStrings('Product', 'Products');
    }

    public function setupListOperation()
    {
        $this->crud->setColumns([
            'user_cookie','title', 'author','status'
        ]);
    }

    public function setupCreateOperation()
    {

        $this->crud->addFields([
            [   // relationship
                'type' => "relationship",
                'name' => 'category_id', // the method on your model that defines the relationship

                // OPTIONALS:
                 'label' => "Category",
                 'attribute' => "title", // foreign key attribute that is shown to user (identifiable attribute)
                 'entity' => 'category', // the method that defines the relationship in your Model
                 'model' => "App\Models\Category", // foreign key Eloquent model
                // 'placeholder' => "Select a category", // placeholder for the select2 input
            ],
            [   // relationship
                'type' => "relationship",
                'name' => 'sub_category_id', // the method on your model that defines the relationship

                // OPTIONALS:
                'label' => "SubCategory",
                'attribute' => "title", // foreign key attribute that is shown to user (identifiable attribute)
                'entity' => 'subCategory', // the method that defines the relationship in your Model
                'model' => "App\Models\SubCategory", // foreign key Eloquent model
                // 'placeholder' => "Select a category", // placeholder for the select2 input
            ],
            [
                'label' => 'Title',
                'name' => 'title',
                'type' => 'text'
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => 'Description'
            ],
            [
                'name' => 'text',
                'type_' => 'textarea',
                'label' => 'Text'
            ],
            [
                'name' => 'h_one',
                'label' => 'H One',
                'type' => 'text'
            ],
            [
                'label' => "Image",
                'name' => "image",
                'type' => 'image',
                'crop' => true, // set to true to allow cropping, false to disable
                'aspect_ratio' => 1, // omit or set to 0 to allow any aspect ratio
            ],
            [
                'name' => 'author',
                'label' => 'Author',
                'type' => 'text',
            ],
            [
                'name' => 'for_reading',
                'type' => 'number',
                'label' => 'For Reading'
            ],
            [
                'name' => 'views',
                'label' => 'View',
                'type' => 'number'
            ],
            [
                'name' => 'time',
                'type' => 'text',
                'label' => 'Time'
            ],
            [
                'name' => 'price',
                'type' => 'number',
                'label' => 'Price',
                'attributes' => ["step" => "any"], // allow decimals
                // 'prefix'     => "$",
                'suffix' => ".00",
            ],
            [   // URL
                'name'  => 'url',
                'label' => 'Url',
                'type'  => 'text'
            ],
            [
                'name' => 'status',
                'label' => 'Publish',
                'type' => 'checkbox'
            ]
        ]);

    }

    public function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function setupShowOperation()
    {
        $this->crud->setColumns([
            [
                // any type of relationship
                'name'         => 'category_id', // name of relationship method in the model
                'type'         => 'relationship',
                'label'        => 'Category', // Table column heading
                // OPTIONAL
                 'entity'    => 'category', // the method that defines the relationship in your Model
                 'attribute' => 'title', // foreign key attribute that is shown to user
                 'model'     => 'App\Models\Category', // foreign key model
            ],
            [
                // any type of relationship
                'name'         => 'sub_category_id', // name of relationship method in the model
                'type'         => 'relationship',
                'label'        => 'Sub Category', // Table column heading
                // OPTIONAL
                'entity'    => 'subCategory', // the method that defines the relationship in your Model
                'attribute' => 'title', // foreign key attribute that is shown to user
                'model'     => 'App\Models\SubCategory', // foreign key model
            ],
        ]);
    }

}
