<?php

namespace App\Http\Controllers;

use App\Models\AboutMe;
use App\Models\Bookmark;
use App\Models\Notification;
use App\Models\Product;
use App\Models\Subscription;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PersonalAreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $notification = Notification::query()->where('user_id', Auth::id())->get()->sortBy('read');
        $countNotification = $notification->count();
        $bookmarks = Bookmark::query()->where('user_id', Auth::id())->get();
        $about_me = AboutMe::query()->where('user_id', Auth::id())->first();
        $subscriptions = Subscription::query()->where('subscription_id', Auth::id())->paginate(4);

        return view('cabinet.index', [
            'notification' => $notification,
            'countNotification' => $countNotification,
            'bookmarks' => $bookmarks,
            'about_me' => $about_me,
            'subscriptions' => $subscriptions
        ]);
    }

    public function notificationReadAjax()
    {
        $keys = array_keys(\request()->input());
        $result = [];
        foreach ($keys as $value) {
            $result[] = explode('-', $value)[1];
        }

        $notification = Notification::query()->whereIn('product_id', $result)->get();
        if (!empty($notification)) {
            foreach ($notification as $value) {
                $value->read = 1;
                $value->save();
            }
        }
        return true;
    }

    public function removeAllNotification()
    {
        if (Auth::check()) {
            DB::table('notifications')->where('user_id', '=', Auth::id())->delete();
        }
        return 'success';
    }

    public function deleteNotifications()
    {
        DB::table('notifications')->whereIn('id', request()->input())->delete();

        return 'delete';
    }

    public function readNotifications()
    {
        $notifications = DB::table('notifications')->whereIn('id', request()->input())->get();

        foreach ($notifications as $notification) {
            $notification = Notification::query()->find($notification->id);
            $notification->read = true;
            $notification->save();
        }
        return 'read';
    }

    public function aboutMeStore()
    {
        $user = Auth::id();
        if ($user) {

            $about = AboutMe::query()->where('user_id', $user)->get();

            if ($about->isEmpty()) {

                $about = new AboutMe();
                $about->user_id = $user;
                if (!empty(request()->get('first_name'))) {
                    $about->first_name = request()->get('first_name');
                }
                if (!empty(request()->get('last_name'))) {
                    $about->last_name = request()->get('last_name');
                }
                if (!empty(request()->get('patronymic'))) {
                    $about->patronymic = request()->get('patronymic');
                }
                if (!empty(request()->get('phone'))) {
                    $about->phone = request()->get('phone');
                }
                if (!empty(request()->get('date'))) {
                    $about->date = request()->get('date');
                }
                if (!empty(request()->get('about_me'))) {
                    $about->about_me = request()->get('about_me');
                }
                if (!empty(request()->get('country'))) {
                    $about->country = request()->get('country');
                }
                if (!empty(request()->get('city'))) {
                    $about->city = request()->get('city');
                }

                $about->save();
            } else {
                $about = AboutMe::query()->where('user_id', $user)->first();
                if (!empty(request()->get('first_name'))) {
                    $about->first_name = request()->get('first_name');
                }
                if (!empty(request()->get('last_name'))) {
                    $about->last_name = request()->get('last_name');
                }
                if (!empty(request()->get('phone'))) {
                    $about->phone = request()->get('phone');
                }
                if (!empty(request()->get('patronymic'))) {
                    $about->patronymic = request()->get('patronymic');
                }
                if (!empty(request()->get('date'))) {
                    $about->date = request()->get('date');
                }
                if (!empty(request()->get('about_me'))) {
                    $about->about_me = request()->get('about_me');
                }
                if (!empty(request()->get('country'))) {
                    $about->country = request()->get('country');
                }
                if (!empty(request()->get('city'))) {
                    $about->city = request()->get('city');
                }

                $about->save();
                $info = 'Вы успешно обновили данные о себе.';
                $notification = new Notification();
                $notification->user_id = $user;
                $notification->text = $info;
                $notification->save();
                return $info;
            }
        }
        $info = 'Вы успешно добавили информацию о себе.';
        $notification = new Notification();
        $notification->user_id = $user;
        $notification->text = $info;
        $notification->save();
        return $info;
    }

    public function getAllCursesForUser()
    {
        $products = Product::query()->where('user_id', request()->get('id'))->get();

        return response()->json($products);
    }
}
