<?php

namespace App\Http\Controllers\Authorization;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthorizationController extends Controller
{

    public function githubRedirect()
    {
        return Socialite::driver('github')->redirect();
    }

    public function githubCallback()
    {
        $user = Socialite::driver('github')->user();
        $this->userCreate($user);
        return redirect()->route('product.index');
    }

    public function linkedinRedirect()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    public function linkedinCallback()
    {
        $user = Socialite::driver('linkedin')->user();
        $this->userCreate($user);
        return redirect()->route('product.index');
    }

    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback()
    {
        $user = Socialite::driver('facebook')->user();
        dd($user);
    }

    public function userCreate($user)
    {

        $auth = User::query()->where('email', $user->getEmail())->first();
        if (empty($auth)) {
            $newUser = new User();
            $newUser->name = $user->getNickname();
            $newUser->email = $user->getEmail();
            $newUser->avatar = $user->getAvatar();

            $newUser->save();
            $newUser = User::query()->where('email', $user->getEmail())->first();
        } else {

            if (is_null($user->getNickname())){
                $auth->name = $user->getName();
            }else{
                $auth->name = $user->getNickname();
            }
            $auth->email = $user->getEmail();
            $auth->avatar = $user->getAvatar();
            $auth->save();
            $newUser = User::query()->where('email', $user->getEmail())->first();
        }
        $this->userAuthorization($newUser);
    }

    public function userAuthorization($user)
    {
        Auth::login($user, true);
    }
}
