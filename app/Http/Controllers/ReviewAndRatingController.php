<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Product;
use App\Models\Review;
use App\Models\ReviewChild;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReviewAndRatingController extends Controller
{


    public function storeAjax()
    {
        \request()->validate(['review' => 'required|min:2|max:150']);

        if (!Auth::check()){
            DB::table('reviews')->insert([
                'product_id' => \request()->get('product_id'),
                'review' => \request()->get('review'),
                'created_at' => Carbon::now(),
                'ip' => \request()->ip()
            ]);

        }else{
            DB::table('reviews')->insert([
                'user_id' => \request()->user()->id,
                'product_id' => \request()->get('product_id'),
                'review' => \request()->get('review'),
                'created_at' => Carbon::now(),
                'ip' => \request()->ip()
            ]);

            $notification = new Notification();
            $notification->user_id = \request()->user()->id;
            $notification->text = 'Отзыв успешно оставлен к курсу -' . Product::find(\request()->get('product_id'))->h_one;
            $notification->product_id = \request()->get('product_id');
            $notification->save();


            if (!empty(\request()->get('rating'))) {
                $rating = DB::table('ratings')->where('user_id', \request()->user()->id)
                    ->where('product_id', \request()->get('product_id'))->get();

                if ($rating->isEmpty()) {
                    DB::table('ratings')->insert([
                        'user_id' => \request()->user()->id,
                        'product_id' => \request()->get('product_id'),
                        'rating' => \request()->get('rating'),
                        'created_at' => Carbon::now()
                    ]);
                    $notification = new Notification();
                    $notification->user_id = \request()->user()->id;
                    $notification->text = 'Оценка курса успешно была произведена. -' . Product::find(\request()->get('product_id'))->h_one;
                    $notification->save();
                } else {
                    DB::table('ratings')->find($rating->pluck('id')[0])->update([
                        'rating' => \request()->get('rating'),
                        'created_at' => Carbon::now()
                    ]);
                    $notification = new Notification();
                    $notification->user_id = \request()->user()->id;
                    $notification->text = 'Оценка курса успешно была обновлена. -' . Product::find(\request()->get('product_id'))->h_one;
                    $notification->save();
                }
            }
        }
    }

    public function storeReviewChildren(Request $request)
    {
        $reviewChild = new ReviewChild();
        $reviewChild->user_id = Auth::id();
        $reviewChild->review_id = $request->get('review_id');
        $reviewChild->text = $request->get('text');
        $reviewChild->created_at = Carbon::now();
        $reviewChild->save();

        return redirect()->back();
    }

    public function reviewDelete($id)
    {
        $review = Review::query()->find($id);
        if ($review->user_id == Auth::id()) {
            $review->delete();
        }

        return redirect()->back();
    }

    public function reviewChildDelete($id)
    {
        $reviewChild = ReviewChild::query()->find($id);
        if ($reviewChild->user_id == Auth::id()) {
            $reviewChild->delete();
        }

        return redirect()->back();
    }

    public function reviewPaginate()
    {
        $result = json_decode(\request()->get('json'), true);

        $reviews = Review::query()
            ->where('product_id', $result['product_id'])
            ->with('user')
            ->with('reviewChild')
            ->get()
            ->reverse()
            ->skip(\request()
                ->get('skip'))
            ->take(3);

        return $reviews;
    }
}
