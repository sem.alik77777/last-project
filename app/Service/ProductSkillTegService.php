<?php

namespace App\Service;


use App\Models\User;
use Illuminate\Support\Facades\DB;

class ProductSkillTegService
{
//    public function renderTag($product)
//    {
//        if ($product['skill'] == SkillType::Pro) {
//            $result = ' <svg class="me-1 mt-n1" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
//    <rect x="3" y="8" width="2" height="6" rx="1" fill="#754FFE"></rect>
//    <rect x="7" y="5" width="2" height="9" rx="1" fill="#754FFE"></rect>
//    <rect x="11" y="2" width="2" height="12" rx="1" fill="#754FFE"></rect>
//</svg>
//    Профи';
//        } elseif ($product['skill'] == SkillType::Amateur) {
//            $result = '    <svg class="me-1 mt-n1" width = "16" height = "16" viewBox = "0 0 16 16" fill = "none" xmlns = "http://www.w3.org/2000/svg" >
//    <rect x = "3" y = "8" width = "2" height = "6" rx = "1" fill = "#754FFE" ></rect >
//    <rect x = "7" y = "5" width = "2" height = "9" rx = "1" fill = "#754FFE" ></rect >
//    <rect x = "11" y = "2" width = "2" height = "12" rx = "1" fill = "#DBD8E9" ></rect >
//</svg >
//    Опытный';
//        } elseif ($product['skill'] == SkillType::Newbie) {
//            $result = '    <svg class="me-1 mt-n1" width = "16" height = "16" viewBox = "0 0 16 16" fill = "none" xmlns = "http://www.w3.org/2000/svg" >
//    <rect x = "3" y = "8" width = "2" height = "6" rx = "1" fill = "#754FFE" ></rect >
//    <rect x = "7" y = "5" width = "2" height = "9" rx = "1" fill = "#DBD8E9" ></rect >
//    <rect x = "11" y = "2" width = "2" height = "12" rx = "1" fill = "#DBD8E9" ></rect >
//</svg >
//    Новичек';
//        }
//        return $result;
//    }

    public function renderStarTag($rating)
    {
        $result = '';

        if (is_null($rating)) {
            for ($i = 0; $i < 5; $i++) {
                $result = $result . '' . '<i class="bi-star me-n1 text-warning"></i>';
            }
            return $result;
        }
//        if ($rating > 0){
        for ($i = 0; $i < $rating; $i++) {
            $result = $result . '' . '<i class="bi-star-fill me-n1 text-warning"></i>';
        }
//        }
//        for ($i = 0; $i < $rating; $i++) {
//            $value = $rating - $i;
//            if ($value < 1) {
//                $result = $result . '' . '<i class="bi-star-half me-n1 text-warning"></i>';
//                return $result;
//            }
//            $result = $result . '' . '<i class="bi-star-fill me-n1 text-warning"></i>';
//        }
        return $result;
    }

    public function productFormation($products)
    {
        foreach ($products as $key => $product) {
            $rating = DB::table('ratings')->where('product_id', $product['id'])
                ->get()->avg('rating');

            $products[$key]['rating'] = (is_null($rating)) ? 0 : $rating;
            $products[$key]['stars'] = $this->renderStarTag($rating);
            $products[$key]['content'] = mb_strimwidth($product['content'], 0, 200, '...');
            if ($product['internship']){
                $products[$key]['internship'] = '<i class="bi-check fs-4">Стажировка</i>';
            }else{
                $products[$key]['internship'] = '';
            }
            if ($product['employment_assistance']){
                $products[$key]['employment_assistance'] = '<i class="bi-check fs-4">Помощь в трудоустройстве</i>';
            }else{
                $products[$key]['employment_assistance'] = '';
            }

            if (!is_null(['user_id'])) {
                $products[$key]['avatar'] = '';
                $products[$key]['name'] = '';
            } else {
                if (User::find($product['user_id'])->avatar !== null) {
                    $user = User::query()->find($product['user_id']);
                    $products[$key]['avatar'] = '<img src="' . $user->avatar . '" class="rounded-circle avatar-xs" alt="">' ;
                    $products[$key]['name'] = $user->name;
                }
            }
        }

        return $products;
    }
}
